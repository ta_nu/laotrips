<?php

/* @gantry-admin/partials/php_unsupported.html.twig */
class __TwigTemplate_731c0e50cc2c7fcc71c5f9fad488b87f2347422682d2368f5592f9924b3cb557 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["php_version"] = twig_constant("PHP_VERSION");
        // line 2
        echo "
";
        // line 3
        if ((is_string($__internal_689af7e6c6fcf5357791862c77e03d00afc56c955142d87365441597ddad8e63 = ($context["php_version"] ?? null)) && is_string($__internal_4ec434d69d09655380cce6b13cd28bda79e1e92493bd72766fe74b6e717c9dbd = "5.4") && ('' === $__internal_4ec434d69d09655380cce6b13cd28bda79e1e92493bd72766fe74b6e717c9dbd || 0 === strpos($__internal_689af7e6c6fcf5357791862c77e03d00afc56c955142d87365441597ddad8e63, $__internal_4ec434d69d09655380cce6b13cd28bda79e1e92493bd72766fe74b6e717c9dbd)))) {
            // line 4
            echo "<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        ";
            // line 6
            echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->transFilter("GANTRY5_PLATFORM_PHP54_WARNING", ($context["php_version"] ?? null));
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@gantry-admin/partials/php_unsupported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@gantry-admin/partials/php_unsupported.html.twig", "C:\\xampp\\htdocs\\tip\\administrator\\components\\com_gantry5\\templates\\partials\\php_unsupported.html.twig");
    }
}
