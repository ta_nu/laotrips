<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5a15471f03a1d9.97313216',
    'content' => '<div id="custom-4731-particle" class="g-content g-particle">            <div class="error-text">
  <h2>Whoops!</h2>
  <p>
      Sorry, the page could not be found. Visit the <a href="/tip/index.php?5a15306d">homepage</a>
  </p>
</div>
            </div>'
];
