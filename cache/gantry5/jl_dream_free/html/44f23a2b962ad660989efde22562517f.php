<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5a15443d96c282.05679854',
    'content' => '<div id="social-8943-particle" class="g-content g-particle">            <div class="g-social ">
                    <a target="_blank" href="https://www.facebook.com/JoomLead" title="" aria-label="">
                <span class="fa fa-facebook"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-pinterest-p fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="http://www.twitter.com/JoomLead" title="" aria-label="">
                <span class="fa fa-twitter fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-behance"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-dribbble"></span>                <span class="g-social-text"></span>            </a>
            </div>
            </div>'
];
