<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5a1546184cc6a4.49524890',
    'content' => '<div id="social-8943-particle" class="g-content g-particle">            <div class="g-social ">
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-facebook"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-pinterest-p fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="http://www.twitter.com/JoomLead" title="" aria-label="">
                <span class="fa fa-twitter fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-behance"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-dribbble"></span>                <span class="g-social-text"></span>            </a>
            </div>
            </div>'
];
