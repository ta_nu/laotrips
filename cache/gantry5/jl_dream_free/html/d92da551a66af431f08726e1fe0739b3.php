<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5a154618546590.86309572',
    'content' => '<div id="social-1645-particle" class="g-content g-particle">            <div class="g-social ">
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-facebook"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="#" title="" aria-label="">
                <span class="fa fa-facebook-square fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="http://www.twitter.com/JoomLead" title="" aria-label="">
                <span class="fa fa-twitter-square fa-fw"></span>                <span class="g-social-text"></span>            </a>
            </div>
            </div>'
];
