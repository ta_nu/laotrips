<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1511343902,
    'checksum' => '42efa5551e6c76e9944205e1ad434b29',
    'files' => [
        'templates/jl_dream_free/custom/config/_error' => [
            'index' => [
                'file' => 'templates/jl_dream_free/custom/config/_error/index.yaml',
                'modified' => 1511338753
            ],
            'layout' => [
                'file' => 'templates/jl_dream_free/custom/config/_error/layout.yaml',
                'modified' => 1511338752
            ],
            'page/head' => [
                'file' => 'templates/jl_dream_free/custom/config/_error/page/head.yaml',
                'modified' => 1511338752
            ]
        ],
        'templates/jl_dream_free/custom/config/default' => [
            'index' => [
                'file' => 'templates/jl_dream_free/custom/config/default/index.yaml',
                'modified' => 1511338753
            ],
            'layout' => [
                'file' => 'templates/jl_dream_free/custom/config/default/layout.yaml',
                'modified' => 1511343661
            ]
        ],
        'templates/jl_dream_free/config/default' => [
            'page/assets' => [
                'file' => 'templates/jl_dream_free/config/default/page/assets.yaml',
                'modified' => 1511338752
            ],
            'page/body' => [
                'file' => 'templates/jl_dream_free/config/default/page/body.yaml',
                'modified' => 1511338752
            ],
            'page/head' => [
                'file' => 'templates/jl_dream_free/config/default/page/head.yaml',
                'modified' => 1511338752
            ],
            'particles/branding' => [
                'file' => 'templates/jl_dream_free/config/default/particles/branding.yaml',
                'modified' => 1511338752
            ],
            'particles/copyright' => [
                'file' => 'templates/jl_dream_free/config/default/particles/copyright.yaml',
                'modified' => 1511338752
            ],
            'particles/logo' => [
                'file' => 'templates/jl_dream_free/config/default/particles/logo.yaml',
                'modified' => 1511338752
            ],
            'particles/social' => [
                'file' => 'templates/jl_dream_free/config/default/particles/social.yaml',
                'modified' => 1511343529
            ],
            'particles/totop' => [
                'file' => 'templates/jl_dream_free/config/default/particles/totop.yaml',
                'modified' => 1511338752
            ],
            'styles' => [
                'file' => 'templates/jl_dream_free/config/default/styles.yaml',
                'modified' => 1511338752
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'contentcubes' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'sample' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2017 by <a href="http://joomlead.com/" title="JoomLead" class="g-powered-by">JoomLead</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2015',
                    'end' => 'now'
                ],
                'owner' => 'JoomLead Team',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => true,
                'url' => '',
                'image' => 'gantry-media://logo/logo.png',
                'text' => 'JoomLead',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => '#',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => '#',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'backtotop' => [
                'enabled' => true,
                'icon' => 'fa fa-angle-double-up'
            ],
            'sticky' => [
                'enabled' => true,
                'spacing' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'page' => [
            'doctype' => 'html',
            'body' => [
                'class' => 'gantry',
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-2958',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/uikit.min.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'priority' => '0',
                                    'name' => 'Uikit'
                                ]
                            ],
                            'javascript' => [
                                0 => [
                                    'location' => 'gantry-assets://js/uikit.min.js',
                                    'inline' => '',
                                    'in_footer' => '0',
                                    'extra' => [
                                        
                                    ],
                                    'priority' => '0',
                                    'name' => 'Uikit'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'id' => 'frameworks-6684',
                        'inherit' => [
                            'outline' => 'default',
                            'atom' => 'frameworks-6684',
                            'include' => [
                                0 => 'attributes'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#888888',
                'color-2' => '#56c8d4'
            ],
            'base' => [
                'background' => '#f5f7f9',
                'text-color' => '#666666',
                'text-active-color' => '#232529'
            ],
            'breakpoints' => [
                'large-desktop-container' => '85rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'fixedside' => [
                'background' => '#ffffff',
                'text-color' => '#222222',
                'width' => '18rem',
                'position' => 'left'
            ],
            'font' => [
                'family-default' => 'family=Open+Sans:300,600,400',
                'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
            ],
            'footer' => [
                'background' => '#212121',
                'text-color' => '#ffffff'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#222222'
            ],
            'main' => [
                'background' => '#f5f7f9',
                'text-color' => '#222222'
            ],
            'menu' => [
                'col-width' => '200px',
                'animation' => 'g-zoom'
            ],
            'menustyle' => [
                'text-color' => '#888888',
                'text-color-active' => '#56c8d4',
                'sublevel-text-color' => '#888888',
                'sublevel-text-color-active' => '#56c8d4',
                'sublevel-background' => '#ffffff'
            ],
            'offcanvas' => [
                'background' => '#ffffff',
                'text-color' => '#222222',
                'toggle-color' => '#ffffff',
                'width' => '14rem',
                'toggle-visibility' => '1'
            ],
            'preset' => 'preset1'
        ],
        'index' => [
            'name' => '_error',
            'timestamp' => 1511338752,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1503328712
            ],
            'positions' => [
                'sidebar' => 'Sidebar',
                'aside' => 'Aside'
            ],
            'sections' => [
                'fixedside' => 'Fixedside',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'header' => 'Header',
                'aside' => 'Aside',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'custom' => [
                    'custom-2330' => 'Error Img',
                    'custom-4731' => 'Error text'
                ],
                'content' => [
                    'system-content-1246' => 'Page Content'
                ],
                'messages' => [
                    'system-messages-4126' => 'System Messages'
                ],
                'logo' => [
                    'logo-9302' => 'Logo / Image',
                    'logo-9193' => 'Logo'
                ],
                'menu' => [
                    'menu-8756' => 'Menu'
                ],
                'social' => [
                    'social-9836' => 'Social',
                    'social-1980' => 'Social'
                ],
                'branding' => [
                    'branding-4613' => 'Branding'
                ],
                'position' => [
                    'position-position-8723' => 'Sidebar',
                    'position-position-4593' => 'Aside'
                ],
                'copyright' => [
                    'copyright-9641' => 'Copyright'
                ],
                'mobile-menu' => [
                    'mobile-menu-4703' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'fixedside' => 'fixedside',
                    'header' => 'header',
                    'sidebar' => 'sidebar',
                    'aside' => 'aside',
                    'footer' => 'footer',
                    'offcanvas' => 'offcanvas',
                    'logo-9302' => 'logo-4547',
                    'menu-8756' => 'menu-6591',
                    'social-9836' => 'social-5621',
                    'branding-4613' => 'branding-9659',
                    'logo-9193' => 'logo-9208',
                    'position-position-8723' => 'position-position-1289',
                    'position-position-4593' => 'position-position-5879',
                    'copyright-9641' => 'copyright-6131',
                    'social-1980' => 'social-3565',
                    'mobile-menu-4703' => 'mobile-menu-8340'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1503328712
            ],
            'layout' => [
                'fixedside' => [
                    
                ],
                'header' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 20' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 60' => [
                                0 => [
                                    0 => 'custom-2330'
                                ],
                                1 => [
                                    0 => 'custom-4731'
                                ],
                                2 => [
                                    0 => 'system-content-1246'
                                ],
                                3 => [
                                    0 => 'system-messages-4126'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 20' => [
                                
                            ]
                        ]
                    ]
                ],
                'footer' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'fixedside' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'header' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'home'
                    ]
                ],
                'aside' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '',
                        'class' => '',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'footer' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'custom-2330' => [
                    'title' => 'Error Img',
                    'attributes' => [
                        'html' => '<div class="abovefooter-ct">
  <img src="gantry-media://error/404.png" alt="" /></div>'
                    ]
                ],
                'custom-4731' => [
                    'title' => 'Error text',
                    'attributes' => [
                        'html' => '<div class="error-text">
  <h2>Whoops!</h2>
  <p>
      Sorry, the page could not be found. Visit the <a href="index.php">homepage</a>
  </p>
</div>'
                    ]
                ]
            ]
        ]
    ]
];
