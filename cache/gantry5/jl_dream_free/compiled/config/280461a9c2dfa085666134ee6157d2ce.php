<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1511412872,
    'checksum' => '04c420e243e49d05c52ba88cf4716c5e',
    'files' => [
        'templates/jl_dream_free/custom/config/_body_only' => [
            'index' => [
                'file' => 'templates/jl_dream_free/custom/config/_body_only/index.yaml',
                'modified' => 1511338753
            ],
            'layout' => [
                'file' => 'templates/jl_dream_free/custom/config/_body_only/layout.yaml',
                'modified' => 1511338752
            ]
        ],
        'templates/jl_dream_free/custom/config/default' => [
            'index' => [
                'file' => 'templates/jl_dream_free/custom/config/default/index.yaml',
                'modified' => 1511348816
            ],
            'layout' => [
                'file' => 'templates/jl_dream_free/custom/config/default/layout.yaml',
                'modified' => 1511348816
            ]
        ],
        'templates/jl_dream_free/config/default' => [
            'page/assets' => [
                'file' => 'templates/jl_dream_free/config/default/page/assets.yaml',
                'modified' => 1511338752
            ],
            'page/body' => [
                'file' => 'templates/jl_dream_free/config/default/page/body.yaml',
                'modified' => 1511338752
            ],
            'page/head' => [
                'file' => 'templates/jl_dream_free/config/default/page/head.yaml',
                'modified' => 1511338752
            ],
            'particles/branding' => [
                'file' => 'templates/jl_dream_free/config/default/particles/branding.yaml',
                'modified' => 1511338752
            ],
            'particles/copyright' => [
                'file' => 'templates/jl_dream_free/config/default/particles/copyright.yaml',
                'modified' => 1511338752
            ],
            'particles/logo' => [
                'file' => 'templates/jl_dream_free/config/default/particles/logo.yaml',
                'modified' => 1511338752
            ],
            'particles/social' => [
                'file' => 'templates/jl_dream_free/config/default/particles/social.yaml',
                'modified' => 1511343529
            ],
            'particles/totop' => [
                'file' => 'templates/jl_dream_free/config/default/particles/totop.yaml',
                'modified' => 1511338752
            ],
            'styles' => [
                'file' => 'templates/jl_dream_free/config/default/styles.yaml',
                'modified' => 1511338752
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'contentcubes' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'sample' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2017 by <a href="http://joomlead.com/" title="JoomLead" class="g-powered-by">JoomLead</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2015',
                    'end' => 'now'
                ],
                'owner' => 'JoomLead Team',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => true,
                'url' => '',
                'image' => 'gantry-media://logo/logo.png',
                'text' => 'JoomLead',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => '#',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => '#',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'backtotop' => [
                'enabled' => true,
                'icon' => 'fa fa-angle-double-up'
            ],
            'sticky' => [
                'enabled' => true,
                'spacing' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'page' => [
            'doctype' => 'html',
            'body' => [
                'class' => 'gantry',
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'sticky-1795',
                        'type' => 'sticky',
                        'title' => 'Sticky',
                        'attributes' => [
                            'enabled' => '1',
                            'id' => 'g-header',
                            'spacing' => '0'
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-6684'
                    ],
                    2 => [
                        'title' => 'Back To Top',
                        'type' => 'backtotop',
                        'attributes' => [
                            'enabled' => true,
                            'icon' => 'fa fa-angle-double-up'
                        ],
                        'id' => 'backtotop-6569'
                    ],
                    3 => [
                        'id' => 'assets-9916',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                
                            ],
                            'javascript' => [
                                0 => [
                                    'location' => 'gantry-assets://js/theme.js',
                                    'inline' => '',
                                    'in_footer' => '0',
                                    'extra' => [
                                        
                                    ],
                                    'priority' => '0',
                                    'name' => 'Theme Js'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#888888',
                'color-2' => '#56c8d4'
            ],
            'base' => [
                'background' => '#f5f7f9',
                'text-color' => '#666666',
                'text-active-color' => '#232529'
            ],
            'breakpoints' => [
                'large-desktop-container' => '85rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'fixedside' => [
                'background' => '#ffffff',
                'text-color' => '#222222',
                'width' => '18rem',
                'position' => 'left'
            ],
            'font' => [
                'family-default' => 'family=Open+Sans:300,600,400',
                'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
            ],
            'footer' => [
                'background' => '#212121',
                'text-color' => '#ffffff'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#222222'
            ],
            'main' => [
                'background' => '#f5f7f9',
                'text-color' => '#222222'
            ],
            'menu' => [
                'col-width' => '200px',
                'animation' => 'g-zoom'
            ],
            'menustyle' => [
                'text-color' => '#888888',
                'text-color-active' => '#56c8d4',
                'sublevel-text-color' => '#888888',
                'sublevel-text-color-active' => '#56c8d4',
                'sublevel-background' => '#ffffff'
            ],
            'offcanvas' => [
                'background' => '#ffffff',
                'text-color' => '#222222',
                'toggle-color' => '#ffffff',
                'width' => '14rem',
                'toggle-visibility' => '1'
            ],
            'preset' => 'preset1'
        ],
        'index' => [
            'name' => '_body_only',
            'timestamp' => 1511338752,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/body-only.png',
                'name' => '_body_only',
                'timestamp' => 1493247591
            ],
            'positions' => [
                
            ],
            'sections' => [
                'main' => 'Main'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-2818' => 'System Messages'
                ],
                'content' => [
                    'system-content-1029' => 'Page Content'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/body-only.png',
                'name' => '_body_only',
                'timestamp' => 1493247591
            ],
            'layout' => [
                '/main/' => [
                    0 => [
                        0 => 'system-messages-2818'
                    ],
                    1 => [
                        0 => 'system-content-1029'
                    ]
                ]
            ],
            'structure' => [
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-6345' => [
                    'title' => 'Logo / Image'
                ],
                'social-8943' => [
                    'attributes' => [
                        'items' => NULL
                    ]
                ],
                'branding-5700' => [
                    'attributes' => [
                        'content' => ' '
                    ]
                ],
                'logo-7657' => [
                    'attributes' => [
                        'image' => 'gantry-media://logo/logo.png'
                    ]
                ],
                'position-position-2554' => [
                    'title' => 'Sidebar',
                    'attributes' => [
                        'key' => 'sidebar'
                    ]
                ],
                'position-module-3578' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'key' => 'module-instance'
                    ]
                ],
                'position-position-2615' => [
                    'title' => 'Module Position',
                    'attributes' => [
                        'key' => 'module-position1'
                    ]
                ],
                'position-position-8830' => [
                    'title' => 'Aside',
                    'attributes' => [
                        'key' => 'aside'
                    ]
                ],
                'social-1645' => [
                    'attributes' => [
                        'items' => NULL
                    ],
                    'block' => [
                        'variations' => 'align-right'
                    ]
                ]
            ]
        ]
    ]
];
