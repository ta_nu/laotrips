<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/custom/config/9/index.yaml',
    'modified' => 1511348816,
    'data' => [
        'name' => 9,
        'timestamp' => 1511348816,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1511338752
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'fixedside' => 'Fixedside',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'header' => 'Header',
            'aside' => 'Aside',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-6345' => 'Logo / Image',
                'logo-7657' => 'Logo'
            ],
            'menu' => [
                'menu-5072' => 'Menu'
            ],
            'social' => [
                'social-8943' => 'Social',
                'social-1645' => 'Social'
            ],
            'branding' => [
                'branding-5700' => 'Branding'
            ],
            'position' => [
                'position-position-2554' => 'Sidebar',
                'position-position-8830' => 'Aside'
            ],
            'messages' => [
                'system-messages-4126' => 'System Messages'
            ],
            'content' => [
                'system-content-1246' => 'Page Content'
            ],
            'module' => [
                'position-module-1423' => 'Module Instance'
            ],
            'copyright' => [
                'copyright-7902' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-5419' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'fixedside' => 'fixedside',
                'header' => 'header',
                'sidebar' => 'sidebar',
                'mainbar' => 'mainbar',
                'aside' => 'aside',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'logo-6345' => 'logo-6345',
                'menu-5072' => 'menu-5072',
                'social-8943' => 'social-8943',
                'branding-5700' => 'branding-5700',
                'logo-7657' => 'logo-7657',
                'position-position-2554' => 'position-position-2554',
                'system-messages-4126' => 'system-messages-4126',
                'system-content-1246' => 'system-content-1246',
                'position-module-1423' => 'position-module-3578',
                'position-position-8830' => 'position-position-8830',
                'copyright-7902' => 'copyright-7902',
                'social-1645' => 'social-1645',
                'mobile-menu-5419' => 'mobile-menu-5419'
            ]
        ]
    ]
];
