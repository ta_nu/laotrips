<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/custom/config/_error/layout.yaml',
    'modified' => 1511348816,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1503328712
        ],
        'layout' => [
            'fixedside' => [
                
            ],
            'header' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 20' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 60' => [
                            0 => [
                                0 => 'custom-2330'
                            ],
                            1 => [
                                0 => 'custom-4731'
                            ],
                            2 => [
                                0 => 'system-content-1246'
                            ],
                            3 => [
                                0 => 'system-messages-4126'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 20' => [
                            
                        ]
                    ]
                ]
            ],
            'footer' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'fixedside' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'header' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'home'
                ]
            ],
            'aside' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '',
                    'class' => '',
                    'extra' => [
                        
                    ]
                ]
            ],
            'footer' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'offcanvas' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'custom-2330' => [
                'title' => 'Error Img',
                'attributes' => [
                    'html' => '<div class="abovefooter-ct">
  <img src="gantry-media://error/404.png" alt="" /></div>'
                ]
            ],
            'custom-4731' => [
                'title' => 'Error text',
                'attributes' => [
                    'html' => '<div class="error-text">
  <h2>Whoops!</h2>
  <p>
      Sorry, the page could not be found. Visit the <a href="index.php">homepage</a>
  </p>
</div>'
                ]
            ]
        ]
    ]
];
