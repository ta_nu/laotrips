<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\tip/templates/jl_dream_free/custom/config/default/layout.yaml',
    'modified' => 1511348816,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1511338752
        ],
        'layout' => [
            '/fixedside/' => [
                0 => [
                    0 => 'logo-6345'
                ],
                1 => [
                    0 => 'menu-5072'
                ],
                2 => [
                    0 => 'social-8943'
                ],
                3 => [
                    0 => 'branding-5700'
                ]
            ],
            '/header/' => [
                0 => [
                    0 => 'logo-7657'
                ]
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 20' => [
                            0 => [
                                0 => 'position-position-2554'
                            ]
                        ]
                    ],
                    1 => [
                        'mainbar 60' => [
                            0 => [
                                0 => 'system-messages-4126'
                            ],
                            1 => [
                                0 => 'system-content-1246'
                            ],
                            2 => [
                                0 => 'position-module-3578'
                            ],
                            3 => [
                                0 => 'position-position-2615'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 20' => [
                            0 => [
                                0 => 'position-position-8830'
                            ]
                        ]
                    ]
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'copyright-7902 50',
                    1 => 'social-1645 50'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-5419'
                ]
            ]
        ],
        'structure' => [
            'fixedside' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '0'
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => '',
                    'class' => 'visible-phone'
                ]
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'home'
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '',
                    'class' => '',
                    'extra' => [
                        
                    ]
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => '',
                    'class' => 'visible-phone'
                ]
            ]
        ],
        'content' => [
            'logo-6345' => [
                'title' => 'Logo / Image'
            ],
            'social-8943' => [
                'attributes' => [
                    'items' => NULL
                ]
            ],
            'branding-5700' => [
                'attributes' => [
                    'content' => ' '
                ]
            ],
            'logo-7657' => [
                'attributes' => [
                    'image' => 'gantry-media://logo/logo.png'
                ]
            ],
            'position-position-2554' => [
                'title' => 'Sidebar',
                'attributes' => [
                    'key' => 'sidebar'
                ]
            ],
            'position-module-3578' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'key' => 'module-instance'
                ]
            ],
            'position-position-2615' => [
                'title' => 'Module Position',
                'attributes' => [
                    'key' => 'module-position1'
                ]
            ],
            'position-position-8830' => [
                'title' => 'Aside',
                'attributes' => [
                    'key' => 'aside'
                ]
            ],
            'social-1645' => [
                'attributes' => [
                    'items' => NULL
                ],
                'block' => [
                    'variations' => 'align-right'
                ]
            ]
        ]
    ]
];
