<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/custom/config/_error/index.yaml',
    'modified' => 1511348779,
    'data' => [
        'name' => '_error',
        'timestamp' => 1511348779,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1503328712
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'fixedside' => 'Fixedside',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'header' => 'Header',
            'aside' => 'Aside',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'custom' => [
                'custom-2330' => 'Error Img',
                'custom-4731' => 'Error text'
            ],
            'content' => [
                'system-content-1246' => 'Page Content'
            ],
            'messages' => [
                'system-messages-4126' => 'System Messages'
            ],
            'logo' => [
                'logo-7233' => 'Logo / Image',
                'logo-3079' => 'Logo'
            ],
            'menu' => [
                'menu-5307' => 'Menu'
            ],
            'social' => [
                'social-6085' => 'Social',
                'social-4136' => 'Social'
            ],
            'branding' => [
                'branding-4867' => 'Branding'
            ],
            'position' => [
                'position-position-7821' => 'Sidebar',
                'position-position-1418' => 'Aside'
            ],
            'copyright' => [
                'copyright-6960' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-4316' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'fixedside' => 'fixedside',
                'header' => 'header',
                'sidebar' => 'sidebar',
                'aside' => 'aside',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'logo-7233' => 'logo-6345',
                'menu-5307' => 'menu-5072',
                'social-6085' => 'social-8943',
                'branding-4867' => 'branding-5700',
                'logo-3079' => 'logo-7657',
                'position-position-7821' => 'position-position-2554',
                'position-position-1418' => 'position-position-8830',
                'copyright-6960' => 'copyright-7902',
                'social-4136' => 'social-1645',
                'mobile-menu-4316' => 'mobile-menu-5419'
            ]
        ]
    ]
];
