<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\tip/templates/jl_dream_free/config/default/styles.yaml',
    'modified' => 1511338752,
    'data' => [
        'preset' => 'preset1',
        'base' => [
            'background' => '#f5f7f9',
            'text-color' => '#666666',
            'text-active-color' => '#232529'
        ],
        'accent' => [
            'color-1' => '#888888',
            'color-2' => '#56c8d4'
        ],
        'font' => [
            'family-default' => 'family=Open+Sans:300,600,400',
            'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
        ],
        'menustyle' => [
            'text-color' => '#888888',
            'text-color-active' => '#56c8d4',
            'sublevel-text-color' => '#888888',
            'sublevel-text-color-active' => '#56c8d4',
            'sublevel-background' => '#ffffff'
        ],
        'fixedside' => [
            'background' => '#ffffff',
            'text-color' => '#222222',
            'width' => '18rem',
            'position' => 'left'
        ],
        'header' => [
            'background' => '#ffffff',
            'text-color' => '#222222'
        ],
        'main' => [
            'background' => '#f5f7f9',
            'text-color' => '#222222'
        ],
        'footer' => [
            'background' => '#212121',
            'text-color' => '#ffffff'
        ],
        'offcanvas' => [
            'background' => '#ffffff',
            'text-color' => '#222222',
            'toggle-color' => '#ffffff',
            'width' => '14rem',
            'toggle-visibility' => '1'
        ],
        'breakpoints' => [
            'large-desktop-container' => '85rem',
            'desktop-container' => '60rem',
            'tablet-container' => '51rem',
            'large-mobile-container' => '30rem',
            'mobile-menu-breakpoint' => '51rem'
        ],
        'menu' => [
            'col-width' => '200px',
            'animation' => 'g-zoom'
        ]
    ]
];
