<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\tip/templates/jl_dream_free/custom/config/_error/page/head.yaml',
    'modified' => 1511338752,
    'data' => [
        'atoms' => [
            0 => [
                'id' => 'assets-2958',
                'type' => 'assets',
                'title' => 'Custom CSS / JS',
                'attributes' => [
                    'enabled' => '1',
                    'css' => [
                        0 => [
                            'location' => 'gantry-assets://css/uikit.min.css',
                            'inline' => '',
                            'extra' => [
                                
                            ],
                            'priority' => '0',
                            'name' => 'Uikit'
                        ]
                    ],
                    'javascript' => [
                        0 => [
                            'location' => 'gantry-assets://js/uikit.min.js',
                            'inline' => '',
                            'in_footer' => '0',
                            'extra' => [
                                
                            ],
                            'priority' => '0',
                            'name' => 'Uikit'
                        ]
                    ]
                ]
            ],
            1 => [
                'type' => 'frameworks',
                'title' => 'JavaScript Frameworks',
                'id' => 'frameworks-6684',
                'inherit' => [
                    'outline' => 'default',
                    'atom' => 'frameworks-6684',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ]
        ]
    ]
];
