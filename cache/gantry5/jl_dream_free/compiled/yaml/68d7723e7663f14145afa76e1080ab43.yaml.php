<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/blueprints/styles/fixedside.yaml',
    'modified' => 1511338752,
    'data' => [
        'name' => 'FixedSide Styles',
        'description' => 'FixedSide styles for the Vking theme',
        'type' => 'section',
        'form' => [
            'fields' => [
                'background' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Background',
                    'default' => '#ffffff'
                ],
                'text-color' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Text',
                    'default' => '#222222'
                ],
                'width' => [
                    'type' => 'input.text',
                    'label' => 'FixedSide Width',
                    'default' => '14rem'
                ],
                'position' => [
                    'type' => 'select.selectize',
                    'label' => 'Position',
                    'description' => 'Select the FixedSide position.',
                    'default' => 'left',
                    'options' => [
                        'left' => 'Left',
                        'right' => 'Right'
                    ]
                ]
            ]
        ]
    ]
];
