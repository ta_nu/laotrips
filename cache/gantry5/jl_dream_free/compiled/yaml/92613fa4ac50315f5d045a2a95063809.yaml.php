<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\tip/templates/jl_dream_free/config/default/particles/social.yaml',
    'modified' => 1511343529,
    'data' => [
        'enabled' => '1',
        'css' => [
            'class' => ''
        ],
        'title' => '',
        'target' => '_blank',
        'items' => [
            0 => [
                'icon' => 'fa fa-facebook-square fa-fw',
                'text' => '',
                'link' => '#',
                'name' => 'Facebook'
            ],
            1 => [
                'icon' => 'fa fa-twitter-square fa-fw',
                'text' => '',
                'link' => '#',
                'name' => 'Twitter'
            ]
        ]
    ]
];
