<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\tip/templates/jl_dream_free/custom/config/_error/index.yaml',
    'modified' => 1511338753,
    'data' => [
        'name' => '_error',
        'timestamp' => 1511338752,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1503328712
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'fixedside' => 'Fixedside',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'header' => 'Header',
            'aside' => 'Aside',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'custom' => [
                'custom-2330' => 'Error Img',
                'custom-4731' => 'Error text'
            ],
            'content' => [
                'system-content-1246' => 'Page Content'
            ],
            'messages' => [
                'system-messages-4126' => 'System Messages'
            ],
            'logo' => [
                'logo-9302' => 'Logo / Image',
                'logo-9193' => 'Logo'
            ],
            'menu' => [
                'menu-8756' => 'Menu'
            ],
            'social' => [
                'social-9836' => 'Social',
                'social-1980' => 'Social'
            ],
            'branding' => [
                'branding-4613' => 'Branding'
            ],
            'position' => [
                'position-position-8723' => 'Sidebar',
                'position-position-4593' => 'Aside'
            ],
            'copyright' => [
                'copyright-9641' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-4703' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'fixedside' => 'fixedside',
                'header' => 'header',
                'sidebar' => 'sidebar',
                'aside' => 'aside',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'logo-9302' => 'logo-4547',
                'menu-8756' => 'menu-6591',
                'social-9836' => 'social-5621',
                'branding-4613' => 'branding-9659',
                'logo-9193' => 'logo-9208',
                'position-position-8723' => 'position-position-1289',
                'position-position-4593' => 'position-position-5879',
                'copyright-9641' => 'copyright-6131',
                'social-1980' => 'social-3565',
                'mobile-menu-4703' => 'mobile-menu-8340'
            ]
        ]
    ]
];
