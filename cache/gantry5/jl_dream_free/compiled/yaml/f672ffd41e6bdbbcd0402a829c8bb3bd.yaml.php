<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/gantry/presets.yaml',
    'modified' => 1511338752,
    'data' => [
        'preset1' => [
            'image' => 'gantry-admin://images/preset1.png',
            'description' => 'Preset 1',
            'colors' => [
                0 => '#888888',
                1 => '#56c8d4'
            ],
            'styles' => [
                'base' => [
                    'background' => '#f5f7f9',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#888888',
                    'color-2' => '#56c8d4'
                ],
                'menustyle' => [
                    'text-color' => '#888888',
                    'text-color-active' => '#56c8d4',
                    'sublevel-text-color' => '#888888',
                    'sublevel-text-color-active' => '#56c8d4',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#f5f7f9',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ],
        'preset2' => [
            'image' => 'gantry-admin://images/preset2.png',
            'description' => 'Preset 2',
            'colors' => [
                0 => '#4C3232',
                1 => '#C62727'
            ],
            'styles' => [
                'base' => [
                    'background' => '#F4E1E1',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#4C3232',
                    'color-2' => '#C62727'
                ],
                'menustyle' => [
                    'text-color' => '#4C3232',
                    'text-color-active' => '#C62727',
                    'sublevel-text-color' => '#4C3232',
                    'sublevel-text-color-active' => '#C62727',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#F4E1E1',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ],
        'preset3' => [
            'image' => 'gantry-admin://images/preset3.png',
            'description' => 'Preset 3',
            'colors' => [
                0 => '#303841',
                1 => '#EA9215'
            ],
            'styles' => [
                'base' => [
                    'background' => '#EEEEEE',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#303841',
                    'color-2' => '#EA9215'
                ],
                'menustyle' => [
                    'text-color' => '#303841',
                    'text-color-active' => '#EA9215',
                    'sublevel-text-color' => '#303841',
                    'sublevel-text-color-active' => '#EA9215',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#EEEEEE',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ],
        'preset4' => [
            'image' => 'gantry-admin://images/preset4.png',
            'description' => 'Preset 4',
            'colors' => [
                0 => '#3D262A',
                1 => '#127C56'
            ],
            'styles' => [
                'base' => [
                    'background' => '#ECF3F6',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#3D262A',
                    'color-2' => '#127C56'
                ],
                'menustyle' => [
                    'text-color' => '#3D262A',
                    'text-color-active' => '#127C56',
                    'sublevel-text-color' => '#3D262A',
                    'sublevel-text-color-active' => '#127C56',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#ECF3F6',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ],
        'preset5' => [
            'image' => 'gantry-admin://images/preset5.png',
            'description' => 'Preset 5',
            'colors' => [
                0 => '#342B2B',
                1 => '#90D26D'
            ],
            'styles' => [
                'base' => [
                    'background' => '#DEFBC2',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#342B2B',
                    'color-2' => '#90D26D'
                ],
                'menustyle' => [
                    'text-color' => '#342B2B',
                    'text-color-active' => '#90D26D',
                    'sublevel-text-color' => '#342B2B',
                    'sublevel-text-color-active' => '#90D26D',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#DEFBC2',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ],
        'preset6' => [
            'image' => 'gantry-admin://images/preset6.png',
            'description' => 'Preset 6',
            'colors' => [
                0 => '#45171D',
                1 => '#E84A5F'
            ],
            'styles' => [
                'base' => [
                    'background' => '#FECEA8',
                    'text-color' => '#666666',
                    'text-active-color' => '#232529'
                ],
                'accent' => [
                    'color-1' => '#45171D',
                    'color-2' => '#E84A5F'
                ],
                'menustyle' => [
                    'text-color' => '#45171D',
                    'text-color-active' => '#E84A5F',
                    'sublevel-text-color' => '#45171D',
                    'sublevel-text-color-active' => '#E84A5F',
                    'sublevel-background' => '#ffffff'
                ],
                'font' => [
                    'family-default' => 'family=Open+Sans:300,600,400',
                    'family-title' => 'family=Raleway:100,300,500,600,700,800,900,400'
                ],
                'header' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222'
                ],
                'main' => [
                    'background' => '#FECEA8',
                    'text-color' => '#222222'
                ],
                'footer' => [
                    'background' => '#212121',
                    'text-color' => '#ffffff'
                ],
                'offcanvas' => [
                    'background' => '#ffffff',
                    'text-color' => '#696969',
                    'toggle-color' => '#ffffff',
                    'width' => '14rem',
                    'toggle-visibility' => '1'
                ],
                'fixedside' => [
                    'background' => '#ffffff',
                    'text-color' => '#222222',
                    'width' => '18rem',
                    'position' => 'left'
                ],
                'breakpoints' => [
                    'large-desktop-container' => '85rem',
                    'desktop-container' => '60rem',
                    'tablet-container' => '51rem',
                    'large-mobile-container' => '30rem',
                    'mobile-menu-breakpoint' => '51rem'
                ]
            ]
        ]
    ]
];
