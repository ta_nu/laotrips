<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/custom/config/_offline/index.yaml',
    'modified' => 1511348779,
    'data' => [
        'name' => '_offline',
        'timestamp' => 1511348779,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1503328712
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'fixedside' => 'Fixedside',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'header' => 'Header',
            'aside' => 'Aside',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-4918' => 'Logo / Image',
                'logo-1038' => 'Logo'
            ],
            'menu' => [
                'menu-2110' => 'Menu'
            ],
            'social' => [
                'social-7476' => 'Social',
                'social-6216' => 'Social'
            ],
            'branding' => [
                'branding-5304' => 'Branding'
            ],
            'position' => [
                'position-position-4801' => 'Sidebar',
                'position-position-5207' => 'Aside'
            ],
            'messages' => [
                'system-messages-6741' => 'System Messages'
            ],
            'content' => [
                'system-content-5298' => 'Page Content'
            ],
            'module' => [
                'position-module-1677' => 'Module Instance'
            ],
            'sample' => [
                'sample-8606' => 'Sample Content'
            ],
            'copyright' => [
                'copyright-9640' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-1815' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'fixedside' => 'fixedside',
                'header' => 'header',
                'sidebar' => 'sidebar',
                'mainbar' => 'mainbar',
                'aside' => 'aside',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'logo-4918' => 'logo-6345',
                'menu-2110' => 'menu-5072',
                'social-7476' => 'social-8943',
                'branding-5304' => 'branding-5700',
                'logo-1038' => 'logo-7657',
                'position-position-4801' => 'position-position-2554',
                'system-messages-6741' => 'system-messages-4126',
                'system-content-5298' => 'system-content-1246',
                'position-module-1677' => 'position-module-3578',
                'position-position-5207' => 'position-position-8830',
                'copyright-9640' => 'copyright-7902',
                'social-6216' => 'social-1645',
                'mobile-menu-1815' => 'mobile-menu-5419'
            ]
        ]
    ]
];
