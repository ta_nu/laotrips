<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'gantry-theme://config/default/page/head.yaml',
    'modified' => 1511338752,
    'data' => [
        'meta' => [
            
        ],
        'head_bottom' => '',
        'atoms' => [
            0 => [
                'id' => 'sticky-1795',
                'type' => 'sticky',
                'title' => 'Sticky',
                'attributes' => [
                    'enabled' => '1',
                    'id' => 'g-header',
                    'spacing' => '0'
                ]
            ],
            1 => [
                'type' => 'frameworks',
                'title' => 'JavaScript Frameworks',
                'attributes' => [
                    'enabled' => '1',
                    'jquery' => [
                        'enabled' => '1',
                        'ui_core' => '1',
                        'ui_sortable' => '0'
                    ],
                    'bootstrap' => [
                        'enabled' => '0'
                    ],
                    'mootools' => [
                        'enabled' => '0',
                        'more' => '0'
                    ]
                ],
                'id' => 'frameworks-6684'
            ],
            2 => [
                'title' => 'Back To Top',
                'type' => 'backtotop',
                'attributes' => [
                    'enabled' => true,
                    'icon' => 'fa fa-angle-double-up'
                ],
                'id' => 'backtotop-6569'
            ],
            3 => [
                'id' => 'assets-9916',
                'type' => 'assets',
                'title' => 'Custom CSS / JS',
                'attributes' => [
                    'enabled' => '1',
                    'css' => [
                        
                    ],
                    'javascript' => [
                        0 => [
                            'location' => 'gantry-assets://js/theme.js',
                            'inline' => '',
                            'in_footer' => '0',
                            'extra' => [
                                
                            ],
                            'priority' => '0',
                            'name' => 'Theme Js'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
