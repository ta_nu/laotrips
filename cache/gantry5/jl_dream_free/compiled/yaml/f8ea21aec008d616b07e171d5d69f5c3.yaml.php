<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/tip/templates/jl_dream_free/custom/config/default/index.yaml',
    'modified' => 1511348816,
    'data' => [
        'name' => 'default',
        'timestamp' => 1511348816,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1511338752
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'module-position1' => 'Module Position',
            'aside' => 'Aside'
        ],
        'sections' => [
            'fixedside' => 'Fixedside',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'header' => 'Header',
            'aside' => 'Aside',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-6345' => 'Logo / Image',
                'logo-7657' => 'Logo'
            ],
            'menu' => [
                'menu-5072' => 'Menu'
            ],
            'social' => [
                'social-8943' => 'Social',
                'social-1645' => 'Social'
            ],
            'branding' => [
                'branding-5700' => 'Branding'
            ],
            'position' => [
                'position-position-2554' => 'Sidebar',
                'position-position-2615' => 'Module Position',
                'position-position-8830' => 'Aside'
            ],
            'messages' => [
                'system-messages-4126' => 'System Messages'
            ],
            'content' => [
                'system-content-1246' => 'Page Content'
            ],
            'module' => [
                'position-module-3578' => 'Module Instance'
            ],
            'copyright' => [
                'copyright-7902' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-5419' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
