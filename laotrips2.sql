-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2017 at 08:16 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laotrips2`
--

-- --------------------------------------------------------

--
-- Table structure for table `holp3_assets`
--

CREATE TABLE `holp3_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_assets`
--

INSERT INTO `holp3_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 141, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(8, 1, 17, 36, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(9, 1, 37, 38, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 39, 40, 1, 'com_installer', 'com_installer', '{\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 41, 44, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1}}'),
(12, 1, 45, 46, 1, 'com_login', 'com_login', '{}'),
(13, 1, 47, 48, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 49, 50, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 51, 52, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 53, 56, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1}}'),
(17, 1, 57, 58, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 59, 106, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1}}'),
(19, 1, 107, 110, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(20, 1, 111, 112, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1}}'),
(21, 1, 113, 114, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1}}'),
(22, 1, 115, 116, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 117, 118, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1}}'),
(24, 1, 119, 122, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1}}'),
(26, 1, 123, 124, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 27, 2, 'com_content.category.2', 'Uncategorised', '{}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{}'),
(30, 19, 108, 109, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{}'),
(32, 24, 120, 121, 2, 'com_users.category.7', 'Uncategorised', '{}'),
(33, 1, 125, 126, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 127, 128, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{}'),
(35, 1, 129, 130, 1, 'com_tags', 'com_tags', '{}'),
(36, 1, 131, 132, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 133, 134, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 135, 136, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 60, 61, 2, 'com_modules.module.1', 'Main Menu', '{}'),
(40, 18, 62, 63, 2, 'com_modules.module.2', 'Login', '{}'),
(41, 18, 64, 65, 2, 'com_modules.module.3', 'Popular Articles', '{}'),
(42, 18, 66, 67, 2, 'com_modules.module.4', 'Recently Added Articles', '{}'),
(43, 18, 68, 69, 2, 'com_modules.module.8', 'Toolbar', '{}'),
(44, 18, 70, 71, 2, 'com_modules.module.9', 'Quick Icons', '{}'),
(45, 18, 72, 73, 2, 'com_modules.module.10', 'Logged-in Users', '{}'),
(46, 18, 74, 75, 2, 'com_modules.module.12', 'Admin Menu', '{}'),
(47, 18, 76, 77, 2, 'com_modules.module.13', 'Admin Submenu', '{}'),
(48, 18, 78, 79, 2, 'com_modules.module.14', 'User Status', '{}'),
(49, 18, 80, 81, 2, 'com_modules.module.15', 'Title', '{}'),
(50, 18, 82, 83, 2, 'com_modules.module.16', 'Login Form', '{}'),
(51, 18, 84, 85, 2, 'com_modules.module.17', 'Breadcrumbs', '{}'),
(52, 18, 86, 87, 2, 'com_modules.module.79', 'Multilanguage status', '{}'),
(53, 18, 88, 89, 2, 'com_modules.module.86', 'Joomla Version', '{}'),
(54, 16, 54, 55, 2, 'com_menus.menu.1', 'Main Menu', '{}'),
(55, 18, 90, 91, 2, 'com_modules.module.87', 'Sample Data', '{}'),
(56, 11, 42, 43, 2, 'com_languages.language.2', 'Thai (th-TH)', '{}'),
(57, 18, 92, 93, 2, 'com_modules.module.88', 'Gantry 5 Particle', '{\"core.delete\":{\"1\":1},\"core.edit\":{\"1\":1},\"core.edit.state\":{\"1\":1},\"module.edit.frontend\":{\"1\":1}}'),
(58, 1, 137, 138, 1, 'com_gantry5', 'com_gantry5', '{}'),
(60, 27, 19, 20, 3, 'com_content.article.1', 'ທ່ຽວເມືອງລາວ Laotrips', '{}'),
(61, 63, 31, 32, 3, 'com_content.category.8', 'ທ່ຽວເມືອງລາວ Laotrips', '{}'),
(62, 63, 33, 34, 3, 'com_content.category.9', 'ຜາຫຼວງ (Blue lagoon and resort) ', '{}'),
(63, 8, 28, 35, 2, 'com_content.category.10', 'สอนภาษา', '{}'),
(64, 63, 29, 30, 3, 'com_content.article.2', 'สอนภาษา', '{}'),
(65, 18, 94, 95, 2, 'com_modules.module.89', 'Offcavas', '{}'),
(66, 18, 96, 97, 2, 'com_modules.module.90', 'images', '{}'),
(67, 18, 98, 99, 2, 'com_modules.module.91', 'Slideshow CK', '{}'),
(68, 18, 100, 101, 2, 'com_modules.module.92', 'ທ່ຽວເມືອງລາວ Laotrips', '{}'),
(69, 1, 139, 140, 1, 'com_creativeimageslider', 'COM_CREATIVEIMAGESLIDER', '{}'),
(70, 27, 21, 22, 3, 'com_content.article.3', 'ติดต่อเรา', '{}'),
(71, 27, 23, 24, 3, 'com_content.article.4', 'โรงแรม ที่พัก', '{}'),
(72, 27, 25, 26, 3, 'com_content.article.5', 'รูปภาพ', '{}'),
(73, 18, 102, 103, 2, 'com_modules.module.93', 'ที่พัก', '{}'),
(74, 18, 104, 105, 2, 'com_modules.module.94', 'ธรรมชาติ', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_associations`
--

CREATE TABLE `holp3_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_banners`
--

CREATE TABLE `holp3_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_banner_clients`
--

CREATE TABLE `holp3_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_banner_tracks`
--

CREATE TABLE `holp3_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_categories`
--

CREATE TABLE `holp3_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_categories`
--

INSERT INTO `holp3_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 17, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 108, '2017-11-22 08:14:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '<p><img src=\"https://imglp.com/2017/05/1699.jpg\" /></p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:14:53', 108, '2017-11-22 08:34:55', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:14:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:14:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:14:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:14:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 61, 10, 12, 13, 2, '2017-11-22-08-48-42/laotrips', 'com_content', 'ທ່ຽວເມືອງລາວ Laotrips', 'laotrips', '', '<h1 class=\"entry-title\">“ສວນນໍ້າປ່າພູເຂົາຄວາຍ” ແຫລ່ງທ່ອງທ່ຽວໃໝ່ທີ່ຄັ້ງໜຶ່ງຕ້ອງໄປສຳພັດ</h1>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://imglp.com/2017/05/59667.jpg\" /></p>\r\n<p>ທ່ອງທ່ຽວກັບລາວໂພສຕ໌ ມື້ນີ້ແອັດຈະຊວນທຸກທ່ານມາຫຼິ້ນນ້ຳໃຫ້ຫາຍຮ້ອນທີ່ເມືອງ ທຸລະຄົມ ແຂວງ ວຽງຈັນ ເຊິ່ງສະຖານທີ່ທີ່ແອັດຈະມາພາມານັ້ນກໍ່ຄື <strong>“ສວນນໍ້າປ່າພູເຂົາຄວາຍ” </strong>ເປັນສະຖານທີ່ທ່ອງທ່ຽວແຫ່ງໃໝ່ທີ່ຫາກໍ່ເປີດໃຫ້ບໍລິການໃນໄລຍະບຸນປີໃໝ່ລາວທີ່ຜ່ານມາ.</p>\r\n<p><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/1699.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89811 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/1699.jpg\" sizes=\"(max-width: 960px) 100vw, 960px\" srcset=\"https://imglp.com/2017/05/1699.jpg 960w, https://imglp.com/2017/05/1699-300x198.jpg 300w, https://imglp.com/2017/05/1699-768x508.jpg 768w, https://imglp.com/2017/05/1699-696x460.jpg 696w, https://imglp.com/2017/05/1699-635x420.jpg 635w\" alt=\"\" width=\"960\" height=\"635\" /></a></p>\r\n<p><strong>ສວນນໍ້າປ່າພູເຂົາຄວາຍ</strong> ຕັ້ງຢູ່ ບ້ານພູເຂົາຄວາຍໃໝ່ ເມືອງທຸລະຄົມ ແຂວງວຽງຈັນ. ທາງໄປເຄື່ອນໄຟຟ້ານໍ້າມັງ3 ເສັ້ນທາງເຂົ້າໄປຫາສວນນໍ້າແມ່ນສະດວກສະບາຍ ເປັນທາງປູຍາງໄປຮອດສວນເລີຍ ແລະ ເປັນອີກສະຖານທີ່ທ່ອງທ່ຽວອີກແຫ່ງໜຶ່ງ ທີ່ຢູ່ບໍ່ໄກຈາກນະຄອນຫຼວງວຽງຈັນ ໃຊ້ເວລາໃນການເດີນທາງຈາກນະຄອນຫຼວງວຽງຈັນໄປຫາສວນນໍ້າປະມານ 1 ຊົ່ວໂມງກັບອີກ 30 ນາທີ ກໍ່ມາຮອດແລ້ວ ທ່ານສາມາດມາສຳພັດຄວາມມ່ວນຊື່ນກັບສະຖານທີ່ແຫ່ງນີ້ໄດ້ ເຊິ່ງເພິ່ນຈະເປີດໃຫ້ບໍລິການທຸກວັນເສົາ-ອາທິດ ເລີ່ມເວລາ 10:00-18:00 ໂມງ ຫາກທ່ານໃດມີເວລາກໍ່ລອງມາສຳພັດເບິ່ງ ຮັບຮອງວ່າບໍ່ຜິດຫວັງ.</p>\r\n<p><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/1962.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89812 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/1962.jpg\" sizes=\"(max-width: 960px) 100vw, 960px\" srcset=\"https://imglp.com/2017/05/1962.jpg 960w, https://imglp.com/2017/05/1962-300x198.jpg 300w, https://imglp.com/2017/05/1962-768x508.jpg 768w, https://imglp.com/2017/05/1962-696x460.jpg 696w, https://imglp.com/2017/05/1962-635x420.jpg 635w\" alt=\"\" width=\"960\" height=\"635\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/7466.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89813 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/7466.jpg\" sizes=\"(max-width: 812px) 100vw, 812px\" srcset=\"https://imglp.com/2017/05/7466.jpg 812w, https://imglp.com/2017/05/7466-150x150.jpg 150w, https://imglp.com/2017/05/7466-300x300.jpg 300w, https://imglp.com/2017/05/7466-768x768.jpg 768w, https://imglp.com/2017/05/7466-696x696.jpg 696w, https://imglp.com/2017/05/7466-420x420.jpg 420w\" alt=\"\" width=\"812\" height=\"812\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/6112.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89814 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/6112.jpg\" sizes=\"(max-width: 576px) 100vw, 576px\" srcset=\"https://imglp.com/2017/05/6112.jpg 576w, https://imglp.com/2017/05/6112-150x150.jpg 150w, https://imglp.com/2017/05/6112-300x300.jpg 300w, https://imglp.com/2017/05/6112-420x420.jpg 420w\" alt=\"\" width=\"576\" height=\"576\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/7113.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89816 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/7113.jpg\" sizes=\"(max-width: 960px) 100vw, 960px\" srcset=\"https://imglp.com/2017/05/7113.jpg 960w, https://imglp.com/2017/05/7113-300x169.jpg 300w, https://imglp.com/2017/05/7113-768x432.jpg 768w, https://imglp.com/2017/05/7113-696x392.jpg 696w, https://imglp.com/2017/05/7113-747x420.jpg 747w\" alt=\"\" width=\"960\" height=\"540\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/7114.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89817 size-full td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/7114.jpg\" sizes=\"(max-width: 960px) 100vw, 960px\" srcset=\"https://imglp.com/2017/05/7114.jpg 960w, https://imglp.com/2017/05/7114-300x198.jpg 300w, https://imglp.com/2017/05/7114-768x508.jpg 768w, https://imglp.com/2017/05/7114-696x460.jpg 696w, https://imglp.com/2017/05/7114-635x420.jpg 635w\" alt=\"\" width=\"960\" height=\"635\" /></a></p>\r\n<p>ຂໍ້ມູນ ແລະ ຮູບພາບປະກອບຈາກ: ສວນນ້ຳປ່າພູເຂົາຄວາຍ</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:40:46', 108, '2017-11-22 08:56:23', 0, '*', 1),
(9, 62, 10, 14, 15, 2, '2017-11-22-08-48-42/blue-lagoon-and-resort', 'com_content', 'ຜາຫຼວງ (Blue lagoon and resort) ', 'blue-lagoon-and-resort', '', '<h1 class=\"entry-title\">ຜາຫຼວງ (Blue lagoon and resort) ມາທ່ຽວແລ້ວຈະຕິດໃຈ</h1>\r\n<div class=\"td-post-featured-image\"><a class=\"td-modal-image\" href=\"https://imglp.com/2017/05/Untitled.jpg\" data-caption=\"\"><img class=\"entry-thumb td-animation-stack-type0-2\" title=\"Untitled\" src=\"https://imglp.com/2017/05/Untitled.jpg\" sizes=\"(max-width: 627px) 100vw, 627px\" srcset=\"https://imglp.com/2017/05/Untitled.jpg 627w, https://imglp.com/2017/05/Untitled-300x168.jpg 300w\" alt=\"\" width=\"627\" height=\"352\" /></a></div>\r\n<p>ທ່ອງທ່ຽວກັບລາວໂພສຕ໌ ມື້ນີ້ເຮົາເດີນທາງມາທີ່ເມືອງເຟອງ ແຂວງ ວຽງຈັນ ມາຊົມທິວທັດທຳມະຊາດ ແລະ ມາຫາແຫຼ່ງກະໂດດນ້ຳໃຫ້ຄາຍຄວາມຮ້ອນກັນໄປເລີຍ ເຊິ່ງແນ່ນອນວ່າເມື່ອມາຮອດເມືອງເຟືອງແລ້ວ ສະຖານທີ່ທ່ຽວທີ່ເຮົາພາດບໍ່ໄດ້ນັ້ນກໍ່ຄື <strong>ຜາຫຼວງ (Blue lagoon and resort)</strong></p>\r\n<p>ການເດີນທາງມາ ຜາຫຼວງ  ພຽງແຕ່ທ່ານຂັບລົດອອກຈາກເມືອງວັງວຽງໄປທາງ ຜາເງິນ ທ່ານກໍ່ຈະມາເຖິງ (Blue lagoon and resort) ເຊິ່ງສະຖານທີ່ແຫ່ງນີ້ເປັນອີກໜຶ່ງສະຖານທີ່ທີ່ໄດ້ຮັບຄວາມນິຍົມສູງຈາກນັກທ່ອງທ່ຽວທັງພາຍໃນ ແລະ ຕ່າງປະເທດ ເພາະນອກຈາກຈະສະດວກສະບາຍທາງດ້ານສະຖານທີ່ພັກອາໃສແລ້ວ ເລື່ອງອາຫານການກິນກໍ່ບໍ່ຂາດເຂີນ ແລະ ຍັງມີກິດຈະກຳຫຼາຍຢ່າງໄວ້ໃຫ້ເລືອກຫຼິ້ນຢ່າງເຕັມອີ່ມເຊັ່ນ: ຂີ່ສະລິງ, ຫຼິ້ນນ້ຳ,​ ປີນພູ ແລະ ອື່ນໆ ຖ້າໃຜມັກຄວາມມັນແບບທ້າທາຍ ຮັບຮອງວ່າ ຜາຫຼວງ ມີຄຳຕອບທີ່ດີທີ່ສຸດໃຫ້ທ່ານ.</p>\r\n<p>ເອົາເປັນວ່າເຮົາບໍ່ເວົ້າຫຍັງຫຼາຍ ມາເບິ່ງຮູບພາບງາມໆນຳກັນເລີຍ</p>\r\n<p><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/18268397_1427906463935025_9166127538006293577_n.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89655 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/18268397_1427906463935025_9166127538006293577_n.jpg\" sizes=\"(max-width: 480px) 100vw, 480px\" srcset=\"https://imglp.com/2017/05/18268397_1427906463935025_9166127538006293577_n.jpg 480w, https://imglp.com/2017/05/18268397_1427906463935025_9166127538006293577_n-225x300.jpg 225w, https://imglp.com/2017/05/18268397_1427906463935025_9166127538006293577_n-315x420.jpg 315w\" alt=\"\" width=\"480\" height=\"640\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/18268473_702127539966496_3659583800389016185_n.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89656 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/18268473_702127539966496_3659583800389016185_n.jpg\" sizes=\"(max-width: 640px) 100vw, 640px\" srcset=\"https://imglp.com/2017/05/18268473_702127539966496_3659583800389016185_n.jpg 640w, https://imglp.com/2017/05/18268473_702127539966496_3659583800389016185_n-300x225.jpg 300w, https://imglp.com/2017/05/18268473_702127539966496_3659583800389016185_n-80x60.jpg 80w, https://imglp.com/2017/05/18268473_702127539966496_3659583800389016185_n-265x198.jpg 265w, https://imglp.com/2017/05/18268473_702127539966496_3659583800389016185_n-560x420.jpg 560w\" alt=\"\" width=\"640\" height=\"480\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/18300799_305185849902753_2924584744175011195_n.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89657 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/18300799_305185849902753_2924584744175011195_n.jpg\" sizes=\"(max-width: 640px) 100vw, 640px\" srcset=\"https://imglp.com/2017/05/18300799_305185849902753_2924584744175011195_n.jpg 640w, https://imglp.com/2017/05/18300799_305185849902753_2924584744175011195_n-300x225.jpg 300w, https://imglp.com/2017/05/18300799_305185849902753_2924584744175011195_n-80x60.jpg 80w, https://imglp.com/2017/05/18300799_305185849902753_2924584744175011195_n-265x198.jpg 265w, https://imglp.com/2017/05/18300799_305185849902753_2924584744175011195_n-560x420.jpg 560w\" alt=\"\" width=\"640\" height=\"480\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/18300889_1366770690082854_4114708776835169093_n.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89658 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/18300889_1366770690082854_4114708776835169093_n.jpg\" sizes=\"(max-width: 640px) 100vw, 640px\" srcset=\"https://imglp.com/2017/05/18300889_1366770690082854_4114708776835169093_n.jpg 640w, https://imglp.com/2017/05/18300889_1366770690082854_4114708776835169093_n-300x169.jpg 300w\" alt=\"\" width=\"640\" height=\"360\" /></a><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/05/18300987_1366770696749520_4209542542748387487_n.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-89659 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/05/18300987_1366770696749520_4209542542748387487_n.jpg\" sizes=\"(max-width: 640px) 100vw, 640px\" srcset=\"https://imglp.com/2017/05/18300987_1366770696749520_4209542542748387487_n.jpg 640w, https://imglp.com/2017/05/18300987_1366770696749520_4209542542748387487_n-300x169.jpg 300w\" alt=\"\" width=\"640\" height=\"360\" /></a></p>\r\n<p>ຮູບພາບປະກອບຈາກ: ໂນ່ ພຸດທະສັກ</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:41:44', 108, '2017-11-22 08:56:44', 0, '*', 1),
(10, 63, 1, 11, 16, 1, '2017-11-22-08-48-42', 'com_content', 'สอนภาษา', '2017-11-22-08-48-42', '', '<h1 class=\"entry-title\">“ສວນພຶກສາ” ສະຖານທີ່ທ່ຽວແຫ່ງໃໝ່ຂອງນະຄອນຫຼວງວຽງຈັນ</h1>\r\n<div class=\"td-post-featured-image\"><a class=\"td-modal-image\" href=\"https://imglp.com/2017/03/254.2.jpg\" data-caption=\"\"><img class=\"entry-thumb td-animation-stack-type0-2\" style=\"display: block; margin-left: auto; margin-right: auto;\" title=\"254.2\" src=\"https://imglp.com/2017/03/254.2-696x463.jpg\" sizes=\"(max-width: 696px) 100vw, 696px\" srcset=\"https://imglp.com/2017/03/254.2-696x463.jpg 696w, https://imglp.com/2017/03/254.2-300x200.jpg 300w, https://imglp.com/2017/03/254.2-768x511.jpg 768w, https://imglp.com/2017/03/254.2-631x420.jpg 631w, https://imglp.com/2017/03/254.2.jpg 960w\" alt=\"\" width=\"696\" height=\"463\" /></a></div>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 108, '2017-11-22 08:48:42', 0, '2017-11-22 08:48:42', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_cis_categories`
--

CREATE TABLE `holp3_cis_categories` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `ordering` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `holp3_cis_categories`
--

INSERT INTO `holp3_cis_categories` (`id`, `name`, `published`, `publish_up`, `publish_down`, `ordering`) VALUES
(1, 'Uncategorized', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_cis_images`
--

CREATE TABLE `holp3_cis_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_slider` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `img_name` text NOT NULL,
  `img_url` text NOT NULL,
  `readmoresize` text NOT NULL,
  `readmoreicon` text NOT NULL,
  `showreadmore` tinyint(3) UNSIGNED NOT NULL,
  `readmoretext` text NOT NULL,
  `readmorestyle` text NOT NULL,
  `overlaycolor` text NOT NULL,
  `overlayopacity` tinyint(3) UNSIGNED NOT NULL,
  `textcolor` text NOT NULL,
  `overlayfontsize` int(10) UNSIGNED NOT NULL,
  `textshadowcolor` text NOT NULL,
  `textshadowsize` tinyint(3) UNSIGNED NOT NULL,
  `showarrows` tinyint(3) UNSIGNED NOT NULL,
  `readmorealign` tinyint(3) UNSIGNED NOT NULL,
  `readmoremargin` text NOT NULL,
  `captionalign` tinyint(3) UNSIGNED NOT NULL,
  `captionmargin` text NOT NULL,
  `overlayusedefault` tinyint(3) UNSIGNED NOT NULL,
  `buttonusedefault` tinyint(3) UNSIGNED NOT NULL,
  `caption` text NOT NULL,
  `redirect_url` text NOT NULL,
  `redirect_itemid` int(10) UNSIGNED NOT NULL,
  `redirect_target` tinyint(3) UNSIGNED NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `created` datetime NOT NULL,
  `ordering` mediumint(8) UNSIGNED NOT NULL,
  `popup_img_name` text NOT NULL,
  `popup_img_url` text NOT NULL,
  `popup_open_event` tinyint(3) UNSIGNED NOT NULL DEFAULT '4'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `holp3_cis_images`
--

INSERT INTO `holp3_cis_images` (`id`, `id_user`, `id_slider`, `name`, `img_name`, `img_url`, `readmoresize`, `readmoreicon`, `showreadmore`, `readmoretext`, `readmorestyle`, `overlaycolor`, `overlayopacity`, `textcolor`, `overlayfontsize`, `textshadowcolor`, `textshadowsize`, `showarrows`, `readmorealign`, `readmoremargin`, `captionalign`, `captionmargin`, `overlayusedefault`, `buttonusedefault`, `caption`, `redirect_url`, `redirect_itemid`, `redirect_target`, `published`, `publish_up`, `publish_down`, `created`, `ordering`, `popup_img_name`, `popup_img_url`, `popup_open_event`) VALUES
(1, 0, 1, 'ທ່ຽວເມືອງລາວ Laotrips', 'images/10928838_667071930070683_585670321427441324_o.jpg', '#', 'normal', 'pencil', 0, '', 'red', '', 0, '', 0, '', 0, 0, 0, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By ທ່ຽວເມືອງລາວ Laotrips', '#', 101, 1, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'images/10928838_667071930070683_585670321427441324_o.jpg', '#', 0),
(2, 0, 1, 'ທ່ຽວເມືອງລາວ Laotrips', 'images/11096407_695361340575075_2569555569155628317_o.jpg', '#', 'normal', 'pencil', 0, '', 'red', '', 0, '', 0, '', 0, 0, 0, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By ທ່ຽວເມືອງລາວ Laotrips', '#', 101, 0, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 'images/11096407_695361340575075_2569555569155628317_o.jpg', '#', 0),
(3, 0, 1, 'ທ່ຽວເມືອງລາວ Laotrips', 'images/11121312_695361273908415_6184494388043597167_o.jpg', '#', 'normal', 'pencil', 0, '', 'red', '', 0, '', 0, '', 0, 0, 0, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By ທ່ຽວເມືອງລາວ Laotrips', '#', 101, 0, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 'images/11121312_695361273908415_6184494388043597167_o.jpg', '#', 0),
(4, 0, 1, 'ທ່ຽວເມືອງລາວ Laotrips', 'images/11143544_695361250575084_8705418924721668613_o.jpg', '#', 'normal', 'pencil', 0, '', 'red', '', 0, '', 0, '', 0, 0, 0, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By ທ່ຽວເມືອງລາວ Laotrips', '#', 101, 0, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 'images/11143544_695361250575084_8705418924721668613_o.jpg', '#', 0),
(5, 0, 1, 'ທ່ຽວເມືອງລາວ Laotrips', 'images/13179305_879548355489705_6732883611063976483_n.jpg', '#', 'normal', 'pencil', 0, '', 'red', '', 0, '', 0, '', 0, 0, 0, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By ທ່ຽວເມືອງລາວ Laotrips', '#', 101, 0, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 'images/13179305_879548355489705_6732883611063976483_n.jpg', '#', 0),
(6, 0, 1, 'Face to face with nature...', '', 'http://creative-solutions.net/images/sliders/face-to-face-with-nature/item6-tmb.jpg', 'normal', 'pencil', 1, 'Read More!', 'red', '#000000', 50, '#ffffff', 18, '#000000', 2, 0, 1, '0px 10px 10px 10px', 0, '10px 15px 10px 15px', 0, 0, 'By <a href=\"http://creative-solutions.net/joomla/creative-image-slider\" target=\"_blank\">Creative Image Slider...</a>', '#', 104, 0, 1, '2017-11-22 10:53:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, '', 'http://creative-solutions.net/images/sliders/face-to-face-with-nature/item6.jpg', 4);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_cis_sliders`
--

CREATE TABLE `holp3_cis_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_template` smallint(5) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `width` text NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `margintop` int(10) UNSIGNED NOT NULL,
  `marginbottom` int(10) UNSIGNED NOT NULL,
  `itemsoffset` int(10) UNSIGNED NOT NULL,
  `paddingtop` int(10) UNSIGNED NOT NULL,
  `paddingbottom` int(10) UNSIGNED NOT NULL,
  `bgcolor` text NOT NULL,
  `readmoresize` text NOT NULL,
  `readmoreicon` text NOT NULL,
  `showreadmore` tinyint(3) UNSIGNED NOT NULL,
  `readmoretext` text NOT NULL,
  `readmorestyle` text NOT NULL,
  `overlaycolor` text NOT NULL,
  `overlayopacity` tinyint(3) UNSIGNED NOT NULL,
  `textcolor` text NOT NULL,
  `overlayfontsize` int(10) UNSIGNED NOT NULL,
  `textshadowcolor` text NOT NULL,
  `textshadowsize` tinyint(3) UNSIGNED NOT NULL,
  `showarrows` tinyint(3) UNSIGNED NOT NULL,
  `readmorealign` tinyint(3) UNSIGNED NOT NULL,
  `readmoremargin` text NOT NULL,
  `captionalign` tinyint(3) UNSIGNED NOT NULL,
  `captionmargin` text NOT NULL,
  `alias` text NOT NULL,
  `created` datetime NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `access` int(10) UNSIGNED NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL,
  `ordering` int(11) NOT NULL,
  `language` char(7) NOT NULL,
  `arrow_template` smallint(5) UNSIGNED NOT NULL DEFAULT '37',
  `arrow_width` smallint(5) UNSIGNED NOT NULL DEFAULT '32',
  `arrow_left_offset` smallint(5) UNSIGNED NOT NULL DEFAULT '10',
  `arrow_center_offset` smallint(6) NOT NULL DEFAULT '0',
  `arrow_passive_opacity` smallint(5) UNSIGNED NOT NULL DEFAULT '70',
  `move_step` int(10) UNSIGNED NOT NULL DEFAULT '600',
  `move_time` int(10) UNSIGNED NOT NULL DEFAULT '600',
  `move_ease` int(10) UNSIGNED NOT NULL DEFAULT '60',
  `autoplay` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `autoplay_start_timeout` int(10) UNSIGNED NOT NULL DEFAULT '3000',
  `autoplay_step_timeout` int(10) UNSIGNED NOT NULL DEFAULT '5000',
  `autoplay_evenly_speed` int(10) UNSIGNED NOT NULL DEFAULT '28',
  `autoplay_hover_timeout` int(10) UNSIGNED NOT NULL DEFAULT '800',
  `overlayanimationtype` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `popup_max_size` tinyint(3) UNSIGNED NOT NULL DEFAULT '90',
  `popup_item_min_width` smallint(5) UNSIGNED NOT NULL DEFAULT '300',
  `popup_use_back_img` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_arrow_passive_opacity` tinyint(3) UNSIGNED NOT NULL DEFAULT '70',
  `popup_arrow_left_offset` tinyint(3) UNSIGNED NOT NULL DEFAULT '12',
  `popup_arrow_min_height` tinyint(3) UNSIGNED NOT NULL DEFAULT '25',
  `popup_arrow_max_height` tinyint(3) UNSIGNED NOT NULL DEFAULT '50',
  `popup_showarrows` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_image_order_opacity` tinyint(3) UNSIGNED NOT NULL DEFAULT '70',
  `popup_image_order_top_offset` tinyint(3) UNSIGNED NOT NULL DEFAULT '12',
  `popup_show_orderdata` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_icons_opacity` tinyint(3) UNSIGNED NOT NULL DEFAULT '50',
  `popup_show_icons` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_autoplay_default` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_closeonend` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `popup_autoplay_time` int(10) UNSIGNED NOT NULL DEFAULT '5000',
  `popup_open_event` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `link_open_event` tinyint(3) UNSIGNED NOT NULL DEFAULT '3',
  `cis_touch_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `cis_inf_scroll_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `cis_mouse_scroll_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `cis_item_correction_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `cis_animation_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `cis_item_hover_effect` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `cis_overlay_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `cis_touch_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `cis_font_family` text NOT NULL,
  `cis_font_effect` text NOT NULL,
  `cis_items_appearance_effect` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `icons_size` tinyint(3) UNSIGNED NOT NULL DEFAULT '30',
  `icons_margin` tinyint(3) UNSIGNED NOT NULL DEFAULT '10',
  `icons_offset` tinyint(3) UNSIGNED NOT NULL DEFAULT '5',
  `icons_animation` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `icons_color` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `icons_valign` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `ov_items_offset` tinyint(3) UNSIGNED NOT NULL DEFAULT '10',
  `ov_items_m_offset` smallint(6) NOT NULL DEFAULT '0',
  `cis_button_font_family` text NOT NULL,
  `custom_css` text NOT NULL,
  `custom_js` text NOT NULL,
  `slider_full_size` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `holp3_cis_sliders`
--

INSERT INTO `holp3_cis_sliders` (`id`, `id_user`, `id_category`, `id_template`, `name`, `width`, `height`, `margintop`, `marginbottom`, `itemsoffset`, `paddingtop`, `paddingbottom`, `bgcolor`, `readmoresize`, `readmoreicon`, `showreadmore`, `readmoretext`, `readmorestyle`, `overlaycolor`, `overlayopacity`, `textcolor`, `overlayfontsize`, `textshadowcolor`, `textshadowsize`, `showarrows`, `readmorealign`, `readmoremargin`, `captionalign`, `captionmargin`, `alias`, `created`, `publish_up`, `publish_down`, `published`, `checked_out`, `checked_out_time`, `access`, `featured`, `ordering`, `language`, `arrow_template`, `arrow_width`, `arrow_left_offset`, `arrow_center_offset`, `arrow_passive_opacity`, `move_step`, `move_time`, `move_ease`, `autoplay`, `autoplay_start_timeout`, `autoplay_step_timeout`, `autoplay_evenly_speed`, `autoplay_hover_timeout`, `overlayanimationtype`, `popup_max_size`, `popup_item_min_width`, `popup_use_back_img`, `popup_arrow_passive_opacity`, `popup_arrow_left_offset`, `popup_arrow_min_height`, `popup_arrow_max_height`, `popup_showarrows`, `popup_image_order_opacity`, `popup_image_order_top_offset`, `popup_show_orderdata`, `popup_icons_opacity`, `popup_show_icons`, `popup_autoplay_default`, `popup_closeonend`, `popup_autoplay_time`, `popup_open_event`, `link_open_event`, `cis_touch_enabled`, `cis_inf_scroll_enabled`, `cis_mouse_scroll_enabled`, `cis_item_correction_enabled`, `cis_animation_type`, `cis_item_hover_effect`, `cis_overlay_type`, `cis_touch_type`, `cis_font_family`, `cis_font_effect`, `cis_items_appearance_effect`, `icons_size`, `icons_margin`, `icons_offset`, `icons_animation`, `icons_color`, `icons_valign`, `ov_items_offset`, `ov_items_m_offset`, `cis_button_font_family`, `custom_css`, `custom_js`, `slider_full_size`) VALUES
(1, 0, 1, 1, 'Nature [Slider Example]', '100%', 250, 0, 0, 2, 2, 2, '#ffffff', 'small', 'none', 1, 'View Image', 'black', '#ffffff', 60, '#fcfcfc', 18, '#ffffff', 3, 1, 2, '0px 10px 10px 10px', 2, '10px 15px 20px 15px', '', '0000-00-00 00:00:00', '2017-11-22 10:38:12', '0000-00-00 00:00:00', 1, 0, '0000-00-00 00:00:00', 0, 0, 1, '', 39, 35, 10, 0, 50, 25, 600, 60, 0, 3000, 5000, 25, 800, 0, 90, 150, 1, 50, 12, 30, 50, 1, 70, 12, 1, 50, 1, 1, 1, 5000, 0, 3, 0, 0, 0, 1, 0, 2, 0, 2, 'Arial, Helvetica, sans-serif', 'cis_font_effect_none', 1, 40, 15, 5, 2, 1, 0, 20, 0, 'Arial, Helvetica, sans-serif', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_cis_templates`
--

CREATE TABLE `holp3_cis_templates` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `styles` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `holp3_cis_templates`
--

INSERT INTO `holp3_cis_templates` (`id`, `name`, `styles`, `published`, `publish_up`, `publish_down`) VALUES
(1, 'Test Template', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_contact_details`
--

CREATE TABLE `holp3_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` mediumtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if contact is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_content`
--

CREATE TABLE `holp3_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_content`
--

INSERT INTO `holp3_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 60, 'ທ່ຽວເມືອງລາວ Laotrips', 'laotrips', '<h1 class=\"entry-title\">“ສວນນໍ້າປ່າພູເຂົາຄວາຍ” ແຫລ່ງທ່ອງທ່ຽວໃໝ່ທີ່ຄັ້ງໜຶ່ງຕ້ອງໄປສຳພັດ</h1>\r\n<p><img src=\"https://imglp.com/2017/05/59667.jpg\" /></p>\r\n<p>ທ່ອງທ່ຽວກັບລາວໂພສຕ໌ ມື້ນີ້ແອັດຈະຊວນທຸກທ່ານມາຫຼິ້ນນ້ຳໃຫ້ຫາຍຮ້ອນທີ່ເມືອງ ທຸລະຄົມ ແຂວງ ວຽງຈັນ ເຊິ່ງສະຖານທີ່ທີ່ແອັດຈະມາພາມານັ້ນກໍ່ຄື <strong>“ສວນນໍ້າປ່າພູເຂົາຄວາຍ” </strong>ເປັນສະຖານທີ່ທ່ອງທ່ຽວແຫ່ງໃໝ່ທີ່ຫາກໍ່ເປີດໃຫ້ບໍລິການໃນໄລຍະບຸນປີໃໝ່ລາວທີ່ຜ່ານມາ.</p>\r\n<p><img src=\"https://imglp.com/2017/05/1699.jpg\" /></p>\r\n<p><strong>ສວນນໍ້າປ່າພູເຂົາຄວາຍ</strong> ຕັ້ງຢູ່ ບ້ານພູເຂົາຄວາຍໃໝ່ ເມືອງທຸລະຄົມ ແຂວງວຽງຈັນ. ທາງໄປເຄື່ອນໄຟຟ້ານໍ້າມັງ3 ເສັ້ນທາງເຂົ້າໄປຫາສວນນໍ້າແມ່ນສະດວກສະບາຍ ເປັນທາງປູຍາງໄປຮອດສວນເລີຍ ແລະ ເປັນອີກສະຖານທີ່ທ່ອງທ່ຽວອີກແຫ່ງໜຶ່ງ ທີ່ຢູ່ບໍ່ໄກຈາກນະຄອນຫຼວງວຽງຈັນ ໃຊ້ເວລາໃນການເດີນທາງຈາກນະຄອນຫຼວງວຽງຈັນໄປຫາສວນນໍ້າປະມານ 1 ຊົ່ວໂມງກັບອີກ 30 ນາທີ ກໍ່ມາຮອດແລ້ວ ທ່ານສາມາດມາສຳພັດຄວາມມ່ວນຊື່ນກັບສະຖານທີ່ແຫ່ງນີ້ໄດ້ ເຊິ່ງເພິ່ນຈະເປີດໃຫ້ບໍລິການທຸກວັນເສົາ-ອາທິດ ເລີ່ມເວລາ 10:00-18:00 ໂມງ ຫາກທ່ານໃດມີເວລາກໍ່ລອງມາສຳພັດເບິ່ງ ຮັບຮອງວ່າບໍ່ຜິດຫວັງ.</p>', '', 2, 2, '2017-11-22 08:33:29', 108, '', '2017-11-22 08:33:32', 108, 0, '0000-00-00 00:00:00', '2017-11-22 08:33:29', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 3, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(2, 64, 'สอนภาษา', '2017-11-22-08-49-01', '<h1 class=\"entry-title\">“ສວນພຶກສາ” ສະຖານທີ່ທ່ຽວແຫ່ງໃໝ່ຂອງນະຄອນຫຼວງວຽງຈັນ</h1>\r\n<div class=\"td-post-featured-image\"><a class=\"td-modal-image\" href=\"https://imglp.com/2017/03/254.2.jpg\" data-caption=\"\"><img class=\"entry-thumb td-animation-stack-type0-2\" title=\"254.2\" src=\"https://imglp.com/2017/03/254.2-696x463.jpg\" sizes=\"(max-width: 696px) 100vw, 696px\" srcset=\"https://imglp.com/2017/03/254.2-696x463.jpg 696w, https://imglp.com/2017/03/254.2-300x200.jpg 300w, https://imglp.com/2017/03/254.2-768x511.jpg 768w, https://imglp.com/2017/03/254.2-631x420.jpg 631w, https://imglp.com/2017/03/254.2.jpg 960w\" alt=\"\" width=\"696\" height=\"463\" /></a></div>\r\n<p>ທ່ອງທ່ຽວກັບລາວໂພສຕ໌ ມື້ນີ້ແອັດມິນຈະມາແນະນຳບ່ອນຫຼິ້ນ, ບ່ອນກິນ, ບ່ອນທ່ຽວ, ບ່ອນຖ່າຍເຊລຟີ ສະຖານທີ່ທ່ອງທ່ຽວແຫ່ງໃໝ່ຂອງນະຄອນຫຼວງວຽງຈັນ ທີ່ຈະໄດ້ເປີດໃຫ້ເຂົ້າທ່ຽວຊົມຢ່າງເປັນທາງການໃນວັນທີ 7 ເມາສາ 2017 ນີ້ ປີໃໝ່ນີ້ຖ້າບໍ່ຮູ້ວ່າສິໄປທາງໃດດີລອງມາແວ່ທ່ຽວຊົມສະຖານທີ່ແຫ່ງນີ້ເບິ່່ງຮັບຮອງວ່າທ່ານຈະບໍ່ຜິດຫວັງ.</p>\r\n<p>ຖ້າໃຜຕ້ອງການໄປທ່ຽວຊົມສະຖານທີ່ຕັ້ງຢູ່ ບ້ານໃຫມ່ດານຊີ, ເມືອງ ນາຊາຍທອງ, ນະຄອນຫລວງວຽງຈັນ</p>\r\n<p><a href=\"https://laopost.com/goto/https://www.laopost.com/wp-content/uploads/2017/03/228.jpg\" target=\"_blank\" rel=\"nofollow noopener noreferrer\"><img class=\"aligncenter wp-image-87801 size-full td-animation-stack-type0-2\" src=\"https://www.laopost.com/wp-content/uploads/2017/03/228.jpg\" sizes=\"(max-width: 960px) 100vw, 960px\" srcset=\"https://imglp.com/2017/03/228.jpg 960w, https://imglp.com/2017/03/228-300x200.jpg 300w, https://imglp.com/2017/03/228-768x511.jpg 768w, https://imglp.com/2017/03/228-696x463.jpg 696w, https://imglp.com/2017/03/228-631x420.jpg 631w\" alt=\"\" width=\"960\" height=\"639\" /></a></p>\r\n<p>ນອກຈາກດອກໄມ້ນາໆຊະນິດແລ້ວ ທ່ານຍັງຈະສຳຜັດກັບຫມາກແຕງ ຫລືເມລອນຢີ່ປຸ່ນ ທີ່ປູກແບບບໍ່ມີຢາຂ້າແມງໄມ້ແລະປຸຍຕ່າງໆ ຮັບຮອງວ່າດີຕໍ່ສຸຂະພາບຢ່າງແນ່ນອນ ແລະ ຖ້າໃຜມັກນັ່ງງອຍຂາຕຶກປາຢູ່ແຄມ ຢູ່ທີ່ນີ້ເພິ່ນມີຕູມນ້ອຍໄວ້ໃຫ້ເຊົ່ານັ່ງຕຶກປາຊຸມແຊວຢ່າງສະບາຍໃຈ, ສ່ວນຄົນທີ່ທ່ານຮູບໄປໃສກໍ່ຕ້ອງຖ່າຍເຊລຟີ ແລ້ວໂພສຕ໌ລົງໂຊຊຽວຢູ່ຕະຫຼອດ ຕ້ອງມາທີ່ນີ້ເລີຍທ່ານຈະບໍ່ຜິດຫວັງ ເພາະມີຫຼາຍມຸມໃຫ້ເລືອກຖ່າຍຮູບຢ່າງເຕັມອີ່ມ ເວົ້າແລ້ວເຮົາກໍ່ບໍ່ຊັກຊ້າເສຍເວລາ ເຮົາມາເບິ່ງຮູບບັນຍາກາດ ຂອງສວນພຶກສາ ໄປພ້ອມໆກັນເລີຍ</p>', '', 1, 10, '2017-11-22 08:49:01', 108, '', '2017-11-22 08:49:01', 0, 0, '0000-00-00 00:00:00', '2017-11-22 08:49:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 0, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(3, 70, 'ติดต่อเรา', '2017-11-23-04-35-25', '<h1>ติดต่อเรา</h1>\r\n<p> </p>\r\n<p> <strong>โทร :1234567890</strong></p>\r\n<p><strong> Email: laos@hotmail.com</strong></p>\r\n<p> </p>', '', 1, 2, '2017-11-23 04:35:25', 108, '', '2017-11-23 04:39:24', 108, 0, '0000-00-00 00:00:00', '2017-11-23 04:35:25', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"spfeatured_image\":\"\",\"post_format\":\"standard\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 2, 2, '', '', 1, 10, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(4, 71, 'โรงแรม ที่พัก', '2017-11-23-04-51-38', '<p>ໂຕະບາບີຄິວ ແຄມສາຍນໍ້າກັດ</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"images/21463172_1532559043498049_6379071660908574067_n.jpg\" alt=\"\" /></p>\r\n<p>ECO King Bed<img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"images/21199826_1524946854259268_406583625877263173_o.jpg\" alt=\"\" width=\"1474\" height=\"984\" /></p>\r\n<p> </p>\r\n<p><img src=\"images/19990140_1466245273462760_9070176813509825167_n.jpg\" alt=\"\" /></p>\r\n<p>Chill out near by the pool</p>\r\n<p><img src=\"images/20368832_1481736258580328_1616547803102042229_o.jpg\" alt=\"\" /></p>', '', 1, 2, '2017-11-23 04:51:38', 108, '', '2017-11-23 04:51:38', 0, 0, '0000-00-00 00:00:00', '2017-11-23 04:51:38', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"spfeatured_image\":\"\",\"post_format\":\"standard\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 1, 1, '', '', 1, 7, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(5, 72, 'รูปภาพ', '2017-11-23-06-25-56', '', '', 1, 2, '2017-11-23 06:25:56', 108, '', '2017-11-23 06:31:10', 108, 0, '0000-00-00 00:00:00', '2017-11-23 06:25:56', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/17192460_1299233076830648_2881960565113594214_o.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"spfeatured_image\":\"images\\/2560\\/11\\/23\\/20368832_1481736258580328_1616547803102042229_o1.jpg\",\"post_format\":\"gallery\",\"gallery\":\"{\\\"gallery_images\\\":[\\\"images\\/2560\\/11\\/23\\/20368832_1481736258580328_1616547803102042229_o.jpg\\\",\\\"images\\/2560\\/11\\/23\\/21199826_1524946854259268_406583625877263173_o.jpg\\\",\\\"images\\/2560\\/11\\/23\\/17855493_1336298459790776_1328872890552281111_o.jpg\\\",\\\"images\\/2560\\/11\\/23\\/20645243_1497267137027240_7655830566598971746_o.jpg\\\",\\\"images\\/2560\\/11\\/23\\/13179305_879548355489705_6732883611063976483_n.jpg\\\",\\\"images\\/2560\\/11\\/23\\/10928838_667071930070683_585670321427441324_o.jpg\\\"]}\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 3, 0, '', '', 1, 9, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_contentitem_tag_map`
--

CREATE TABLE `holp3_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `holp3_content_frontpage`
--

CREATE TABLE `holp3_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_content_rating`
--

CREATE TABLE `holp3_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_content_types`
--

CREATE TABLE `holp3_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_content_types`
--

INSERT INTO `holp3_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(4, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(6, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(9, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(11, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(12, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(13, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_core_log_searches`
--

CREATE TABLE `holp3_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_extensions`
--

CREATE TABLE `holp3_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_extensions`
--

INSERT INTO `holp3_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"contact_layout\":\"_:default\",\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_tags\":\"1\",\"show_info\":\"1\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_image\":\"1\",\"show_misc\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"articles_display_num\":\"10\",\"show_profile\":\"0\",\"show_user_custom_fields\":[\"-1\"],\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"0\",\"maxLevel\":\"-1\",\"show_subcat_desc\":\"1\",\"show_empty_categories\":\"0\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_subcat_desc_cat\":\"1\",\"show_empty_categories_cat\":\"0\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"0\",\"show_pagination_limit\":\"0\",\"show_headings\":\"1\",\"show_image_heading\":\"0\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"initial_sort\":\"ordering\",\"captcha\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"0\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_feed_link\":\"1\",\"sef_advanced\":0,\"sef_ids\":0,\"custom_fields_enable\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '{\"show_jed_info\":\"1\",\"cachetimeout\":\"6\",\"minimum_stability\":\"4\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"en-GB\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"search_phrases\":\"1\",\"search_areas\":\"1\",\"show_date\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"0\",\"upload_limit\":\"10\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css,scss,sass,yaml,twig\",\"font_formats\":\"woff,ttf,otf,eot,svg\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"1\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"10\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"12\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"1\",\"useractivation\":\"2\",\"mail_to_admin\":\"1\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"enabled\":\"0\",\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_autosuggest\":\"1\",\"show_suggested_query\":\"1\",\"show_explained_query\":\"1\",\"show_advanced\":\"1\",\"show_advanced_tips\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"sort_order\":\"relevance\",\"sort_direction\":\"desc\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stem\":\"1\",\"stemmer\":\"snowball\",\"enable_logging\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '{\"updatesource\":\"default\",\"customurl\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"Januar 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"16c1f80cd0fb5b4b7e9c5a5b0e835746\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(318, 0, 'mod_sampledata', 'module', 'mod_sampledata', '', 1, 1, 1, 0, '{\"name\":\"mod_sampledata\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"MOD_SAMPLEDATA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sampledata\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0);
INSERT INTO `holp3_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.30.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2017\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.5.7\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"configuration\":{\"toolbars\":{\"2\":{\"toolbar1\":[\"bold\",\"underline\",\"strikethrough\",\"|\",\"undo\",\"redo\",\"|\",\"bullist\",\"numlist\",\"|\",\"pastetext\"]},\"1\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"formatselect\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"code\",\"|\",\"hr\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"pastetext\",\"preview\"]},\"0\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"styleselect\",\"|\",\"formatselect\",\"fontselect\",\"fontsizeselect\",\"|\",\"searchreplace\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"image\",\"|\",\"code\",\"|\",\"forecolor\",\"backcolor\",\"|\",\"fullscreen\",\"|\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"emoticons\",\"media\",\"hr\",\"ltr\",\"rtl\",\"|\",\"cut\",\"copy\",\"paste\",\"pastetext\",\"|\",\"visualchars\",\"visualblocks\",\"nonbreaking\",\"blockquote\",\"template\",\"|\",\"print\",\"preview\",\"codesample\",\"insertdatetime\",\"removeformat\"]}},\"setoptions\":{\"2\":{\"access\":[\"1\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"1\":{\"access\":[\"6\",\"2\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"0\":{\"access\":[\"7\",\"4\",\"8\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}}},\"sets_amount\":3,\"html_height\":\"550\",\"html_width\":\"750\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1511408684}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '{\"mode\":1,\"lastrun\":1511408715,\"unique_id\":\"f9eeeaa0f64b573a1cd22e0ebd0616d00241dab2\",\"interval\":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}', '{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(479, 0, 'plg_sampledata_blog', 'plugin', 'blog', 'sampledata', 0, 1, 1, 0, '{\"name\":\"plg_sampledata_blog\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"PLG_SAMPLEDATA_BLOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"blog\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 0, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 0, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `holp3_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10000, 10002, 'Thaith-TH', 'language', 'th-TH', '', 0, 1, 0, 0, '{\"name\":\"Thai (th-TH)\",\"type\":\"language\",\"creationDate\":\"October 2017\",\"author\":\"Thai Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters & JoomlaCorner.com. All rights reserved.\",\"authorEmail\":\"tt@joomlacorner.com\",\"authorUrl\":\"www.joomlacorner.com\",\"version\":\"3.8.1.1\",\"description\":\"\\n    <div align=\\\"center\\\">\\n      <table border=\\\"0\\\" width=\\\"100%\\\">\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"100%\\\" colspan=\\\"2\\\">\\n  \\t\\t      <div class=\\\"alert alert-success center\\\" align=\\\"center\\\">\\n  \\t\\t        <h3 class=\\\"alert-heading\\\">\\u0e20\\u0e32\\u0e29\\u0e32\\u0e44\\u0e17\\u0e22\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 3.8 \\u0e08\\u0e32\\u0e01\\u0e17\\u0e35\\u0e21\\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c <br \\/>\\u0e20\\u0e32\\u0e22\\u0e43\\u0e15\\u0e49\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e19\\u0e31\\u0e1a\\u0e2a\\u0e19\\u0e38\\u0e19\\u0e02\\u0e2d\\u0e07 \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14<\\/h3>\\n  \\t\\t      <\\/div>\\n  \\t\\t      <hr \\/>\\n  \\t\\t    <\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e23\\u0e32\\u0e22\\u0e25\\u0e30\\u0e40\\u0e2d\\u0e35\\u0e22\\u0e14 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">\\u0e20\\u0e32\\u0e29\\u0e32\\u0e44\\u0e17\\u0e22\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c (\\u0e14\\u0e49\\u0e32\\u0e19\\u0e2b\\u0e19\\u0e49\\u0e32\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c)<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e44\\u0e1f\\u0e25\\u0e4c\\u0e20\\u0e32\\u0e29\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8.1v1<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <td width=\\\"100%\\\" colspan=\\\"2\\\"><hr \\/><\\/td>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.joomlacorner.com\\\" target=\\\"_blank\\\">http:\\/\\/www.joomlacorner.com<\\/a><\\/td>\\n  \\t\\t <\\/tr>\\n  \\t\\t <tr>\\n   \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.marvelic.co.th\\\" target=\\\"_blank\\\">http:\\/\\/www.marvelic.co.th<\\/a><\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"100%\\\" colspan=\\\"2\\\">\\n  \\t\\t      <strong><font color=\\\"#008000\\\">\\u0e17\\u0e48\\u0e32\\u0e19\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e15\\u0e34\\u0e14\\u0e15\\u0e32\\u0e21\\u0e02\\u0e48\\u0e32\\u0e27\\u0e2a\\u0e32\\u0e23\\u0e41\\u0e25\\u0e30\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19\\u0e25\\u0e48\\u0e32\\u0e2a\\u0e38\\u0e14 \\u0e08\\u0e32\\u0e01\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32\\u0e02\\u0e49\\u0e32\\u0e07\\u0e15\\u0e49\\u0e19<\\/font><\\/strong>\\n          <\\/td>\\n  \\t    <\\/tr>\\n      <\\/table>\\n    <\\/div>\\n    \",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 10002, 'Thaith-TH', 'language', 'th-TH', '', 1, 1, 0, 0, '{\"name\":\"Thai (th-TH)\",\"type\":\"language\",\"creationDate\":\"November 2017\",\"author\":\"Thai Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters & JoomlaCorner.com. All rights reserved.\",\"authorEmail\":\"tt@joomlacorner.com\",\"authorUrl\":\"www.joomlacorner.com\",\"version\":\"3.8.2.1\",\"description\":\"\\n    <div align=\\\"center\\\">\\n      <table border=\\\"0\\\" width=\\\"100%\\\">\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"100%\\\" colspan=\\\"2\\\">\\n  \\t\\t      <div class=\\\"alert alert-success center\\\" align=\\\"center\\\">\\n  \\t\\t        <h3 class=\\\"alert-heading\\\">\\u0e20\\u0e32\\u0e29\\u0e32\\u0e44\\u0e17\\u0e22\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 3.8 \\u0e08\\u0e32\\u0e01\\u0e17\\u0e35\\u0e21\\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c <br \\/>\\u0e20\\u0e32\\u0e22\\u0e43\\u0e15\\u0e49\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e19\\u0e31\\u0e1a\\u0e2a\\u0e19\\u0e38\\u0e19\\u0e42\\u0e14\\u0e22 \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14<\\/h3>\\n  \\t\\t      <\\/div>\\n  \\t\\t      <hr \\/>\\n  \\t\\t    <\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e23\\u0e32\\u0e22\\u0e25\\u0e30\\u0e40\\u0e2d\\u0e35\\u0e22\\u0e14 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">\\u0e20\\u0e32\\u0e29\\u0e32\\u0e44\\u0e17\\u0e22\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25\\u0e23\\u0e30\\u0e1a\\u0e1a (\\u0e14\\u0e49\\u0e32\\u0e19\\u0e2b\\u0e19\\u0e49\\u0e32\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25)<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e44\\u0e1f\\u0e25\\u0e4c\\u0e20\\u0e32\\u0e29\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8.2v1<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <td width=\\\"100%\\\" colspan=\\\"2\\\"><hr \\/><\\/td>\\n   \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.joomlacorner.com\\\" target=\\\"_blank\\\">http:\\/\\/www.joomlacorner.com<\\/a><\\/td>\\n  \\t\\t <\\/tr>\\n  \\t\\t <tr>\\n   \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.marvelic.co.th\\\" target=\\\"_blank\\\">http:\\/\\/www.marvelic.co.th<\\/a><\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"100%\\\" colspan=\\\"2\\\">\\n  \\t\\t      <strong><font color=\\\"#008000\\\">\\u0e17\\u0e48\\u0e32\\u0e19\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e15\\u0e34\\u0e14\\u0e15\\u0e32\\u0e21\\u0e02\\u0e48\\u0e32\\u0e27\\u0e2a\\u0e32\\u0e23\\u0e41\\u0e25\\u0e30\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19\\u0e25\\u0e48\\u0e32\\u0e2a\\u0e38\\u0e14 \\u0e08\\u0e32\\u0e01\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32\\u0e02\\u0e49\\u0e32\\u0e07\\u0e15\\u0e49\\u0e19<\\/font><\\/strong>\\n          <\\/td>\\n  \\t    <\\/tr>\\n      <\\/table>\\n    <\\/div>\\n    \",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 0, 'Thai (th-TH) Language Pack', 'package', 'pkg_th-TH', '', 0, 1, 1, 0, '{\"name\":\"Thai (th-TH) Language Pack\",\"type\":\"package\",\"creationDate\":\"November 2017\",\"author\":\"Thai Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc & JoomlaCorner.com. All rights reserved.\",\"authorEmail\":\"tt@joomlacorner.com\",\"authorUrl\":\"www.joomlacorner.com\",\"version\":\"3.8.2.1\",\"description\":\"\\n\\t\\t<div align=\\\"center\\\">\\n\\t\\t<table border=\\\"0\\\" width=\\\"100%\\\">\\n\\t\\t<tr>\\n\\t\\t\\t<td width=\\\"100%\\\" colspan=\\\"2\\\">\\n\\t\\t\\t\\t<div class=\\\"alert alert-success center\\\" align=\\\"center\\\">\\n\\t\\t\\t\\t\\t<h3 class=\\\"alert-heading\\\">Joomla 3.8 Thai Language Pack by JoomlaCorner Team <br \\/>under Marvelic Engine Co.,Ltd.<br \\/><br \\/>\\u0e20\\u0e32\\u0e29\\u0e32\\u0e44\\u0e17\\u0e22\\u0e2a\\u0e33\\u0e2b\\u0e23\\u0e31\\u0e1a\\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 3.8 \\u0e08\\u0e32\\u0e01\\u0e17\\u0e35\\u0e21\\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c <br \\/>\\u0e20\\u0e32\\u0e22\\u0e43\\u0e15\\u0e49\\u0e01\\u0e32\\u0e23\\u0e2a\\u0e19\\u0e31\\u0e1a\\u0e2a\\u0e19\\u0e38\\u0e19\\u0e42\\u0e14\\u0e22 \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14<\\/h3>\\n  \\t\\t      <\\/div>\\n  \\t\\t      <hr \\/>\\n  \\t\\t    <\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e1b\\u0e23\\u0e30\\u0e01\\u0e2d\\u0e1a\\u0e14\\u0e49\\u0e27\\u0e22 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">\\u0e2a\\u0e48\\u0e27\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25 (\\u0e14\\u0e49\\u0e32\\u0e19\\u0e2b\\u0e19\\u0e49\\u0e32\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25) <br \\/>\\u0e41\\u0e25\\u0e30\\u0e2a\\u0e48\\u0e27\\u0e19\\u0e02\\u0e2d\\u0e07\\u0e44\\u0e0b\\u0e15\\u0e4c (\\u0e14\\u0e49\\u0e32\\u0e19\\u0e2b\\u0e19\\u0e49\\u0e32\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c)<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19 \\u0e02\\u0e2d\\u0e07\\u0e44\\u0e1f\\u0e25\\u0e4c\\u0e20\\u0e32\\u0e29\\u0e32 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\">3.8.2v1<\\/td>\\n  \\t    <\\/tr>\\n  \\t    <td width=\\\"100%\\\" colspan=\\\"2\\\"><hr \\/><\\/td>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e08\\u0e39\\u0e21\\u0e25\\u0e48\\u0e32\\u0e04\\u0e2d\\u0e23\\u0e4c\\u0e40\\u0e19\\u0e2d\\u0e23\\u0e4c :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.joomlacorner.com\\\" target=\\\"_blank\\\">http:\\/\\/www.joomlacorner.com<\\/a><\\/td>\\n  \\t\\t <\\/tr>\\n  \\t\\t <tr>\\n   \\t\\t    <td width=\\\"40%\\\"><u><strong>\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c \\u0e1a\\u0e23\\u0e34\\u0e29\\u0e31\\u0e17 \\u0e21\\u0e32\\u0e23\\u0e4c\\u0e40\\u0e27\\u0e25\\u0e34\\u0e04 \\u0e40\\u0e2d\\u0e47\\u0e19\\u0e08\\u0e34\\u0e49\\u0e19 \\u0e08\\u0e33\\u0e01\\u0e31\\u0e14 :<\\/strong><\\/u><\\/td>\\n  \\t\\t    <td width=\\\"60%\\\"><a href=\\\"http:\\/\\/www.marvelic.co.th\\\" target=\\\"_blank\\\">http:\\/\\/www.marvelic.co.th<\\/a><\\/td>\\n  \\t    <\\/tr>\\n  \\t    <tr>\\n  \\t\\t    <td width=\\\"100%\\\" colspan=\\\"2\\\">\\n  \\t\\t      <strong><font color=\\\"#008000\\\">\\u0e17\\u0e48\\u0e32\\u0e19\\u0e2a\\u0e32\\u0e21\\u0e32\\u0e23\\u0e16\\u0e15\\u0e34\\u0e14\\u0e15\\u0e32\\u0e21\\u0e02\\u0e48\\u0e32\\u0e27\\u0e2a\\u0e32\\u0e23\\u0e41\\u0e25\\u0e30\\u0e40\\u0e27\\u0e2d\\u0e23\\u0e4c\\u0e0a\\u0e31\\u0e19\\u0e25\\u0e48\\u0e32\\u0e2a\\u0e38\\u0e14 \\u0e08\\u0e32\\u0e01\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e23\\u0e32\\u0e02\\u0e49\\u0e32\\u0e07\\u0e15\\u0e49\\u0e19<\\/font><\\/strong>\\n          <\\/td>\\n  \\t    <\\/tr>\\n      <\\/table>\\n    <\\/div>\\n\\t\",\"group\":\"\",\"filename\":\"pkg_th-TH\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 10010, 'plg_system_gantry5', 'plugin', 'gantry5', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_gantry5\",\"type\":\"plugin\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"PLG_SYSTEM_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{\"production\":\"1\",\"use_media_folder\":\"0\",\"asset_timestamps\":\"1\",\"asset_timestamps_period\":\"7\",\"compile_yaml\":\"1\",\"compile_twig\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 10010, 'plg_quickicon_gantry5', 'plugin', 'gantry5', 'quickicon', 0, 1, 1, 0, '{\"name\":\"plg_quickicon_gantry5\",\"type\":\"plugin\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"PLG_QUICKICON_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{\"context\":\"mod_quickicon\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 10010, 'plg_gantry5_preset', 'plugin', 'preset', 'gantry5', 0, 0, 1, 0, '{\"name\":\"plg_gantry5_preset\",\"type\":\"plugin\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"PLG_GANTRY5_PRESET_DESCRIPTION\",\"group\":\"\",\"filename\":\"preset\"}', '{\"preset\":\"presets\",\"reset\":\"reset-settings\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 10010, 'mod_gantry5_particle', 'module', 'mod_gantry5_particle', '', 0, 1, 0, 0, '{\"name\":\"mod_gantry5_particle\",\"type\":\"module\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"MOD_GANTRY5_PARTICLE_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_gantry5_particle\"}', '{\"owncache\":\"0\",\"cache_time\":\"900\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 10010, 'Gantry 5 Framework', 'library', 'gantry5', '', 0, 1, 1, 0, '{\"name\":\"Gantry 5 Framework\",\"type\":\"library\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"LIB_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 10010, 'gantry5_nucleus', 'file', 'gantry5_nucleus', '', 0, 1, 0, 0, '{\"name\":\"gantry5_nucleus\",\"type\":\"file\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"GANTRY5_NUCLEUS_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 10010, 'com_gantry5', 'component', 'com_gantry5', '', 1, 1, 0, 0, '{\"name\":\"com_gantry5\",\"type\":\"component\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"COM_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 0, 'pkg_gantry5', 'package', 'pkg_gantry5', '', 0, 1, 1, 0, '{\"name\":\"pkg_gantry5\",\"type\":\"package\",\"creationDate\":\"October 18, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.19\",\"description\":\"PKG_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"pkg_gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10011, 0, 'jl_dream_free', 'template', 'jl_dream_free', '', 0, 1, 1, 0, '{\"name\":\"jl_dream_free\",\"type\":\"template\",\"creationDate\":\"July 07, 2017\",\"author\":\"JoomLead\",\"copyright\":\"(C) 2015 - 2017 JoomLead. All rights reserved.\",\"authorEmail\":\"support@joomlead.com\",\"authorUrl\":\"http:\\/\\/www.joomlead.com\",\"version\":\"1.0.1\",\"description\":\"TPL_JL_DREAM_FREE_DESC\",\"group\":\"\",\"filename\":\"templateDetails\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 0, 'Helix3 - Ajax', 'plugin', 'helix3', 'ajax', 0, 1, 1, 0, '{\"name\":\"Helix3 - Ajax\",\"type\":\"plugin\",\"creationDate\":\"Jan 2015\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2017 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"2.3\",\"description\":\"Helix3 Framework - Joomla Template Framework by JoomShaper\",\"group\":\"\",\"filename\":\"helix3\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 0, 'System - Helix3 Framework', 'plugin', 'helix3', 'system', 0, 1, 1, 0, '{\"name\":\"System - Helix3 Framework\",\"type\":\"plugin\",\"creationDate\":\"Jan 2015\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2017 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"2.3\",\"description\":\"Helix3 Framework - Joomla Template Framework by JoomShaper\",\"group\":\"\",\"filename\":\"helix3\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 0, 'jl_homifi_free', 'template', 'jl_homifi_free', '', 0, 1, 1, 0, '{\"name\":\"jl_homifi_free\",\"type\":\"template\",\"creationDate\":\"August 2016\",\"author\":\"JoomLead.com\",\"copyright\":\"Copyright (C) 2015 - 2017 JoomLead.com. All rights reserved.\",\"authorEmail\":\"support@joomlead.com\",\"authorUrl\":\"https:\\/\\/joomlead.com\",\"version\":\"1.0.4\",\"description\":\"\\n          \\n          \\n          \\n    \",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"preloader\":\"0\",\"preloader_animation\":\"circle\",\"preloader_bg\":\"#f5f5f5\",\"preloader_tx\":\"#333333\",\"goto_top\":\"0\",\"sticky_header\":\"1\",\"boxed_layout\":\"0\",\"logo_type\":\"image\",\"logo_position\":\"logo\",\"logo_load_pos\":\"default\",\"body_bg_repeat\":\"inherit\",\"body_bg_size\":\"inherit\",\"body_bg_attachment\":\"inherit\",\"body_bg_position\":\"0 0\",\"enabled_copyright\":\"1\",\"copyright_position\":\"footer1\",\"copyright_load_pos\":\"default\",\"copyright\":\"\\u00a9 2017 Your Company. All Rights Reserved. Designed By JoomLead\",\"show_social_icons\":\"1\",\"social_position\":\"top2\",\"social_load_pos\":\"default\",\"enable_contactinfo\":\"1\",\"contact_position\":\"top1\",\"contact_phone\":\"+660-984-893\",\"contact_email\":\"booking@homify.com\",\"comingsoon_mode\":\"0\",\"comingsoon_title\":\"Coming Soon Title\",\"comingsoon_date\":\"5-10-2018\",\"comingsoon_content\":\"Coming soon content\",\"preset\":\"preset1\",\"preset1_bg\":\"#ffffff\",\"preset1_text\":\"#3a4b51\",\"preset1_major\":\"#96d036\",\"preset1_megabg\":\"#ffffff\",\"preset1_megatx\":\"#3a4b51\",\"preset2_bg\":\"#ffffff\",\"preset2_text\":\"#3a4b51\",\"preset2_major\":\"#1bb5ec\",\"preset2_megabg\":\"#ffffff\",\"preset2_megatx\":\"#3a4b51\",\"preset3_bg\":\"#ffffff\",\"preset3_text\":\"#3a4b51\",\"preset3_major\":\"#ac7c7c\",\"preset3_megabg\":\"#ffffff\",\"preset3_megatx\":\"#3a4b51\",\"preset4_bg\":\"#ffffff\",\"preset4_text\":\"#3a4b51\",\"preset4_major\":\"#f7c873\",\"preset4_megabg\":\"#ffffff\",\"preset4_megatx\":\"#3a4b51\",\"menu\":\"mainmenu\",\"menu_type\":\"mega_offcanvas\",\"menu_animation\":\"menu-fade\",\"offcanvas_animation\":\"default\",\"enable_body_font\":\"1\",\"body_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"300\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h1_font\":\"1\",\"h1_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"800\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h2_font\":\"1\",\"h2_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h3_font\":\"1\",\"h3_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h4_font\":\"1\",\"h4_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h5_font\":\"1\",\"h5_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h6_font\":\"1\",\"h6_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_navigation_font\":\"0\",\"enable_custom_font\":\"0\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"lessoption\":\"0\",\"show_post_format\":\"1\",\"commenting_engine\":\"disabled\",\"disqus_devmode\":\"0\",\"intensedebate_acc\":\"\",\"fb_width\":\"500\",\"fb_cpp\":\"10\",\"comments_count\":\"0\",\"social_share\":\"1\",\"image_small\":\"0\",\"image_small_size\":\"100X100\",\"image_thumbnail\":\"1\",\"image_thumbnail_size\":\"200X200\",\"image_medium\":\"0\",\"image_medium_size\":\"300X300\",\"image_large\":\"0\",\"image_large_size\":\"600X600\",\"blog_list_image\":\"default\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 0, 'Slideshow CK', 'module', 'mod_slideshowck', '', 0, 1, 0, 0, '{\"name\":\"Slideshow CK\",\"type\":\"module\",\"creationDate\":\"Avril 2012\",\"author\":\"C\\u00e9dric KEIFLIN\",\"copyright\":\"C\\u00e9dric KEIFLIN\",\"authorEmail\":\"ced1870@gmail.com\",\"authorUrl\":\"http:\\/\\/www.joomlack.fr\",\"version\":\"1.4.46\",\"description\":\"MOD_SLIDESHOWCK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_slideshowck\"}', '{\"slidesssource\":\"slidesmanager\",\"slides\":\"[{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imgcaption|qq|:|qq|This bridge is very long|qq|,|qq|imgtitle|qq|:|qq|This is a bridge|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imgcaption|qq|:|qq|This slideshow uses a JQuery script adapted from <a href=|dq|http:\\/\\/www.pixedelic.com\\/plugins\\/camera\\/|dq|>Pixedelic<\\/a>|qq|,|qq|imgtitle|qq|:|qq|On the road again|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq|http:\\/\\/player.vimeo.com\\/video\\/2203727|qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|}]\",\"skin\":\"camera_amber_skin\",\"alignment\":\"center\",\"loader\":\"pie\",\"width\":\"auto\",\"height\":\"62%\",\"minheight\":\"150\",\"navigation\":\"2\",\"thumbnails\":\"1\",\"thumbnailwidth\":\"100\",\"thumbnailheight\":\"75\",\"pagination\":\"1\",\"effect\":\"random\",\"time\":\"7000\",\"transperiod\":\"1500\",\"captioneffect\":\"random\",\"portrait\":\"0\",\"autoAdvance\":\"1\",\"hover\":\"1\",\"displayorder\":\"normal\",\"limitslides\":\"\",\"fullpage\":\"0\",\"imagetarget\":\"_parent\",\"linkposition\":\"fullslide\",\"container\":\"\",\"usemobileimage\":\"0\",\"mobileimageresolution\":\"640\",\"loadjquery\":\"1\",\"loadjqueryeasing\":\"1\",\"autocreatethumbs\":\"1\",\"fixhtml\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"articlelength\":\"150\",\"articlelink\":\"readmore\",\"articletitle\":\"h3\",\"showarticletitle\":\"1\",\"usecaption\":\"1\",\"usecaptiondesc\":\"1\",\"usecaptionresponsive\":\"1\",\"captionresponsiveresolution\":\"480\",\"captionresponsivefontsize\":\"0.6em\",\"captionresponsivehidecaption\":\"0\",\"captionstylesusefont\":\"1\",\"captionstylestextgfont\":\"Droid Sans\",\"captionstylesfontsize\":\"1.1em\",\"captionstylesfontcolor\":\"\",\"captionstylesfontweight\":\"normal\",\"captionstylesdescfontsize\":\"0.8em\",\"captionstylesdescfontcolor\":\"\",\"captionstylesusemargin\":\"1\",\"captionstylesmargintop\":\"0\",\"captionstylesmarginright\":\"0\",\"captionstylesmarginbottom\":\"0\",\"captionstylesmarginleft\":\"0\",\"captionstylespaddingtop\":\"0\",\"captionstylespaddingright\":\"0\",\"captionstylespaddingbottom\":\"0\",\"captionstylespaddingleft\":\"0\",\"captionstylesusebackground\":\"1\",\"captionstylesbgcolor1\":\"\",\"captionstylesbgopacity\":\"0.6\",\"captionstylesbgpositionx\":\"left\",\"captionstylesbgpositiony\":\"top\",\"captionstylesbgimagerepeat\":\"repeat\",\"captionstylesusegradient\":\"1\",\"captionstylesbgcolor2\":\"\",\"captionstylesuseroundedcorners\":\"1\",\"captionstylesroundedcornerstl\":\"5\",\"captionstylesroundedcornerstr\":\"5\",\"captionstylesroundedcornersbr\":\"5\",\"captionstylesroundedcornersbl\":\"5\",\"captionstylesuseshadow\":\"1\",\"captionstylesshadowcolor\":\"\",\"captionstylesshadowblur\":\"3\",\"captionstylesshadowspread\":\"0\",\"captionstylesshadowoffsetx\":\"0\",\"captionstylesshadowoffsety\":\"0\",\"captionstylesshadowinset\":\"0\",\"captionstylesuseborders\":\"1\",\"captionstylesbordercolor\":\"\",\"captionstylesborderwidth\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 0, 'COM_CREATIVEIMAGESLIDER', 'component', 'com_creativeimageslider', '', 1, 1, 0, 0, '{\"name\":\"COM_CREATIVEIMAGESLIDER\",\"type\":\"component\",\"creationDate\":\"Februrary 2014\",\"author\":\"Creative Solutions Company\",\"copyright\":\"Copyright (\\u00a9) 2008-2017 Creative Solutions company. All rights reserved.\",\"authorEmail\":\"info@creative-solutions.net\",\"authorUrl\":\"http:\\/\\/creative-solutions.net\",\"version\":\"3.1.0\",\"description\":\"COM_CREATIVEIMAGESLIDER_DESCRIPTION\",\"group\":\"\",\"filename\":\"creativeimageslider\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 0, 'Creative Image Slider', 'module', 'mod_creativeimageslider', '', 0, 1, 0, 0, '{\"name\":\"Creative Image Slider\",\"type\":\"module\",\"creationDate\":\"Februrary 2014\",\"author\":\"Creative Solutions Company\",\"copyright\":\"Copyright (\\u00a9) 2008-2017 Creative Solutions company. All rights reserved.\",\"authorEmail\":\"info@creative-solutions.net\",\"authorUrl\":\"http:\\/\\/creative-solutions.net\",\"version\":\"3.1.0\",\"description\":\"MOD_CREATIVEIMAGESLIDER_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_creativeimageslider\"}', '{\"slider_id\":\"\",\"class_suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 0, 'Creative Image Slider', 'plugin', 'creativeimageslider', 'system', 0, 1, 1, 0, '{\"name\":\"Creative Image Slider\",\"type\":\"plugin\",\"creationDate\":\"Februrary 2014\",\"author\":\"Creative Solutions Company\",\"copyright\":\"Copyright (\\u00a9) 2008-2017 Creative Solutions company. All rights reserved.\",\"authorEmail\":\"info@creative-solutions.net\",\"authorUrl\":\"http:\\/\\/creative-solutions.net\",\"version\":\"3.1.0\",\"description\":\"PLG_CREATIVEIMAGESLIDER_DESCRIPTION\",\"group\":\"\",\"filename\":\"creativeimageslider\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_fields`
--

CREATE TABLE `holp3_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_fields_categories`
--

CREATE TABLE `holp3_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_fields_groups`
--

CREATE TABLE `holp3_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_fields_values`
--

CREATE TABLE `holp3_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_filters`
--

CREATE TABLE `holp3_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links`
--

CREATE TABLE `holp3_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms0`
--

CREATE TABLE `holp3_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms1`
--

CREATE TABLE `holp3_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms2`
--

CREATE TABLE `holp3_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms3`
--

CREATE TABLE `holp3_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms4`
--

CREATE TABLE `holp3_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms5`
--

CREATE TABLE `holp3_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms6`
--

CREATE TABLE `holp3_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms7`
--

CREATE TABLE `holp3_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms8`
--

CREATE TABLE `holp3_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_terms9`
--

CREATE TABLE `holp3_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termsa`
--

CREATE TABLE `holp3_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termsb`
--

CREATE TABLE `holp3_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termsc`
--

CREATE TABLE `holp3_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termsd`
--

CREATE TABLE `holp3_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termse`
--

CREATE TABLE `holp3_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_links_termsf`
--

CREATE TABLE `holp3_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_taxonomy`
--

CREATE TABLE `holp3_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holp3_finder_taxonomy`
--

INSERT INTO `holp3_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_taxonomy_map`
--

CREATE TABLE `holp3_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_terms`
--

CREATE TABLE `holp3_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_terms_common`
--

CREATE TABLE `holp3_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holp3_finder_terms_common`
--

INSERT INTO `holp3_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_tokens`
--

CREATE TABLE `holp3_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_tokens_aggregate`
--

CREATE TABLE `holp3_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_finder_types`
--

CREATE TABLE `holp3_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_languages`
--

CREATE TABLE `holp3_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_languages`
--

INSERT INTO `holp3_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (en-GB)', 'English (United Kingdom)', 'en', 'en_gb', '', '', '', '', 1, 1, 2),
(2, 56, 'th-TH', 'Thai (th-TH)', 'Thai ไทย (ภาษาไทย)', 'th', 'th_th', '', '', '', '', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_menu`
--

CREATE TABLE `holp3_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_menu`
--

INSERT INTO `holp3_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 67, 0, '*', 0),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 20, 0, '*', 1),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 21, 26, 0, '*', 1),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 22, 23, 0, '*', 1),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 24, 25, 0, '*', 1),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 27, 28, 0, '*', 1),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 29, 30, 0, '*', 1),
(18, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 31, 32, 0, '*', 1),
(19, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 33, 34, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 35, 36, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 37, 38, 0, '*', 1),
(22, 'main', 'com_associations', 'Multilingual Associations', '', 'Multilingual Associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 0, 'class:associations', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"featured_categories\":[\"\"],\"layout_type\":\"blog\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"3\",\"num_columns\":\"3\",\"num_links\":\"0\",\"multi_column_order\":\"1\",\"orderby_pri\":\"\",\"orderby_sec\":\"front\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 41, 42, 1, '*', 0),
(102, 'main', 'COM_GANTRY5', 'com-gantry5', '', 'com-gantry5', 'index.php?option=com_gantry5', 'component', 1, 1, 1, 10009, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 43, 48, 0, '', 1),
(103, 'main', 'COM_GANTRY5_ADMIN_MENU_THEMES', 'com-gantry5-admin-menu-themes', '', 'com-gantry5/com-gantry5-admin-menu-themes', 'index.php?option=com_gantry5&view=themes', 'component', 1, 102, 2, 10009, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 44, 45, 0, '', 1),
(104, 'main', 'COM_GANTRY5_ADMIN_MENU_THEME', 'com-gantry5-admin-menu-theme', '', 'com-gantry5/com-gantry5-admin-menu-theme', 'index.php?option=com_gantry5', 'component', 1, 102, 2, 10009, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 46, 47, 0, '', 1),
(105, 'mainmenu', 'ท่องเที่ยว', 'test-link', '', 'test-link', 'index.php?option=com_content&view=categories&id=10', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_base_description\":\"\",\"categories_description\":\"\",\"maxLevelcat\":\"\",\"show_empty_categories_cat\":\"\",\"show_subcat_desc_cat\":\"\",\"show_cat_num_articles_cat\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"alpha\",\"order_date\":\"created\",\"show_pagination_limit\":\"\",\"filter_field\":\"\",\"show_headings\":\"\",\"list_show_date\":\"\",\"date_format\":\"\",\"list_show_hits\":\"\",\"list_show_author\":\"\",\"display_num\":\"10\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"article_layout\":\"_:default\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 49, 52, 0, '*', 0),
(106, 'mainmenu', 'ຜາຫຼວງ', '2017-11-22-08-57-46', '', 'test-link/2017-11-22-08-57-46', 'index.php?option=com_content&view=categories&id=9', 'component', 1, 105, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_base_description\":\"\",\"categories_description\":\"\",\"maxLevelcat\":\"\",\"show_empty_categories_cat\":\"\",\"show_subcat_desc_cat\":\"\",\"show_cat_num_articles_cat\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination_limit\":\"\",\"filter_field\":\"\",\"show_headings\":\"\",\"list_show_date\":\"\",\"date_format\":\"\",\"list_show_hits\":\"\",\"list_show_author\":\"\",\"display_num\":\"10\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"article_layout\":\"_:default\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 50, 51, 0, '*', 0),
(107, 'mainmenu', 'Login', 'profile-profile', '', 'profile-profile', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"loginredirectchoice\":\"0\",\"login_redirect_url\":\"index.php\\/profile-profile\\/profile\",\"login_redirect_menuitem\":\"\",\"logindescription_show\":\"1\",\"login_description\":\"dddd\",\"login_image\":\"\",\"logoutredirectchoice\":\"1\",\"logout_redirect_url\":\"\",\"logout_redirect_menuitem\":\"\",\"logoutdescription_show\":\"1\",\"logout_description\":\"\",\"logout_image\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 59, 60, 0, '*', 0),
(108, 'main', 'COM_CREATIVEIMAGESLIDER_MENU', 'com-creativeimageslider-menu', '', 'com-creativeimageslider-menu', 'index.php?option=com_creativeimageslider', 'component', 1, 1, 1, 10017, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_creativeimageslider/assets/images/project_16.png', 0, '{}', 61, 66, 0, '', 1),
(109, 'main', 'COM_CREATIVEIMAGESLIDER_SUBMENU_SLIDERS', 'com-creativeimageslider-submenu-sliders', '', 'com-creativeimageslider-menu/com-creativeimageslider-submenu-sliders', 'index.php?option=com_creativeimageslider&view=creativesliders', 'component', 1, 108, 2, 10017, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_creativeimageslider/assets/images/poll_16.png', 0, '{}', 62, 63, 0, '', 1),
(110, 'main', 'COM_CREATIVEIMAGESLIDER_SUBMENU_IMAGES', 'com-creativeimageslider-submenu-images', '', 'com-creativeimageslider-menu/com-creativeimageslider-submenu-images', 'index.php?option=com_creativeimageslider&view=creativeimages', 'component', 1, 108, 2, 10017, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_creativeimageslider/assets/images/answer_16.png', 0, '{}', 64, 65, 0, '', 1),
(111, 'mainmenu', 'ติดต่อเรา', '2017-11-23-04-35-50', '', '2017-11-23-04-35-50', 'index.php?option=com_content&view=article&id=3', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"0\",\"link_titles\":\"0\",\"show_intro\":\"0\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_associations\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"111\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 57, 58, 0, '*', 0),
(112, 'mainmenu', 'โรงแรม ที่พัก', '2017-11-23-04-45-08', '', '2017-11-23-04-45-08', 'index.php?option=com_content&view=article&id=4', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_associations\":\"0\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"\",\"show_hits\":\"0\",\"show_tags\":\"0\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"112\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 53, 54, 0, '*', 0),
(113, 'mainmenu', 'รูปภาพ', '2017-11-23-06-27-16', '', '2017-11-23-06-27-16', 'index.php?option=com_content&view=article&id=5', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"0\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_associations\":\"0\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"0\",\"show_noauth\":\"0\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"113\\\",\\\"moduleId\\\":\\\"16,1\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 55, 56, 0, '*', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_menu_types`
--

CREATE TABLE `holp3_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_menu_types`
--

INSERT INTO `holp3_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_messages`
--

CREATE TABLE `holp3_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_messages_cfg`
--

CREATE TABLE `holp3_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_modules`
--

CREATE TABLE `holp3_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_modules`
--

INSERT INTO `holp3_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"mainmenu\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"_menu\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{\"greeting\":\"1\",\"name\":\"0\"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{\"moduleclass_sfx\":\"\",\"showHome\":\"1\",\"homeText\":\"\",\"showComponent\":\"1\",\"separator\":\"\",\"cache\":\"0\",\"cache_time\":\"0\",\"cachemode\":\"itemid\"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(87, 55, 'Sample Data', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sampledata', 6, 1, '{}', 1, '*'),
(88, 57, 'Gantry 5 Particle', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_gantry5_particle', 1, 0, '{\"particle\":\"{\\\"type\\\":\\\"particle\\\",\\\"particle\\\":\\\"menu\\\",\\\"title\\\":\\\"Menu\\\",\\\"options\\\":{\\\"particle\\\":{\\\"enabled\\\":\\\"0\\\",\\\"menu\\\":\\\"\\\",\\\"base\\\":\\\"\\/\\\",\\\"startLevel\\\":\\\"1\\\",\\\"maxLevels\\\":\\\"0\\\",\\\"renderTitles\\\":\\\"0\\\",\\\"hoverExpand\\\":\\\"1\\\",\\\"mobileTarget\\\":\\\"1\\\"}}}\",\"moduleclass_sfx\":\"\",\"owncache\":\"0\",\"cache_time\":\"900\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(89, 65, 'Offcavas', '', '', 1, 'offcanvas', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"mainmenu\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(90, 66, 'images', '', '', 1, 'position7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_random_image', 1, 1, '{\"type\":\"jpg\",\"folder\":\"\\/images\\/headers\",\"link\":\"\",\"width\":\"\",\"height\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(91, 67, 'Slideshow CK', '', '', 1, 'module-position1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slideshowck', 1, 0, '{\"slidesssource\":\"slidesmanager\",\"slides\":\"[{|qq|imgname|qq|:|qq|images\\/17192460_1299233076830648_2881960565113594214_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/17192460_1299233076830648_2881960565113594214_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/sampledata\\/17855493_1336298459790776_1328872890552281111_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/sampledata\\/17855493_1336298459790776_1328872890552281111_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/15873457_1231302826957007_1166167835903597114_n.jpg|qq|,|qq|imgcaption|qq|:|qq|Ziplining from the tree house to Huay Soum Waterfall|qq|,|qq|imgtitle|qq|:|qq|\\u0e42\\u0e2b\\u0e19\\u0e0b\\u0e34\\u0e1b\\u0e44\\u0e25\\u0e19\\u0e4c\\u0e08\\u0e32\\u0e01\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e15\\u0e49\\u0e19\\u0e44\\u0e21\\u0e49 \\u0e21\\u0e32\\u0e22\\u0e31\\u0e07\\u0e19\\u0e49\\u0e33\\u0e15\\u0e01\\u0e15\\u0e32\\u0e14\\u0e2b\\u0e49\\u0e27\\u0e22\\u0e2a\\u0e38\\u0e21|qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/15873457_1231302826957007_1166167835903597114_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/sampledata\\/20645243_1497267137027240_7655830566598971746_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/sampledata\\/20645243_1497267137027240_7655830566598971746_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/sampledata\\/17855550_1336301893123766_5333978329282235297_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/sampledata\\/17855550_1336301893123766_5333978329282235297_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/sampledata\\/20767997_1497340843686536_2469439796014243030_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/sampledata\\/20767997_1497340843686536_2469439796014243030_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/sampledata\\/22449688_1568560763231210_4927787241169431354_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/sampledata\\/22449688_1568560763231210_4927787241169431354_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]\",\"theme\":\"default\",\"skin\":\"camera_amber_skin\",\"alignment\":\"center\",\"loader\":\"pie\",\"width\":\"auto\",\"height\":\"62%\",\"minheight\":\"150\",\"navigation\":\"2\",\"thumbnails\":\"1\",\"thumbnailwidth\":\"100\",\"thumbnailheight\":\"75\",\"pagination\":\"1\",\"effect\":[\"random\"],\"time\":\"7000\",\"transperiod\":\"1500\",\"captioneffect\":\"moveFromLeft\",\"portrait\":\"0\",\"autoAdvance\":\"1\",\"hover\":\"1\",\"displayorder\":\"normal\",\"limitslides\":\"\",\"fullpage\":\"0\",\"imagetarget\":\"_parent\",\"linkposition\":\"fullslide\",\"container\":\"\",\"usemobileimage\":\"0\",\"mobileimageresolution\":\"640\",\"loadjquery\":\"1\",\"loadjqueryeasing\":\"1\",\"autocreatethumbs\":\"1\",\"fixhtml\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"articlelength\":\"150\",\"articlelink\":\"readmore\",\"articletitle\":\"h3\",\"showarticletitle\":\"1\",\"usecaption\":\"1\",\"usecaptiondesc\":\"1\",\"usecaptionresponsive\":\"1\",\"captionresponsiveresolution\":\"480\",\"captionresponsivefontsize\":\"0.6em\",\"captionresponsivehidecaption\":\"0\",\"captionstylesusefont\":\"1\",\"captionstylestextgfont\":\"Droid Sans\",\"captionstylesfontsize\":\"1.1em\",\"captionstylesfontcolor\":\"\",\"captionstylesfontweight\":\"normal\",\"captionstylesdescfontsize\":\"0.8em\",\"captionstylesdescfontcolor\":\"\",\"captionstylesusemargin\":\"1\",\"captionstylesmargintop\":\"0\",\"captionstylesmarginright\":\"0\",\"captionstylesmarginbottom\":\"0\",\"captionstylesmarginleft\":\"0\",\"captionstylespaddingtop\":\"0\",\"captionstylespaddingright\":\"0\",\"captionstylespaddingbottom\":\"0\",\"captionstylespaddingleft\":\"0\",\"captionstylesusebackground\":\"1\",\"captionstylesbgcolor1\":\"\",\"captionstylesbgopacity\":\"0.6\",\"captionstylesbgimage\":\"\",\"captionstylesbgpositionx\":\"left\",\"captionstylesbgpositiony\":\"top\",\"captionstylesbgimagerepeat\":\"repeat\",\"captionstylesusegradient\":\"1\",\"captionstylesbgcolor2\":\"\",\"captionstylesuseroundedcorners\":\"1\",\"captionstylesroundedcornerstl\":\"5\",\"captionstylesroundedcornerstr\":\"5\",\"captionstylesroundedcornersbr\":\"5\",\"captionstylesroundedcornersbl\":\"5\",\"captionstylesuseshadow\":\"1\",\"captionstylesshadowcolor\":\"\",\"captionstylesshadowblur\":\"3\",\"captionstylesshadowspread\":\"0\",\"captionstylesshadowoffsetx\":\"0\",\"captionstylesshadowoffsety\":\"0\",\"captionstylesshadowinset\":\"0\",\"captionstylesuseborders\":\"1\",\"captionstylesbordercolor\":\"\",\"captionstylesborderwidth\":\"1\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(92, 68, 'ທ່ຽວເມືອງລາວ Laotrips', '', '', 1, 'module-position1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_creativeimageslider', 1, 1, '{\"slider_id\":\"1\",\"class_suffix\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"xxxx\",\"style\":\"System-html5\"}', 0, '*'),
(93, 73, 'ที่พัก', '', '', 1, 'module-position1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slideshowck', 1, 1, '{\"slidesssource\":\"slidesmanager\",\"slides\":\"[{|qq|imgname|qq|:|qq|images\\/15871513_1231330393620917_5296685528854603045_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/15871513_1231330393620917_5296685528854603045_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/15823264_1231269296960360_2700947561929027916_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/15823264_1231269296960360_2700947561929027916_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16300523_1261411857279437_796819189238326673_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16300523_1261411857279437_796819189238326673_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16422234_1261411320612824_7730284719783190573_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16422234_1261411320612824_7730284719783190573_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16422535_1261411807279442_1860677141248570443_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16422535_1261411807279442_1860677141248570443_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16463249_1261411597279463_6543662535329261556_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16463249_1261411597279463_6543662535329261556_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16463448_1261411917279431_5643840573560723002_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16463448_1261411917279431_5643840573560723002_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/16463826_1261411720612784_2605508877466958283_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/16463826_1261411720612784_2605508877466958283_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/15823592_1231271000293523_5911476114789457046_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/15823592_1231271000293523_5911476114789457046_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]\",\"theme\":\"default\",\"skin\":\"camera_amber_skin\",\"alignment\":\"center\",\"loader\":\"pie\",\"width\":\"auto\",\"height\":\"62%\",\"minheight\":\"150\",\"navigation\":\"2\",\"thumbnails\":\"1\",\"thumbnailwidth\":\"100\",\"thumbnailheight\":\"75\",\"pagination\":\"1\",\"effect\":[\"random\"],\"time\":\"7000\",\"transperiod\":\"1500\",\"captioneffect\":\"moveFromLeft\",\"portrait\":\"0\",\"autoAdvance\":\"1\",\"hover\":\"1\",\"displayorder\":\"normal\",\"limitslides\":\"\",\"fullpage\":\"0\",\"imagetarget\":\"_parent\",\"linkposition\":\"fullslide\",\"container\":\"\",\"usemobileimage\":\"0\",\"mobileimageresolution\":\"640\",\"loadjquery\":\"1\",\"loadjqueryeasing\":\"1\",\"autocreatethumbs\":\"1\",\"fixhtml\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"articlelength\":\"150\",\"articlelink\":\"readmore\",\"articletitle\":\"h3\",\"showarticletitle\":\"1\",\"usecaption\":\"1\",\"usecaptiondesc\":\"1\",\"usecaptionresponsive\":\"1\",\"captionresponsiveresolution\":\"480\",\"captionresponsivefontsize\":\"0.6em\",\"captionresponsivehidecaption\":\"0\",\"captionstylesusefont\":\"1\",\"captionstylestextgfont\":\"Droid Sans\",\"captionstylesfontsize\":\"1.1em\",\"captionstylesfontcolor\":\"\",\"captionstylesfontweight\":\"normal\",\"captionstylesdescfontsize\":\"0.8em\",\"captionstylesdescfontcolor\":\"\",\"captionstylesusemargin\":\"1\",\"captionstylesmargintop\":\"0\",\"captionstylesmarginright\":\"0\",\"captionstylesmarginbottom\":\"0\",\"captionstylesmarginleft\":\"0\",\"captionstylespaddingtop\":\"0\",\"captionstylespaddingright\":\"0\",\"captionstylespaddingbottom\":\"0\",\"captionstylespaddingleft\":\"0\",\"captionstylesusebackground\":\"1\",\"captionstylesbgcolor1\":\"\",\"captionstylesbgopacity\":\"0.6\",\"captionstylesbgimage\":\"\",\"captionstylesbgpositionx\":\"left\",\"captionstylesbgpositiony\":\"top\",\"captionstylesbgimagerepeat\":\"repeat\",\"captionstylesusegradient\":\"1\",\"captionstylesbgcolor2\":\"\",\"captionstylesuseroundedcorners\":\"1\",\"captionstylesroundedcornerstl\":\"5\",\"captionstylesroundedcornerstr\":\"5\",\"captionstylesroundedcornersbr\":\"5\",\"captionstylesroundedcornersbl\":\"5\",\"captionstylesuseshadow\":\"1\",\"captionstylesshadowcolor\":\"\",\"captionstylesshadowblur\":\"3\",\"captionstylesshadowspread\":\"0\",\"captionstylesshadowoffsetx\":\"0\",\"captionstylesshadowoffsety\":\"0\",\"captionstylesshadowinset\":\"0\",\"captionstylesuseborders\":\"1\",\"captionstylesbordercolor\":\"\",\"captionstylesborderwidth\":\"1\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(94, 74, 'ธรรมชาติ', '', '', 1, 'module-position1', 108, '2017-11-23 07:03:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slideshowck', 1, 1, '{\"slidesssource\":\"slidesmanager\",\"slides\":\"[{|qq|imgname|qq|:|qq|images\\/2560\\/14707955_1145083568912267_1750739353408766559_o.jpg|qq|,|qq|imgcaption|qq|:|qq|This slideshow uses a JQuery script adapted from <a href=|dq|http:\\/\\/www.pixedelic.com\\/plugins\\/camera\\/|dq|>Pixedelic<\\/a>|qq|,|qq|imgtitle|qq|:|qq|On the road again|qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/14707955_1145083568912267_1750739353408766559_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15219375_1179606525459971_198056112600332316_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15219375_1179606525459971_198056112600332316_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15219600_1193437277410229_6491249203880025946_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15219600_1193437277410229_6491249203880025946_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15391051_1211080215645935_4504571758340336577_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15391051_1211080215645935_4504571758340336577_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15391200_1210588125695144_3026320119250898620_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15391200_1210588125695144_3026320119250898620_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15697363_1220499211370702_8688332968865819589_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15697363_1220499211370702_8688332968865819589_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15740830_1226611777426112_8748521605796016348_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15740830_1226611777426112_8748521605796016348_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/15799826_1228279363926020_5197029603610632459_o.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/15799826_1228279363926020_5197029603610632459_o.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/2560\\/18698023_1405449589542329_5264644177770302387_n.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/localhost\\/tip\\/images\\/2560\\/18698023_1405449589542329_5264644177770302387_n.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]\",\"theme\":\"default\",\"skin\":\"camera_amber_skin\",\"alignment\":\"center\",\"loader\":\"pie\",\"width\":\"auto\",\"height\":\"62%\",\"minheight\":\"150\",\"navigation\":\"2\",\"thumbnails\":\"1\",\"thumbnailwidth\":\"100\",\"thumbnailheight\":\"75\",\"pagination\":\"1\",\"effect\":[\"random\"],\"time\":\"7000\",\"transperiod\":\"1500\",\"captioneffect\":\"moveFromLeft\",\"portrait\":\"0\",\"autoAdvance\":\"1\",\"hover\":\"1\",\"displayorder\":\"normal\",\"limitslides\":\"\",\"fullpage\":\"0\",\"imagetarget\":\"_parent\",\"linkposition\":\"fullslide\",\"container\":\"\",\"usemobileimage\":\"0\",\"mobileimageresolution\":\"640\",\"loadjquery\":\"1\",\"loadjqueryeasing\":\"1\",\"autocreatethumbs\":\"1\",\"fixhtml\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"articlelength\":\"150\",\"articlelink\":\"readmore\",\"articletitle\":\"h3\",\"showarticletitle\":\"1\",\"usecaption\":\"1\",\"usecaptiondesc\":\"1\",\"usecaptionresponsive\":\"1\",\"captionresponsiveresolution\":\"480\",\"captionresponsivefontsize\":\"0.6em\",\"captionresponsivehidecaption\":\"0\",\"captionstylesusefont\":\"1\",\"captionstylestextgfont\":\"Droid Sans\",\"captionstylesfontsize\":\"1.1em\",\"captionstylesfontcolor\":\"\",\"captionstylesfontweight\":\"normal\",\"captionstylesdescfontsize\":\"0.8em\",\"captionstylesdescfontcolor\":\"\",\"captionstylesusemargin\":\"1\",\"captionstylesmargintop\":\"0\",\"captionstylesmarginright\":\"0\",\"captionstylesmarginbottom\":\"0\",\"captionstylesmarginleft\":\"0\",\"captionstylespaddingtop\":\"0\",\"captionstylespaddingright\":\"0\",\"captionstylespaddingbottom\":\"0\",\"captionstylespaddingleft\":\"0\",\"captionstylesusebackground\":\"1\",\"captionstylesbgcolor1\":\"\",\"captionstylesbgopacity\":\"0.6\",\"captionstylesbgimage\":\"\",\"captionstylesbgpositionx\":\"left\",\"captionstylesbgpositiony\":\"top\",\"captionstylesbgimagerepeat\":\"repeat\",\"captionstylesusegradient\":\"1\",\"captionstylesbgcolor2\":\"\",\"captionstylesuseroundedcorners\":\"1\",\"captionstylesroundedcornerstl\":\"5\",\"captionstylesroundedcornerstr\":\"5\",\"captionstylesroundedcornersbr\":\"5\",\"captionstylesroundedcornersbl\":\"5\",\"captionstylesuseshadow\":\"1\",\"captionstylesshadowcolor\":\"\",\"captionstylesshadowblur\":\"3\",\"captionstylesshadowspread\":\"0\",\"captionstylesshadowoffsetx\":\"0\",\"captionstylesshadowoffsety\":\"0\",\"captionstylesshadowinset\":\"0\",\"captionstylesuseborders\":\"1\",\"captionstylesbordercolor\":\"\",\"captionstylesborderwidth\":\"1\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_modules_menu`
--

CREATE TABLE `holp3_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_modules_menu`
--

INSERT INTO `holp3_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 101),
(92, -113),
(93, 113),
(94, 113);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_newsfeeds`
--

CREATE TABLE `holp3_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_overrider`
--

CREATE TABLE `holp3_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_postinstall_messages`
--

CREATE TABLE `holp3_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_postinstall_messages`
--

INSERT INTO `holp3_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(5, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(6, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_redirect_links`
--

CREATE TABLE `holp3_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_schemas`
--

CREATE TABLE `holp3_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_schemas`
--

INSERT INTO `holp3_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.8.2-2017-10-14');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_session`
--

CREATE TABLE `holp3_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_session`
--

INSERT INTO `holp3_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('47026t9sr484ilnivj3gslhpgk', 1, 0, '1511420599', 'joomla|s:2404:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjo1OntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aToxNzk7czo1OiJ0aW1lciI7Tzo4OiJzdGRDbGFzcyI6Mzp7czo1OiJzdGFydCI7aToxNTExNDE4MDg2O3M6NDoibGFzdCI7aToxNTExNDIwNTkzO3M6Mzoibm93IjtpOjE1MTE0MjA1OTk7fXM6NToidG9rZW4iO3M6MzI6InRUTkh5T1JtWDZRaDM3S1JiTVBsdEF3UVVjVW45ZnZ2Ijt9czo4OiJyZWdpc3RyeSI7TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjQ6e3M6MTQ6ImNvbV9jYXRlZ29yaWVzIjtPOjg6InN0ZENsYXNzIjoxOntzOjEwOiJjYXRlZ29yaWVzIjtPOjg6InN0ZENsYXNzIjoxOntzOjc6ImNvbnRlbnQiO086ODoic3RkQ2xhc3MiOjI6e3M6NjoiZmlsdGVyIjtPOjg6InN0ZENsYXNzIjoxOntzOjk6ImV4dGVuc2lvbiI7czoxMToiY29tX2NvbnRlbnQiO31zOjQ6Imxpc3QiO2E6NDp7czo5OiJkaXJlY3Rpb24iO3M6MzoiYXNjIjtzOjU6ImxpbWl0IjtzOjI6IjIwIjtzOjg6Im9yZGVyaW5nIjtzOjU6ImEubGZ0IjtzOjU6InN0YXJ0IjtkOjA7fX19fXM6OToiY29tX21lbnVzIjtPOjg6InN0ZENsYXNzIjoyOntzOjU6Iml0ZW1zIjtPOjg6InN0ZENsYXNzIjo0OntzOjg6Im1lbnV0eXBlIjtzOjg6Im1haW5tZW51IjtzOjk6ImNsaWVudF9pZCI7aTowO3M6MTA6ImxpbWl0c3RhcnQiO2k6MDtzOjQ6Imxpc3QiO2E6NDp7czo5OiJkaXJlY3Rpb24iO3M6MzoiYXNjIjtzOjU6ImxpbWl0IjtzOjI6IjIwIjtzOjg6Im9yZGVyaW5nIjtzOjU6ImEubGZ0IjtzOjU6InN0YXJ0IjtkOjA7fX1zOjQ6ImVkaXQiO086ODoic3RkQ2xhc3MiOjE6e3M6NDoiaXRlbSI7Tzo4OiJzdGRDbGFzcyI6NDp7czo0OiJkYXRhIjtOO3M6NDoidHlwZSI7TjtzOjQ6ImxpbmsiO047czoyOiJpZCI7YTowOnt9fX19czoxMToiY29tX2NvbnRlbnQiO086ODoic3RkQ2xhc3MiOjE6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo3OiJhcnRpY2xlIjtPOjg6InN0ZENsYXNzIjoyOntzOjQ6ImRhdGEiO047czoyOiJpZCI7YTowOnt9fX19czoxMToiY29tX21vZHVsZXMiO086ODoic3RkQ2xhc3MiOjI6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo2OiJtb2R1bGUiO086ODoic3RkQ2xhc3MiOjI6e3M6NDoiZGF0YSI7TjtzOjI6ImlkIjthOjI6e2k6MDtpOjkxO2k6MTtpOjk0O319fXM6MzoiYWRkIjtPOjg6InN0ZENsYXNzIjoxOntzOjY6Im1vZHVsZSI7Tzo4OiJzdGRDbGFzcyI6Mjp7czoxMjoiZXh0ZW5zaW9uX2lkIjtOO3M6NjoicGFyYW1zIjtOO319fX1zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086MjA6Ikpvb21sYVxDTVNcVXNlclxVc2VyIjoxOntzOjI6ImlkIjtzOjM6IjEwOCI7fXM6OToiY29tX21lZGlhIjtPOjg6InN0ZENsYXNzIjoxOntzOjEwOiJyZXR1cm5fdXJsIjtzOjExMDoiaW5kZXgucGhwP29wdGlvbj1jb21fbWVkaWEmdmlldz1pbWFnZXMmdG1wbD1jb21wb25lbnQmZmllbGRpZD0mZV9uYW1lPWNrc2xpZGVpbWduYW1lOSZhc3NldD1jb21fY29uZmlnJmF1dGhvcj0iO31zOjExOiJhcHBsaWNhdGlvbiI7Tzo4OiJzdGRDbGFzcyI6MTp7czo1OiJxdWV1ZSI7YTowOnt9fX19czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9\";', 108, 'admin'),
('emqfsh2rtm5dll143sn8mlupt4', 0, 1, '1511419922', 'joomla|s:1240:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjo0OntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aToyNTtzOjU6InRpbWVyIjtPOjg6InN0ZENsYXNzIjozOntzOjU6InN0YXJ0IjtpOjE1MTE0MTc3Njc7czo0OiJsYXN0IjtpOjE1MTE0MTk4NDI7czozOiJub3ciO2k6MTUxMTQxOTkyMTt9czo1OiJ0b2tlbiI7czozMjoiV2dQWWIzSTZoekc2U0RXNlNZZmdyd2xWa3BvQVd3Z3ciO31zOjg6InJlZ2lzdHJ5IjtPOjI0OiJKb29tbGFcUmVnaXN0cnlcUmVnaXN0cnkiOjM6e3M6NzoiACoAZGF0YSI7Tzo4OiJzdGRDbGFzcyI6MDp7fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fXM6NDoidXNlciI7TzoyMDoiSm9vbWxhXENNU1xVc2VyXFVzZXIiOjE6e3M6MjoiaWQiO2k6MDt9czoxMDoiY29tX21haWx0byI7Tzo4OiJzdGRDbGFzcyI6MTp7czo1OiJsaW5rcyI7YToyOntzOjQwOiIyYzNkYmE1YTcwZmQyMGQ2NmY4M2I1YWFjYzFmMGRhYzUzODAzNTU4IjtPOjg6InN0ZENsYXNzIjoyOntzOjQ6ImxpbmsiO3M6NTA6Imh0dHA6Ly9sb2NhbGhvc3QvdGlwL2luZGV4LnBocC8yMDE3LTExLTIzLTA0LTQ1LTA4IjtzOjY6ImV4cGlyeSI7aToxNTExNDE3Nzg5O31zOjQwOiI2YzEyNjFmZDZhZTEwNWQxZmNmYjRmMzU5MjQ4NDM3NWZiNDRlYWNjIjtPOjg6InN0ZENsYXNzIjoyOntzOjQ6ImxpbmsiO3M6NTA6Imh0dHA6Ly9sb2NhbGhvc3QvdGlwL2luZGV4LnBocC8yMDE3LTExLTIzLTA0LTM1LTUwIjtzOjY6ImV4cGlyeSI7aToxNTExNDE3Nzk5O319fX19czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9\";', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_tags`
--

CREATE TABLE `holp3_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_tags`
--

INSERT INTO `holp3_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 108, '2017-11-22 08:14:53', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_template_styles`
--

CREATE TABLE `holp3_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_template_styles`
--

INSERT INTO `holp3_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.png\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(9, 'jl_dream_free', 0, '1', 'Dream Free - ค่าพื้นฐาน', '{\"preset\":null}'),
(10, 'jl_homifi_free', 0, '0', 'jl_homifi_free - ค่าพื้นฐาน', '{\"preloader\":\"0\",\"preloader_animation\":\"circle\",\"preloader_bg\":\"#f5f5f5\",\"preloader_tx\":\"#333333\",\"goto_top\":\"0\",\"sticky_header\":\"1\",\"boxed_layout\":\"0\",\"logo_type\":\"image\",\"logo_position\":\"logo\",\"logo_load_pos\":\"default\",\"body_bg_repeat\":\"inherit\",\"body_bg_size\":\"inherit\",\"body_bg_attachment\":\"inherit\",\"body_bg_position\":\"0 0\",\"enabled_copyright\":\"1\",\"copyright_position\":\"footer1\",\"copyright_load_pos\":\"default\",\"copyright\":\"\\u00a9 2017 Your Company. All Rights Reserved. Designed By JoomLead\",\"show_social_icons\":\"1\",\"social_position\":\"top2\",\"social_load_pos\":\"default\",\"enable_contactinfo\":\"1\",\"contact_position\":\"top1\",\"contact_phone\":\"+660-984-893\",\"contact_email\":\"booking@homify.com\",\"comingsoon_mode\":\"0\",\"comingsoon_title\":\"Coming Soon Title\",\"comingsoon_date\":\"5-10-2018\",\"comingsoon_content\":\"Coming soon content\",\"preset\":\"preset1\",\"preset1_bg\":\"#ffffff\",\"preset1_text\":\"#3a4b51\",\"preset1_major\":\"#96d036\",\"preset1_megabg\":\"#ffffff\",\"preset1_megatx\":\"#3a4b51\",\"preset2_bg\":\"#ffffff\",\"preset2_text\":\"#3a4b51\",\"preset2_major\":\"#1bb5ec\",\"preset2_megabg\":\"#ffffff\",\"preset2_megatx\":\"#3a4b51\",\"preset3_bg\":\"#ffffff\",\"preset3_text\":\"#3a4b51\",\"preset3_major\":\"#ac7c7c\",\"preset3_megabg\":\"#ffffff\",\"preset3_megatx\":\"#3a4b51\",\"preset4_bg\":\"#ffffff\",\"preset4_text\":\"#3a4b51\",\"preset4_major\":\"#f7c873\",\"preset4_megabg\":\"#ffffff\",\"preset4_megatx\":\"#3a4b51\",\"menu\":\"mainmenu\",\"menu_type\":\"mega_offcanvas\",\"menu_animation\":\"menu-fade\",\"offcanvas_animation\":\"default\",\"enable_body_font\":\"1\",\"body_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"300\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h1_font\":\"1\",\"h1_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"800\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h2_font\":\"1\",\"h2_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h3_font\":\"1\",\"h3_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h4_font\":\"1\",\"h4_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h5_font\":\"1\",\"h5_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h6_font\":\"1\",\"h6_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_navigation_font\":\"0\",\"enable_custom_font\":\"0\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"lessoption\":\"0\",\"show_post_format\":\"1\",\"commenting_engine\":\"disabled\",\"disqus_devmode\":\"0\",\"intensedebate_acc\":\"\",\"fb_width\":\"500\",\"fb_cpp\":\"10\",\"comments_count\":\"0\",\"social_share\":\"1\",\"image_small\":\"0\",\"image_small_size\":\"100X100\",\"image_thumbnail\":\"1\",\"image_thumbnail_size\":\"200X200\",\"image_medium\":\"0\",\"image_medium_size\":\"300X300\",\"image_large\":\"0\",\"image_large_size\":\"600X600\",\"blog_list_image\":\"default\"}');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_ucm_base`
--

CREATE TABLE `holp3_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_ucm_content`
--

CREATE TABLE `holp3_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Table structure for table `holp3_ucm_history`
--

CREATE TABLE `holp3_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_ucm_history`
--

INSERT INTO `holp3_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 1, 1, '', '2017-11-22 08:33:29', 108, 6507, 'c56691ffedd7c612d299cf550088eb631d2c7037', '{\"id\":1,\"asset_id\":60,\"title\":\"\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ea5\\u0eb2\\u0ea7 Laotrips\",\"alias\":\"laotrips\",\"introtext\":\"<h1 class=\\\"entry-title\\\">\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d \\u0ec1\\u0eab\\u0ea5\\u0ec8\\u0e87\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0e84\\u0eb1\\u0ec9\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87\\u0e95\\u0ec9\\u0ead\\u0e87\\u0ec4\\u0e9b\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94<\\/h1>\\r\\n<p><img src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/59667.jpg\\\" \\/><\\/p>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0e8a\\u0ea7\\u0e99\\u0e97\\u0eb8\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0e99\\u0ec9\\u0eb3\\u0ec3\\u0eab\\u0ec9\\u0eab\\u0eb2\\u0e8d\\u0eae\\u0ec9\\u0ead\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87 \\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87 \\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0e9e\\u0eb2\\u0ea1\\u0eb2\\u0e99\\u0eb1\\u0ec9\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e84\\u0eb7\\u00a0<strong>\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d\\u00a0<\\/strong>\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0eab\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0ec3\\u0e99\\u0ec4\\u0ea5\\u0e8d\\u0eb0\\u0e9a\\u0eb8\\u0e99\\u0e9b\\u0eb5\\u0ec3\\u0edd\\u0ec8\\u0ea5\\u0eb2\\u0ea7\\u0e97\\u0eb5\\u0ec8\\u0e9c\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2.<\\/p>\\r\\n<p><img src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1699.jpg\\\" \\/><\\/p>\\r\\n<p><strong>\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d<\\/strong>\\u00a0\\u0e95\\u0eb1\\u0ec9\\u0e87\\u0ea2\\u0eb9\\u0ec8 \\u0e9a\\u0ec9\\u0eb2\\u0e99\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u0ec3\\u0edd\\u0ec8 \\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99. \\u0e97\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0ec0\\u0e84\\u0eb7\\u0ec8\\u0ead\\u0e99\\u0ec4\\u0e9f\\u0e9f\\u0ec9\\u0eb2\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ea1\\u0eb1\\u0e873 \\u0ec0\\u0eaa\\u0eb1\\u0ec9\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ec0\\u0e82\\u0ebb\\u0ec9\\u0eb2\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ec1\\u0ea1\\u0ec8\\u0e99\\u0eaa\\u0eb0\\u0e94\\u0ea7\\u0e81\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e9b\\u0eb9\\u0e8d\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0eae\\u0ead\\u0e94\\u0eaa\\u0ea7\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d \\u0ec1\\u0ea5\\u0eb0 \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ead\\u0eb5\\u0e81\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ead\\u0eb5\\u0e81\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87 \\u0e97\\u0eb5\\u0ec8\\u0ea2\\u0eb9\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0ec4\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec3\\u0e8a\\u0ec9\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0ec3\\u0e99\\u0e81\\u0eb2\\u0e99\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0eb0\\u0ea1\\u0eb2\\u0e99 1 \\u0e8a\\u0ebb\\u0ec8\\u0ea7\\u0ec2\\u0ea1\\u0e87\\u0e81\\u0eb1\\u0e9a\\u0ead\\u0eb5\\u0e81 30 \\u0e99\\u0eb2\\u0e97\\u0eb5 \\u0e81\\u0ecd\\u0ec8\\u0ea1\\u0eb2\\u0eae\\u0ead\\u0e94\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0eaa\\u0eb2\\u0ea1\\u0eb2\\u0e94\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0ea1\\u0ec8\\u0ea7\\u0e99\\u0e8a\\u0eb7\\u0ec8\\u0e99\\u0e81\\u0eb1\\u0e9a\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec4\\u0e94\\u0ec9 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0ec0\\u0e9e\\u0eb4\\u0ec8\\u0e99\\u0e88\\u0eb0\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0e97\\u0eb8\\u0e81\\u0ea7\\u0eb1\\u0e99\\u0ec0\\u0eaa\\u0ebb\\u0eb2-\\u0ead\\u0eb2\\u0e97\\u0eb4\\u0e94 \\u0ec0\\u0ea5\\u0eb5\\u0ec8\\u0ea1\\u0ec0\\u0ea7\\u0ea5\\u0eb2 10:00-18:00 \\u0ec2\\u0ea1\\u0e87 \\u0eab\\u0eb2\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ec3\\u0e94\\u0ea1\\u0eb5\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ea5\\u0ead\\u0e87\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87 \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0e9c\\u0eb4\\u0e94\\u0eab\\u0ea7\\u0eb1\\u0e87.<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-22 08:33:29\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-22 08:33:29\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-11-22 08:33:29\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(2, 2, 5, '', '2017-11-22 08:34:55', 108, 660, '1d0f5b6b5684809f2fa85030b04170d17c0434d2', '{\"id\":2,\"asset_id\":\"27\",\"parent_id\":\"1\",\"lft\":\"1\",\"rgt\":\"2\",\"level\":\"1\",\"path\":\"uncategorised\",\"extension\":\"com_content\",\"title\":\"Uncategorised\",\"alias\":\"uncategorised\",\"note\":\"\",\"description\":\"<p><img src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1699.jpg\\\" \\/><\\/p>\",\"published\":\"1\",\"checked_out\":\"108\",\"checked_out_time\":\"2017-11-22 08:34:44\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:14:53\",\"modified_user_id\":\"108\",\"modified_time\":\"2017-11-22 08:34:55\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(3, 8, 5, '', '2017-11-22 08:40:46', 108, 10062, 'c68a8442af0694936159f0ce7bde5353224427fe', '{\"id\":8,\"asset_id\":61,\"parent_id\":\"2\",\"lft\":\"2\",\"rgt\":3,\"level\":2,\"path\":null,\"extension\":\"com_content\",\"title\":\"\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ea5\\u0eb2\\u0ea7 Laotrips\",\"alias\":\"laotrips\",\"note\":\"\",\"description\":\"<h1 class=\\\"entry-title\\\">\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d \\u0ec1\\u0eab\\u0ea5\\u0ec8\\u0e87\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0e84\\u0eb1\\u0ec9\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87\\u0e95\\u0ec9\\u0ead\\u0e87\\u0ec4\\u0e9b\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94<\\/h1>\\r\\n<p><img style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/59667.jpg\\\" \\/><\\/p>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0e8a\\u0ea7\\u0e99\\u0e97\\u0eb8\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0e99\\u0ec9\\u0eb3\\u0ec3\\u0eab\\u0ec9\\u0eab\\u0eb2\\u0e8d\\u0eae\\u0ec9\\u0ead\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87 \\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87 \\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0e9e\\u0eb2\\u0ea1\\u0eb2\\u0e99\\u0eb1\\u0ec9\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e84\\u0eb7\\u00a0<strong>\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d\\u00a0<\\/strong>\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0eab\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0ec3\\u0e99\\u0ec4\\u0ea5\\u0e8d\\u0eb0\\u0e9a\\u0eb8\\u0e99\\u0e9b\\u0eb5\\u0ec3\\u0edd\\u0ec8\\u0ea5\\u0eb2\\u0ea7\\u0e97\\u0eb5\\u0ec8\\u0e9c\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2.<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1699.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89811 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1699.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1699.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><\\/p>\\r\\n<p><strong>\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d<\\/strong>\\u00a0\\u0e95\\u0eb1\\u0ec9\\u0e87\\u0ea2\\u0eb9\\u0ec8 \\u0e9a\\u0ec9\\u0eb2\\u0e99\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u0ec3\\u0edd\\u0ec8 \\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99. \\u0e97\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0ec0\\u0e84\\u0eb7\\u0ec8\\u0ead\\u0e99\\u0ec4\\u0e9f\\u0e9f\\u0ec9\\u0eb2\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ea1\\u0eb1\\u0e873 \\u0ec0\\u0eaa\\u0eb1\\u0ec9\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ec0\\u0e82\\u0ebb\\u0ec9\\u0eb2\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ec1\\u0ea1\\u0ec8\\u0e99\\u0eaa\\u0eb0\\u0e94\\u0ea7\\u0e81\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e9b\\u0eb9\\u0e8d\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0eae\\u0ead\\u0e94\\u0eaa\\u0ea7\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d \\u0ec1\\u0ea5\\u0eb0 \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ead\\u0eb5\\u0e81\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ead\\u0eb5\\u0e81\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87 \\u0e97\\u0eb5\\u0ec8\\u0ea2\\u0eb9\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0ec4\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec3\\u0e8a\\u0ec9\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0ec3\\u0e99\\u0e81\\u0eb2\\u0e99\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0eb0\\u0ea1\\u0eb2\\u0e99 1 \\u0e8a\\u0ebb\\u0ec8\\u0ea7\\u0ec2\\u0ea1\\u0e87\\u0e81\\u0eb1\\u0e9a\\u0ead\\u0eb5\\u0e81 30 \\u0e99\\u0eb2\\u0e97\\u0eb5 \\u0e81\\u0ecd\\u0ec8\\u0ea1\\u0eb2\\u0eae\\u0ead\\u0e94\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0eaa\\u0eb2\\u0ea1\\u0eb2\\u0e94\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0ea1\\u0ec8\\u0ea7\\u0e99\\u0e8a\\u0eb7\\u0ec8\\u0e99\\u0e81\\u0eb1\\u0e9a\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec4\\u0e94\\u0ec9 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0ec0\\u0e9e\\u0eb4\\u0ec8\\u0e99\\u0e88\\u0eb0\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0e97\\u0eb8\\u0e81\\u0ea7\\u0eb1\\u0e99\\u0ec0\\u0eaa\\u0ebb\\u0eb2-\\u0ead\\u0eb2\\u0e97\\u0eb4\\u0e94 \\u0ec0\\u0ea5\\u0eb5\\u0ec8\\u0ea1\\u0ec0\\u0ea7\\u0ea5\\u0eb2 10:00-18:00 \\u0ec2\\u0ea1\\u0e87 \\u0eab\\u0eb2\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ec3\\u0e94\\u0ea1\\u0eb5\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ea5\\u0ead\\u0e87\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87 \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0e9c\\u0eb4\\u0e94\\u0eab\\u0ea7\\u0eb1\\u0e87.<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1962.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89812 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1962.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1962.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7466.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89813 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7466.jpg\\\" sizes=\\\"(max-width: 812px) 100vw, 812px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7466.jpg 812w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-150x150.jpg 150w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-300x300.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-768x768.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-696x696.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-420x420.jpg 420w\\\" alt=\\\"\\\" width=\\\"812\\\" height=\\\"812\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/6112.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89814 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/6112.jpg\\\" sizes=\\\"(max-width: 576px) 100vw, 576px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/6112.jpg 576w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-150x150.jpg 150w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-300x300.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-420x420.jpg 420w\\\" alt=\\\"\\\" width=\\\"576\\\" height=\\\"576\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7113.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89816 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7113.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7113.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-300x169.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-768x432.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-696x392.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-747x420.jpg 747w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"540\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7114.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89817 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7114.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7114.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><\\/p>\\r\\n<p>\\u0e82\\u0ecd\\u0ec9\\u0ea1\\u0eb9\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e9b\\u0eb0\\u0e81\\u0ead\\u0e9a\\u0e88\\u0eb2\\u0e81:\\u00a0\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ec9\\u0eb3\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d<\\/p>\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:40:46\",\"modified_user_id\":null,\"modified_time\":\"2017-11-22 08:40:46\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(4, 9, 5, '', '2017-11-22 08:41:44', 108, 9672, '9f01c909b7be6240a52933a6ea527a2db2d3987c', '{\"id\":9,\"asset_id\":62,\"parent_id\":\"2\",\"lft\":\"4\",\"rgt\":5,\"level\":2,\"path\":null,\"extension\":\"com_content\",\"title\":\"\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort) \",\"alias\":\"blue-lagoon-and-resort\",\"note\":\"\",\"description\":\"<h1 class=\\\"entry-title\\\">\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort) \\u0ea1\\u0eb2\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0ea5\\u0ec9\\u0ea7\\u0e88\\u0eb0\\u0e95\\u0eb4\\u0e94\\u0ec3\\u0e88<\\/h1>\\r\\n<div class=\\\"td-post-featured-image\\\"><a class=\\\"td-modal-image\\\" href=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg\\\" data-caption=\\\"\\\"><img class=\\\"entry-thumb td-animation-stack-type0-2\\\" title=\\\"Untitled\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg\\\" sizes=\\\"(max-width: 627px) 100vw, 627px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg 627w, https:\\/\\/imglp.com\\/2017\\/05\\/Untitled-300x168.jpg 300w\\\" alt=\\\"\\\" width=\\\"627\\\" height=\\\"352\\\" \\/><\\/a><\\/div>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ea1\\u0eb2\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ec0\\u0e9f\\u0ead\\u0e87 \\u0ec1\\u0e82\\u0ea7\\u0e87 \\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ea1\\u0eb2\\u0e8a\\u0ebb\\u0ea1\\u0e97\\u0eb4\\u0ea7\\u0e97\\u0eb1\\u0e94\\u0e97\\u0eb3\\u0ea1\\u0eb0\\u0e8a\\u0eb2\\u0e94 \\u0ec1\\u0ea5\\u0eb0 \\u0ea1\\u0eb2\\u0eab\\u0eb2\\u0ec1\\u0eab\\u0ebc\\u0ec8\\u0e87\\u0e81\\u0eb0\\u0ec2\\u0e94\\u0e94\\u0e99\\u0ec9\\u0eb3\\u0ec3\\u0eab\\u0ec9\\u0e84\\u0eb2\\u0e8d\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0eae\\u0ec9\\u0ead\\u0e99\\u0e81\\u0eb1\\u0e99\\u0ec4\\u0e9b\\u0ec0\\u0ea5\\u0eb5\\u0e8d \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0ec1\\u0e99\\u0ec8\\u0e99\\u0ead\\u0e99\\u0ea7\\u0ec8\\u0eb2\\u0ec0\\u0ea1\\u0eb7\\u0ec8\\u0ead\\u0ea1\\u0eb2\\u0eae\\u0ead\\u0e94\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ec0\\u0e9f\\u0eb7\\u0ead\\u0e87\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0e9e\\u0eb2\\u0e94\\u0e9a\\u0ecd\\u0ec8\\u0ec4\\u0e94\\u0ec9\\u0e99\\u0eb1\\u0ec9\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e84\\u0eb7\\u00a0<strong>\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort)<\\/strong><\\/p>\\r\\n<p>\\u0e81\\u0eb2\\u0e99\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ea1\\u0eb2\\u00a0\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 \\u00a0\\u0e9e\\u0ebd\\u0e87\\u0ec1\\u0e95\\u0ec8\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e82\\u0eb1\\u0e9a\\u0ea5\\u0ebb\\u0e94\\u0ead\\u0ead\\u0e81\\u0e88\\u0eb2\\u0e81\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ea7\\u0eb1\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0ec4\\u0e9b\\u0e97\\u0eb2\\u0e87 \\u0e9c\\u0eb2\\u0ec0\\u0e87\\u0eb4\\u0e99 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0ec0\\u0e96\\u0eb4\\u0e87\\u00a0(Blue lagoon and resort) \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ead\\u0eb5\\u0e81\\u0edc\\u0eb6\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0ec4\\u0e94\\u0ec9\\u0eae\\u0eb1\\u0e9a\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0e99\\u0eb4\\u0e8d\\u0ebb\\u0ea1\\u0eaa\\u0eb9\\u0e87\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb1\\u0e81\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e97\\u0eb1\\u0e87\\u0e9e\\u0eb2\\u0e8d\\u0ec3\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0e95\\u0ec8\\u0eb2\\u0e87\\u0e9b\\u0eb0\\u0ec0\\u0e97\\u0e94 \\u0ec0\\u0e9e\\u0eb2\\u0eb0\\u0e99\\u0ead\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e88\\u0eb0\\u0eaa\\u0eb0\\u0e94\\u0ea7\\u0e81\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d\\u0e97\\u0eb2\\u0e87\\u0e94\\u0ec9\\u0eb2\\u0e99\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e9e\\u0eb1\\u0e81\\u0ead\\u0eb2\\u0ec3\\u0eaa\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0ec0\\u0ea5\\u0eb7\\u0ec8\\u0ead\\u0e87\\u0ead\\u0eb2\\u0eab\\u0eb2\\u0e99\\u0e81\\u0eb2\\u0e99\\u0e81\\u0eb4\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0e82\\u0eb2\\u0e94\\u0ec0\\u0e82\\u0eb5\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0e8d\\u0eb1\\u0e87\\u0ea1\\u0eb5\\u0e81\\u0eb4\\u0e94\\u0e88\\u0eb0\\u0e81\\u0eb3\\u0eab\\u0ebc\\u0eb2\\u0e8d\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec4\\u0ea7\\u0ec9\\u0ec3\\u0eab\\u0ec9\\u0ec0\\u0ea5\\u0eb7\\u0ead\\u0e81\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec0\\u0e95\\u0eb1\\u0ea1\\u0ead\\u0eb5\\u0ec8\\u0ea1\\u0ec0\\u0e8a\\u0eb1\\u0ec8\\u0e99: \\u0e82\\u0eb5\\u0ec8\\u0eaa\\u0eb0\\u0ea5\\u0eb4\\u0e87, \\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0e99\\u0ec9\\u0eb3,\\u200b \\u0e9b\\u0eb5\\u0e99\\u0e9e\\u0eb9 \\u0ec1\\u0ea5\\u0eb0 \\u0ead\\u0eb7\\u0ec8\\u0e99\\u0ec6 \\u0e96\\u0ec9\\u0eb2\\u0ec3\\u0e9c\\u0ea1\\u0eb1\\u0e81\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0ea1\\u0eb1\\u0e99\\u0ec1\\u0e9a\\u0e9a\\u0e97\\u0ec9\\u0eb2\\u0e97\\u0eb2\\u0e8d \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2 \\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 \\u0ea1\\u0eb5\\u0e84\\u0eb3\\u0e95\\u0ead\\u0e9a\\u0e97\\u0eb5\\u0ec8\\u0e94\\u0eb5\\u0e97\\u0eb5\\u0ec8\\u0eaa\\u0eb8\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e97\\u0ec8\\u0eb2\\u0e99.<\\/p>\\r\\n<p>\\u0ec0\\u0ead\\u0ebb\\u0eb2\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ea7\\u0ec8\\u0eb2\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0ec0\\u0ea7\\u0ebb\\u0ec9\\u0eb2\\u0eab\\u0e8d\\u0eb1\\u0e87\\u0eab\\u0ebc\\u0eb2\\u0e8d \\u0ea1\\u0eb2\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87\\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e87\\u0eb2\\u0ea1\\u0ec6\\u0e99\\u0eb3\\u0e81\\u0eb1\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89655 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg\\\" sizes=\\\"(max-width: 480px) 100vw, 480px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg 480w, https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n-225x300.jpg 225w, https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n-315x420.jpg 315w\\\" alt=\\\"\\\" width=\\\"480\\\" height=\\\"640\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89656 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-300x225.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-80x60.jpg 80w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-265x198.jpg 265w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-560x420.jpg 560w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"480\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89657 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-300x225.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-80x60.jpg 80w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-265x198.jpg 265w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-560x420.jpg 560w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"480\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89658 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n-300x169.jpg 300w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"360\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89659 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n-300x169.jpg 300w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"360\\\" \\/><\\/a><\\/p>\\r\\n<p>\\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e9b\\u0eb0\\u0e81\\u0ead\\u0e9a\\u0e88\\u0eb2\\u0e81: \\u0ec2\\u0e99\\u0ec8 \\u0e9e\\u0eb8\\u0e94\\u0e97\\u0eb0\\u0eaa\\u0eb1\\u0e81<\\/p>\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:41:44\",\"modified_user_id\":null,\"modified_time\":\"2017-11-22 08:41:44\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(5, 10, 5, '', '2017-11-22 08:48:42', 108, 1616, '7d347343aa0979d25cc4ce79cca63f98286d4aab', '{\"id\":10,\"asset_id\":63,\"parent_id\":\"1\",\"lft\":\"15\",\"rgt\":16,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"\\u0e2a\\u0e2d\\u0e19\\u0e20\\u0e32\\u0e29\\u0e32\",\"alias\":\"2017-11-22-08-48-42\",\"note\":\"\",\"description\":\"<h1 class=\\\"entry-title\\\">\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e9e\\u0eb6\\u0e81\\u0eaa\\u0eb2\\u201d \\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e82\\u0ead\\u0e87\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99<\\/h1>\\r\\n<div class=\\\"td-post-featured-image\\\"><a class=\\\"td-modal-image\\\" href=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2.jpg\\\" data-caption=\\\"\\\"><img class=\\\"entry-thumb td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" title=\\\"254.2\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2-696x463.jpg\\\" sizes=\\\"(max-width: 696px) 100vw, 696px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2-696x463.jpg 696w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-300x200.jpg 300w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-768x511.jpg 768w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-631x420.jpg 631w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2.jpg 960w\\\" alt=\\\"\\\" width=\\\"696\\\" height=\\\"463\\\" \\/><\\/a><\\/div>\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:48:42\",\"modified_user_id\":null,\"modified_time\":\"2017-11-22 08:48:42\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(6, 2, 1, '', '2017-11-22 08:49:01', 108, 8179, '2ce91ff03c959b75ce19923fca87d6e7c3748720', '{\"id\":2,\"asset_id\":64,\"title\":\"\\u0e2a\\u0e2d\\u0e19\\u0e20\\u0e32\\u0e29\\u0e32\",\"alias\":\"2017-11-22-08-49-01\",\"introtext\":\"<h1 class=\\\"entry-title\\\">\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e9e\\u0eb6\\u0e81\\u0eaa\\u0eb2\\u201d \\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e82\\u0ead\\u0e87\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99<\\/h1>\\r\\n<div class=\\\"td-post-featured-image\\\"><a class=\\\"td-modal-image\\\" href=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2.jpg\\\" data-caption=\\\"\\\"><img class=\\\"entry-thumb td-animation-stack-type0-2\\\" title=\\\"254.2\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2-696x463.jpg\\\" sizes=\\\"(max-width: 696px) 100vw, 696px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/254.2-696x463.jpg 696w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-300x200.jpg 300w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-768x511.jpg 768w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2-631x420.jpg 631w, https:\\/\\/imglp.com\\/2017\\/03\\/254.2.jpg 960w\\\" alt=\\\"\\\" width=\\\"696\\\" height=\\\"463\\\" \\/><\\/a><\\/div>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0ea1\\u0eb4\\u0e99\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0ec1\\u0e99\\u0eb0\\u0e99\\u0eb3\\u0e9a\\u0ec8\\u0ead\\u0e99\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99, \\u0e9a\\u0ec8\\u0ead\\u0e99\\u0e81\\u0eb4\\u0e99, \\u0e9a\\u0ec8\\u0ead\\u0e99\\u0e97\\u0ec8\\u0ebd\\u0ea7, \\u0e9a\\u0ec8\\u0ead\\u0e99\\u0e96\\u0ec8\\u0eb2\\u0e8d\\u0ec0\\u0e8a\\u0ea5\\u0e9f\\u0eb5 \\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e82\\u0ead\\u0e87\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0e97\\u0eb5\\u0ec8\\u0e88\\u0eb0\\u0ec4\\u0e94\\u0ec9\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0ec0\\u0e82\\u0ebb\\u0ec9\\u0eb2\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e8a\\u0ebb\\u0ea1\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e81\\u0eb2\\u0e99\\u0ec3\\u0e99\\u0ea7\\u0eb1\\u0e99\\u0e97\\u0eb5 7 \\u0ec0\\u0ea1\\u0eb2\\u0eaa\\u0eb2 2017 \\u0e99\\u0eb5\\u0ec9 \\u0e9b\\u0eb5\\u0ec3\\u0edd\\u0ec8\\u0e99\\u0eb5\\u0ec9\\u0e96\\u0ec9\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0eae\\u0eb9\\u0ec9\\u0ea7\\u0ec8\\u0eb2\\u0eaa\\u0eb4\\u0ec4\\u0e9b\\u0e97\\u0eb2\\u0e87\\u0ec3\\u0e94\\u0e94\\u0eb5\\u0ea5\\u0ead\\u0e87\\u0ea1\\u0eb2\\u0ec1\\u0ea7\\u0ec8\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e8a\\u0ebb\\u0ea1\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0ec8\\u0e87\\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e88\\u0eb0\\u0e9a\\u0ecd\\u0ec8\\u0e9c\\u0eb4\\u0e94\\u0eab\\u0ea7\\u0eb1\\u0e87.<\\/p>\\r\\n<p>\\u0e96\\u0ec9\\u0eb2\\u0ec3\\u0e9c\\u0e95\\u0ec9\\u0ead\\u0e87\\u0e81\\u0eb2\\u0e99\\u0ec4\\u0e9b\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e8a\\u0ebb\\u0ea1\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e95\\u0eb1\\u0ec9\\u0e87\\u0ea2\\u0eb9\\u0ec8\\u00a0\\u0e9a\\u0ec9\\u0eb2\\u0e99\\u0ec3\\u0eab\\u0ea1\\u0ec8\\u0e94\\u0eb2\\u0e99\\u0e8a\\u0eb5, \\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87 \\u0e99\\u0eb2\\u0e8a\\u0eb2\\u0e8d\\u0e97\\u0ead\\u0e87, \\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ea5\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/03\\/228.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-87801 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/03\\/228.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/03\\/228.jpg 960w, https:\\/\\/imglp.com\\/2017\\/03\\/228-300x200.jpg 300w, https:\\/\\/imglp.com\\/2017\\/03\\/228-768x511.jpg 768w, https:\\/\\/imglp.com\\/2017\\/03\\/228-696x463.jpg 696w, https:\\/\\/imglp.com\\/2017\\/03\\/228-631x420.jpg 631w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"639\\\" \\/><\\/a><\\/p>\\r\\n<p>\\u0e99\\u0ead\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e94\\u0ead\\u0e81\\u0ec4\\u0ea1\\u0ec9\\u0e99\\u0eb2\\u0ec6\\u0e8a\\u0eb0\\u0e99\\u0eb4\\u0e94\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e8d\\u0eb1\\u0e87\\u0e88\\u0eb0\\u0eaa\\u0eb3\\u0e9c\\u0eb1\\u0e94\\u0e81\\u0eb1\\u0e9a\\u0eab\\u0ea1\\u0eb2\\u0e81\\u0ec1\\u0e95\\u0e87 \\u0eab\\u0ea5\\u0eb7\\u0ec0\\u0ea1\\u0ea5\\u0ead\\u0e99\\u0ea2\\u0eb5\\u0ec8\\u0e9b\\u0eb8\\u0ec8\\u0e99 \\u0e97\\u0eb5\\u0ec8\\u0e9b\\u0eb9\\u0e81\\u0ec1\\u0e9a\\u0e9a\\u0e9a\\u0ecd\\u0ec8\\u0ea1\\u0eb5\\u0ea2\\u0eb2\\u0e82\\u0ec9\\u0eb2\\u0ec1\\u0ea1\\u0e87\\u0ec4\\u0ea1\\u0ec9\\u0ec1\\u0ea5\\u0eb0\\u0e9b\\u0eb8\\u0e8d\\u0e95\\u0ec8\\u0eb2\\u0e87\\u0ec6 \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2\\u0e94\\u0eb5\\u0e95\\u0ecd\\u0ec8\\u0eaa\\u0eb8\\u0e82\\u0eb0\\u0e9e\\u0eb2\\u0e9a\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec1\\u0e99\\u0ec8\\u0e99\\u0ead\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0e96\\u0ec9\\u0eb2\\u0ec3\\u0e9c\\u0ea1\\u0eb1\\u0e81\\u0e99\\u0eb1\\u0ec8\\u0e87\\u0e87\\u0ead\\u0e8d\\u0e82\\u0eb2\\u0e95\\u0eb6\\u0e81\\u0e9b\\u0eb2\\u0ea2\\u0eb9\\u0ec8\\u0ec1\\u0e84\\u0ea1 \\u0ea2\\u0eb9\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0e9e\\u0eb4\\u0ec8\\u0e99\\u0ea1\\u0eb5\\u0e95\\u0eb9\\u0ea1\\u0e99\\u0ec9\\u0ead\\u0e8d\\u0ec4\\u0ea7\\u0ec9\\u0ec3\\u0eab\\u0ec9\\u0ec0\\u0e8a\\u0ebb\\u0ec8\\u0eb2\\u0e99\\u0eb1\\u0ec8\\u0e87\\u0e95\\u0eb6\\u0e81\\u0e9b\\u0eb2\\u0e8a\\u0eb8\\u0ea1\\u0ec1\\u0e8a\\u0ea7\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d\\u0ec3\\u0e88, \\u0eaa\\u0ec8\\u0ea7\\u0e99\\u0e84\\u0ebb\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0eae\\u0eb9\\u0e9a\\u0ec4\\u0e9b\\u0ec3\\u0eaa\\u0e81\\u0ecd\\u0ec8\\u0e95\\u0ec9\\u0ead\\u0e87\\u0e96\\u0ec8\\u0eb2\\u0e8d\\u0ec0\\u0e8a\\u0ea5\\u0e9f\\u0eb5 \\u0ec1\\u0ea5\\u0ec9\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc\\u0ea5\\u0ebb\\u0e87\\u0ec2\\u0e8a\\u0e8a\\u0ebd\\u0ea7\\u0ea2\\u0eb9\\u0ec8\\u0e95\\u0eb0\\u0eab\\u0ebc\\u0ead\\u0e94 \\u0e95\\u0ec9\\u0ead\\u0e87\\u0ea1\\u0eb2\\u0e97\\u0eb5\\u0ec8\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0ea5\\u0eb5\\u0e8d\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e88\\u0eb0\\u0e9a\\u0ecd\\u0ec8\\u0e9c\\u0eb4\\u0e94\\u0eab\\u0ea7\\u0eb1\\u0e87 \\u0ec0\\u0e9e\\u0eb2\\u0eb0\\u0ea1\\u0eb5\\u0eab\\u0ebc\\u0eb2\\u0e8d\\u0ea1\\u0eb8\\u0ea1\\u0ec3\\u0eab\\u0ec9\\u0ec0\\u0ea5\\u0eb7\\u0ead\\u0e81\\u0e96\\u0ec8\\u0eb2\\u0e8d\\u0eae\\u0eb9\\u0e9a\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec0\\u0e95\\u0eb1\\u0ea1\\u0ead\\u0eb5\\u0ec8\\u0ea1 \\u0ec0\\u0ea7\\u0ebb\\u0ec9\\u0eb2\\u0ec1\\u0ea5\\u0ec9\\u0ea7\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0e8a\\u0eb1\\u0e81\\u0e8a\\u0ec9\\u0eb2\\u0ec0\\u0eaa\\u0e8d\\u0ec0\\u0ea7\\u0ea5\\u0eb2 \\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0ea1\\u0eb2\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87\\u0eae\\u0eb9\\u0e9a\\u0e9a\\u0eb1\\u0e99\\u0e8d\\u0eb2\\u0e81\\u0eb2\\u0e94 \\u0e82\\u0ead\\u0e87\\u0eaa\\u0ea7\\u0e99\\u0e9e\\u0eb6\\u0e81\\u0eaa\\u0eb2 \\u0ec4\\u0e9b\\u0e9e\\u0ec9\\u0ead\\u0ea1\\u0ec6\\u0e81\\u0eb1\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"10\",\"created\":\"2017-11-22 08:49:01\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-22 08:49:01\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-11-22 08:49:01\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0);
INSERT INTO `holp3_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(7, 8, 5, '', '2017-11-22 08:56:23', 108, 10106, 'af654d2a97588bd6ad9e28512cb7ff8cc617fb22', '{\"id\":8,\"asset_id\":\"61\",\"parent_id\":\"10\",\"lft\":\"14\",\"rgt\":15,\"level\":2,\"path\":\"uncategorised\\/laotrips\",\"extension\":\"com_content\",\"title\":\"\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ea5\\u0eb2\\u0ea7 Laotrips\",\"alias\":\"laotrips\",\"note\":\"\",\"description\":\"<h1 class=\\\"entry-title\\\">\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d \\u0ec1\\u0eab\\u0ea5\\u0ec8\\u0e87\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0e84\\u0eb1\\u0ec9\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87\\u0e95\\u0ec9\\u0ead\\u0e87\\u0ec4\\u0e9b\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94<\\/h1>\\r\\n<p><img style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/59667.jpg\\\" \\/><\\/p>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0e8a\\u0ea7\\u0e99\\u0e97\\u0eb8\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0e99\\u0ec9\\u0eb3\\u0ec3\\u0eab\\u0ec9\\u0eab\\u0eb2\\u0e8d\\u0eae\\u0ec9\\u0ead\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87 \\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87 \\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0ead\\u0eb1\\u0e94\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0e9e\\u0eb2\\u0ea1\\u0eb2\\u0e99\\u0eb1\\u0ec9\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e84\\u0eb7\\u00a0<strong>\\u201c\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u201d\\u00a0<\\/strong>\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0ec3\\u0edd\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0eab\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0ec3\\u0e99\\u0ec4\\u0ea5\\u0e8d\\u0eb0\\u0e9a\\u0eb8\\u0e99\\u0e9b\\u0eb5\\u0ec3\\u0edd\\u0ec8\\u0ea5\\u0eb2\\u0ea7\\u0e97\\u0eb5\\u0ec8\\u0e9c\\u0ec8\\u0eb2\\u0e99\\u0ea1\\u0eb2.<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1699.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89811 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1699.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1699.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/1699-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><\\/p>\\r\\n<p><strong>\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d<\\/strong>\\u00a0\\u0e95\\u0eb1\\u0ec9\\u0e87\\u0ea2\\u0eb9\\u0ec8 \\u0e9a\\u0ec9\\u0eb2\\u0e99\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d\\u0ec3\\u0edd\\u0ec8 \\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0e97\\u0eb8\\u0ea5\\u0eb0\\u0e84\\u0ebb\\u0ea1 \\u0ec1\\u0e82\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99. \\u0e97\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0ec0\\u0e84\\u0eb7\\u0ec8\\u0ead\\u0e99\\u0ec4\\u0e9f\\u0e9f\\u0ec9\\u0eb2\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ea1\\u0eb1\\u0e873 \\u0ec0\\u0eaa\\u0eb1\\u0ec9\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ec0\\u0e82\\u0ebb\\u0ec9\\u0eb2\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0ec1\\u0ea1\\u0ec8\\u0e99\\u0eaa\\u0eb0\\u0e94\\u0ea7\\u0e81\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e9b\\u0eb9\\u0e8d\\u0eb2\\u0e87\\u0ec4\\u0e9b\\u0eae\\u0ead\\u0e94\\u0eaa\\u0ea7\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d \\u0ec1\\u0ea5\\u0eb0 \\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ead\\u0eb5\\u0e81\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ead\\u0eb5\\u0e81\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0edc\\u0eb6\\u0ec8\\u0e87 \\u0e97\\u0eb5\\u0ec8\\u0ea2\\u0eb9\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0ec4\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ec3\\u0e8a\\u0ec9\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0ec3\\u0e99\\u0e81\\u0eb2\\u0e99\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb0\\u0e84\\u0ead\\u0e99\\u0eab\\u0ebc\\u0ea7\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99\\u0ec4\\u0e9b\\u0eab\\u0eb2\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e9b\\u0eb0\\u0ea1\\u0eb2\\u0e99 1 \\u0e8a\\u0ebb\\u0ec8\\u0ea7\\u0ec2\\u0ea1\\u0e87\\u0e81\\u0eb1\\u0e9a\\u0ead\\u0eb5\\u0e81 30 \\u0e99\\u0eb2\\u0e97\\u0eb5 \\u0e81\\u0ecd\\u0ec8\\u0ea1\\u0eb2\\u0eae\\u0ead\\u0e94\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0eaa\\u0eb2\\u0ea1\\u0eb2\\u0e94\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0ea1\\u0ec8\\u0ea7\\u0e99\\u0e8a\\u0eb7\\u0ec8\\u0e99\\u0e81\\u0eb1\\u0e9a\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec4\\u0e94\\u0ec9 \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0ec0\\u0e9e\\u0eb4\\u0ec8\\u0e99\\u0e88\\u0eb0\\u0ec0\\u0e9b\\u0eb5\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e9a\\u0ecd\\u0ea5\\u0eb4\\u0e81\\u0eb2\\u0e99\\u0e97\\u0eb8\\u0e81\\u0ea7\\u0eb1\\u0e99\\u0ec0\\u0eaa\\u0ebb\\u0eb2-\\u0ead\\u0eb2\\u0e97\\u0eb4\\u0e94 \\u0ec0\\u0ea5\\u0eb5\\u0ec8\\u0ea1\\u0ec0\\u0ea7\\u0ea5\\u0eb2 10:00-18:00 \\u0ec2\\u0ea1\\u0e87 \\u0eab\\u0eb2\\u0e81\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0ec3\\u0e94\\u0ea1\\u0eb5\\u0ec0\\u0ea7\\u0ea5\\u0eb2\\u0e81\\u0ecd\\u0ec8\\u0ea5\\u0ead\\u0e87\\u0ea1\\u0eb2\\u0eaa\\u0eb3\\u0e9e\\u0eb1\\u0e94\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87 \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0e9c\\u0eb4\\u0e94\\u0eab\\u0ea7\\u0eb1\\u0e87.<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1962.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89812 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/1962.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/1962.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/1962-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7466.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89813 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7466.jpg\\\" sizes=\\\"(max-width: 812px) 100vw, 812px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7466.jpg 812w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-150x150.jpg 150w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-300x300.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-768x768.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-696x696.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7466-420x420.jpg 420w\\\" alt=\\\"\\\" width=\\\"812\\\" height=\\\"812\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/6112.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89814 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/6112.jpg\\\" sizes=\\\"(max-width: 576px) 100vw, 576px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/6112.jpg 576w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-150x150.jpg 150w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-300x300.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/6112-420x420.jpg 420w\\\" alt=\\\"\\\" width=\\\"576\\\" height=\\\"576\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7113.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89816 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7113.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7113.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-300x169.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-768x432.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-696x392.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7113-747x420.jpg 747w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"540\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7114.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89817 size-full td-animation-stack-type0-2\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/7114.jpg\\\" sizes=\\\"(max-width: 960px) 100vw, 960px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/7114.jpg 960w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-300x198.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-768x508.jpg 768w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-696x460.jpg 696w, https:\\/\\/imglp.com\\/2017\\/05\\/7114-635x420.jpg 635w\\\" alt=\\\"\\\" width=\\\"960\\\" height=\\\"635\\\" \\/><\\/a><\\/p>\\r\\n<p>\\u0e82\\u0ecd\\u0ec9\\u0ea1\\u0eb9\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e9b\\u0eb0\\u0e81\\u0ead\\u0e9a\\u0e88\\u0eb2\\u0e81:\\u00a0\\u0eaa\\u0ea7\\u0e99\\u0e99\\u0ec9\\u0eb3\\u0e9b\\u0ec8\\u0eb2\\u0e9e\\u0eb9\\u0ec0\\u0e82\\u0ebb\\u0eb2\\u0e84\\u0ea7\\u0eb2\\u0e8d<\\/p>\",\"published\":\"1\",\"checked_out\":\"108\",\"checked_out_time\":\"2017-11-22 08:55:56\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:40:46\",\"modified_user_id\":\"108\",\"modified_time\":\"2017-11-22 08:56:23\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(8, 9, 5, '', '2017-11-22 08:56:44', 108, 9730, '0bc604211f8485608b88ae38035c870650b68d55', '{\"id\":9,\"asset_id\":\"62\",\"parent_id\":\"10\",\"lft\":\"14\",\"rgt\":15,\"level\":2,\"path\":\"uncategorised\\/blue-lagoon-and-resort\",\"extension\":\"com_content\",\"title\":\"\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort) \",\"alias\":\"blue-lagoon-and-resort\",\"note\":\"\",\"description\":\"<h1 class=\\\"entry-title\\\">\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort) \\u0ea1\\u0eb2\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0ec1\\u0ea5\\u0ec9\\u0ea7\\u0e88\\u0eb0\\u0e95\\u0eb4\\u0e94\\u0ec3\\u0e88<\\/h1>\\r\\n<div class=\\\"td-post-featured-image\\\"><a class=\\\"td-modal-image\\\" href=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg\\\" data-caption=\\\"\\\"><img class=\\\"entry-thumb td-animation-stack-type0-2\\\" title=\\\"Untitled\\\" src=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg\\\" sizes=\\\"(max-width: 627px) 100vw, 627px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/Untitled.jpg 627w, https:\\/\\/imglp.com\\/2017\\/05\\/Untitled-300x168.jpg 300w\\\" alt=\\\"\\\" width=\\\"627\\\" height=\\\"352\\\" \\/><\\/a><\\/div>\\r\\n<p>\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e81\\u0eb1\\u0e9a\\u0ea5\\u0eb2\\u0ea7\\u0ec2\\u0e9e\\u0eaa\\u0e95\\u0ecc \\u0ea1\\u0eb7\\u0ec9\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ea1\\u0eb2\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ec0\\u0e9f\\u0ead\\u0e87 \\u0ec1\\u0e82\\u0ea7\\u0e87 \\u0ea7\\u0ebd\\u0e87\\u0e88\\u0eb1\\u0e99 \\u0ea1\\u0eb2\\u0e8a\\u0ebb\\u0ea1\\u0e97\\u0eb4\\u0ea7\\u0e97\\u0eb1\\u0e94\\u0e97\\u0eb3\\u0ea1\\u0eb0\\u0e8a\\u0eb2\\u0e94 \\u0ec1\\u0ea5\\u0eb0 \\u0ea1\\u0eb2\\u0eab\\u0eb2\\u0ec1\\u0eab\\u0ebc\\u0ec8\\u0e87\\u0e81\\u0eb0\\u0ec2\\u0e94\\u0e94\\u0e99\\u0ec9\\u0eb3\\u0ec3\\u0eab\\u0ec9\\u0e84\\u0eb2\\u0e8d\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0eae\\u0ec9\\u0ead\\u0e99\\u0e81\\u0eb1\\u0e99\\u0ec4\\u0e9b\\u0ec0\\u0ea5\\u0eb5\\u0e8d \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0ec1\\u0e99\\u0ec8\\u0e99\\u0ead\\u0e99\\u0ea7\\u0ec8\\u0eb2\\u0ec0\\u0ea1\\u0eb7\\u0ec8\\u0ead\\u0ea1\\u0eb2\\u0eae\\u0ead\\u0e94\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ec0\\u0e9f\\u0eb7\\u0ead\\u0e87\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e97\\u0eb5\\u0ec8\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0e9e\\u0eb2\\u0e94\\u0e9a\\u0ecd\\u0ec8\\u0ec4\\u0e94\\u0ec9\\u0e99\\u0eb1\\u0ec9\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e84\\u0eb7\\u00a0<strong>\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 (Blue lagoon and resort)<\\/strong><\\/p>\\r\\n<p>\\u0e81\\u0eb2\\u0e99\\u0ec0\\u0e94\\u0eb5\\u0e99\\u0e97\\u0eb2\\u0e87\\u0ea1\\u0eb2\\u00a0\\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 \\u00a0\\u0e9e\\u0ebd\\u0e87\\u0ec1\\u0e95\\u0ec8\\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e82\\u0eb1\\u0e9a\\u0ea5\\u0ebb\\u0e94\\u0ead\\u0ead\\u0e81\\u0e88\\u0eb2\\u0e81\\u0ec0\\u0ea1\\u0eb7\\u0ead\\u0e87\\u0ea7\\u0eb1\\u0e87\\u0ea7\\u0ebd\\u0e87\\u0ec4\\u0e9b\\u0e97\\u0eb2\\u0e87 \\u0e9c\\u0eb2\\u0ec0\\u0e87\\u0eb4\\u0e99 \\u0e97\\u0ec8\\u0eb2\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e88\\u0eb0\\u0ea1\\u0eb2\\u0ec0\\u0e96\\u0eb4\\u0e87\\u00a0(Blue lagoon and resort) \\u0ec0\\u0e8a\\u0eb4\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0ec1\\u0eab\\u0ec8\\u0e87\\u0e99\\u0eb5\\u0ec9\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ead\\u0eb5\\u0e81\\u0edc\\u0eb6\\u0ec8\\u0e87\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e97\\u0eb5\\u0ec8\\u0ec4\\u0e94\\u0ec9\\u0eae\\u0eb1\\u0e9a\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0e99\\u0eb4\\u0e8d\\u0ebb\\u0ea1\\u0eaa\\u0eb9\\u0e87\\u0e88\\u0eb2\\u0e81\\u0e99\\u0eb1\\u0e81\\u0e97\\u0ec8\\u0ead\\u0e87\\u0e97\\u0ec8\\u0ebd\\u0ea7\\u0e97\\u0eb1\\u0e87\\u0e9e\\u0eb2\\u0e8d\\u0ec3\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0e95\\u0ec8\\u0eb2\\u0e87\\u0e9b\\u0eb0\\u0ec0\\u0e97\\u0e94 \\u0ec0\\u0e9e\\u0eb2\\u0eb0\\u0e99\\u0ead\\u0e81\\u0e88\\u0eb2\\u0e81\\u0e88\\u0eb0\\u0eaa\\u0eb0\\u0e94\\u0ea7\\u0e81\\u0eaa\\u0eb0\\u0e9a\\u0eb2\\u0e8d\\u0e97\\u0eb2\\u0e87\\u0e94\\u0ec9\\u0eb2\\u0e99\\u0eaa\\u0eb0\\u0e96\\u0eb2\\u0e99\\u0e97\\u0eb5\\u0ec8\\u0e9e\\u0eb1\\u0e81\\u0ead\\u0eb2\\u0ec3\\u0eaa\\u0ec1\\u0ea5\\u0ec9\\u0ea7 \\u0ec0\\u0ea5\\u0eb7\\u0ec8\\u0ead\\u0e87\\u0ead\\u0eb2\\u0eab\\u0eb2\\u0e99\\u0e81\\u0eb2\\u0e99\\u0e81\\u0eb4\\u0e99\\u0e81\\u0ecd\\u0ec8\\u0e9a\\u0ecd\\u0ec8\\u0e82\\u0eb2\\u0e94\\u0ec0\\u0e82\\u0eb5\\u0e99 \\u0ec1\\u0ea5\\u0eb0 \\u0e8d\\u0eb1\\u0e87\\u0ea1\\u0eb5\\u0e81\\u0eb4\\u0e94\\u0e88\\u0eb0\\u0e81\\u0eb3\\u0eab\\u0ebc\\u0eb2\\u0e8d\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec4\\u0ea7\\u0ec9\\u0ec3\\u0eab\\u0ec9\\u0ec0\\u0ea5\\u0eb7\\u0ead\\u0e81\\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0ea2\\u0ec8\\u0eb2\\u0e87\\u0ec0\\u0e95\\u0eb1\\u0ea1\\u0ead\\u0eb5\\u0ec8\\u0ea1\\u0ec0\\u0e8a\\u0eb1\\u0ec8\\u0e99: \\u0e82\\u0eb5\\u0ec8\\u0eaa\\u0eb0\\u0ea5\\u0eb4\\u0e87, \\u0eab\\u0ebc\\u0eb4\\u0ec9\\u0e99\\u0e99\\u0ec9\\u0eb3,\\u200b \\u0e9b\\u0eb5\\u0e99\\u0e9e\\u0eb9 \\u0ec1\\u0ea5\\u0eb0 \\u0ead\\u0eb7\\u0ec8\\u0e99\\u0ec6 \\u0e96\\u0ec9\\u0eb2\\u0ec3\\u0e9c\\u0ea1\\u0eb1\\u0e81\\u0e84\\u0ea7\\u0eb2\\u0ea1\\u0ea1\\u0eb1\\u0e99\\u0ec1\\u0e9a\\u0e9a\\u0e97\\u0ec9\\u0eb2\\u0e97\\u0eb2\\u0e8d \\u0eae\\u0eb1\\u0e9a\\u0eae\\u0ead\\u0e87\\u0ea7\\u0ec8\\u0eb2 \\u0e9c\\u0eb2\\u0eab\\u0ebc\\u0ea7\\u0e87 \\u0ea1\\u0eb5\\u0e84\\u0eb3\\u0e95\\u0ead\\u0e9a\\u0e97\\u0eb5\\u0ec8\\u0e94\\u0eb5\\u0e97\\u0eb5\\u0ec8\\u0eaa\\u0eb8\\u0e94\\u0ec3\\u0eab\\u0ec9\\u0e97\\u0ec8\\u0eb2\\u0e99.<\\/p>\\r\\n<p>\\u0ec0\\u0ead\\u0ebb\\u0eb2\\u0ec0\\u0e9b\\u0eb1\\u0e99\\u0ea7\\u0ec8\\u0eb2\\u0ec0\\u0eae\\u0ebb\\u0eb2\\u0e9a\\u0ecd\\u0ec8\\u0ec0\\u0ea7\\u0ebb\\u0ec9\\u0eb2\\u0eab\\u0e8d\\u0eb1\\u0e87\\u0eab\\u0ebc\\u0eb2\\u0e8d \\u0ea1\\u0eb2\\u0ec0\\u0e9a\\u0eb4\\u0ec8\\u0e87\\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e87\\u0eb2\\u0ea1\\u0ec6\\u0e99\\u0eb3\\u0e81\\u0eb1\\u0e99\\u0ec0\\u0ea5\\u0eb5\\u0e8d<\\/p>\\r\\n<p><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89655 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg\\\" sizes=\\\"(max-width: 480px) 100vw, 480px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n.jpg 480w, https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n-225x300.jpg 225w, https:\\/\\/imglp.com\\/2017\\/05\\/18268397_1427906463935025_9166127538006293577_n-315x420.jpg 315w\\\" alt=\\\"\\\" width=\\\"480\\\" height=\\\"640\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89656 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-300x225.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-80x60.jpg 80w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-265x198.jpg 265w, https:\\/\\/imglp.com\\/2017\\/05\\/18268473_702127539966496_3659583800389016185_n-560x420.jpg 560w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"480\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89657 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-300x225.jpg 300w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-80x60.jpg 80w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-265x198.jpg 265w, https:\\/\\/imglp.com\\/2017\\/05\\/18300799_305185849902753_2924584744175011195_n-560x420.jpg 560w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"480\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89658 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300889_1366770690082854_4114708776835169093_n-300x169.jpg 300w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"360\\\" \\/><\\/a><a href=\\\"https:\\/\\/laopost.com\\/goto\\/https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg\\\" target=\\\"_blank\\\" rel=\\\"nofollow noopener noreferrer\\\"><img class=\\\"aligncenter wp-image-89659 size-full td-animation-stack-type0-2\\\" src=\\\"https:\\/\\/www.laopost.com\\/wp-content\\/uploads\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg\\\" sizes=\\\"(max-width: 640px) 100vw, 640px\\\" srcset=\\\"https:\\/\\/imglp.com\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n.jpg 640w, https:\\/\\/imglp.com\\/2017\\/05\\/18300987_1366770696749520_4209542542748387487_n-300x169.jpg 300w\\\" alt=\\\"\\\" width=\\\"640\\\" height=\\\"360\\\" \\/><\\/a><\\/p>\\r\\n<p>\\u0eae\\u0eb9\\u0e9a\\u0e9e\\u0eb2\\u0e9a\\u0e9b\\u0eb0\\u0e81\\u0ead\\u0e9a\\u0e88\\u0eb2\\u0e81: \\u0ec2\\u0e99\\u0ec8 \\u0e9e\\u0eb8\\u0e94\\u0e97\\u0eb0\\u0eaa\\u0eb1\\u0e81<\\/p>\",\"published\":\"1\",\"checked_out\":\"108\",\"checked_out_time\":\"2017-11-22 08:56:30\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"108\",\"created_time\":\"2017-11-22 08:41:44\",\"modified_user_id\":\"108\",\"modified_time\":\"2017-11-22 08:56:44\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(9, 3, 1, '', '2017-11-23 04:35:25', 108, 2084, '2585c43191e98956726fba2a9739ffede7eb9e7c', '{\"id\":3,\"asset_id\":70,\"title\":\"\\u0e15\\u0e34\\u0e14\\u0e15\\u0e48\\u0e2d\\u0e40\\u0e23\\u0e32\",\"alias\":\"2017-11-23-04-35-25\",\"introtext\":\"<h1>\\u0e15\\u0e34\\u0e14\\u0e15\\u0e48\\u0e2d\\u0e40\\u0e23\\u0e32<\\/h1>\\r\\n<p>\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-23 04:35:25\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-23 04:35:25\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-11-23 04:35:25\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(10, 3, 1, '', '2017-11-23 04:39:24', 108, 2246, '758bffbdf2c1391a448906502b60bd4b13a9c273', '{\"id\":3,\"asset_id\":\"70\",\"title\":\"\\u0e15\\u0e34\\u0e14\\u0e15\\u0e48\\u0e2d\\u0e40\\u0e23\\u0e32\",\"alias\":\"2017-11-23-04-35-25\",\"introtext\":\"<h1>\\u0e15\\u0e34\\u0e14\\u0e15\\u0e48\\u0e2d\\u0e40\\u0e23\\u0e32<\\/h1>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>\\u00a0<strong>\\u0e42\\u0e17\\u0e23 :1234567890<\\/strong><\\/p>\\r\\n<p><strong>\\u00a0Email: laos@hotmail.com<\\/strong><\\/p>\\r\\n<p>\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-23 04:35:25\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-23 04:39:24\",\"modified_by\":\"108\",\"checked_out\":\"108\",\"checked_out_time\":\"2017-11-23 04:38:20\",\"publish_up\":\"2017-11-23 04:35:25\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"6\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(11, 4, 1, '', '2017-11-23 04:51:38', 108, 2783, '7e9814573a4d40c0daf387c0831ffc6cfc1dc917', '{\"id\":4,\"asset_id\":71,\"title\":\"\\u0e42\\u0e23\\u0e07\\u0e41\\u0e23\\u0e21 \\u0e17\\u0e35\\u0e48\\u0e1e\\u0e31\\u0e01\",\"alias\":\"2017-11-23-04-51-38\",\"introtext\":\"<p>\\u0ec2\\u0e95\\u0eb0\\u0e9a\\u0eb2\\u0e9a\\u0eb5\\u0e84\\u0eb4\\u0ea7 \\u0ec1\\u0e84\\u0ea1\\u0eaa\\u0eb2\\u0e8d\\u0e99\\u0ecd\\u0ec9\\u0eb2\\u0e81\\u0eb1\\u0e94<\\/p>\\r\\n<p><img style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"images\\/21463172_1532559043498049_6379071660908574067_n.jpg\\\" alt=\\\"\\\" \\/><\\/p>\\r\\n<p>ECO King Bed<img style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" src=\\\"images\\/21199826_1524946854259268_406583625877263173_o.jpg\\\" alt=\\\"\\\" width=\\\"1474\\\" height=\\\"984\\\" \\/><\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p><img src=\\\"images\\/19990140_1466245273462760_9070176813509825167_n.jpg\\\" alt=\\\"\\\" \\/><\\/p>\\r\\n<p>Chill out near by the pool<\\/p>\\r\\n<p><img src=\\\"images\\/20368832_1481736258580328_1616547803102042229_o.jpg\\\" alt=\\\"\\\" \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-23 04:51:38\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-23 04:51:38\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-11-23 04:51:38\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(12, 5, 1, '', '2017-11-23 06:25:56', 108, 2688, 'cee2b5cf6ca826720d1a12b0485eff0ccc094e97', '{\"id\":5,\"asset_id\":72,\"title\":\"\\u0e23\\u0e39\\u0e1b\\u0e20\\u0e32\\u0e1e\",\"alias\":\"2017-11-23-06-25-56\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-23 06:25:56\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-23 06:25:56\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-11-23 06:25:56\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/17192460_1299233076830648_2881960565113594214_o.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20368832_1481736258580328_1616547803102042229_o1.jpg\\\",\\\"post_format\\\":\\\"gallery\\\",\\\"gallery\\\":\\\"{\\\\\\\"gallery_images\\\\\\\":[\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20368832_1481736258580328_1616547803102042229_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/21199826_1524946854259268_406583625877263173_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/17855493_1336298459790776_1328872890552281111_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20645243_1497267137027240_7655830566598971746_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/13179305_879548355489705_6732883611063976483_n.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/10928838_667071930070683_585670321427441324_o.jpg\\\\\\\"]}\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(13, 5, 1, '', '2017-11-23 06:29:51', 108, 2719, '06c809ac61b2e9e1f860d9967e877a3e91f6184d', '{\"id\":5,\"asset_id\":\"72\",\"title\":\"\\u0e23\\u0e39\\u0e1b\\u0e20\\u0e32\\u0e1e\",\"alias\":\"2017-11-23-06-25-56\",\"introtext\":\"<p>1111<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-11-23 06:25:56\",\"created_by\":\"108\",\"created_by_alias\":\"\",\"modified\":\"2017-11-23 06:29:51\",\"modified_by\":\"108\",\"checked_out\":\"108\",\"checked_out_time\":\"2017-11-23 06:29:29\",\"publish_up\":\"2017-11-23 06:25:56\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/17192460_1299233076830648_2881960565113594214_o.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20368832_1481736258580328_1616547803102042229_o1.jpg\\\",\\\"post_format\\\":\\\"gallery\\\",\\\"gallery\\\":\\\"{\\\\\\\"gallery_images\\\\\\\":[\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20368832_1481736258580328_1616547803102042229_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/21199826_1524946854259268_406583625877263173_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/17855493_1336298459790776_1328872890552281111_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/20645243_1497267137027240_7655830566598971746_o.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/13179305_879548355489705_6732883611063976483_n.jpg\\\\\\\",\\\\\\\"images\\\\\\/2560\\\\\\/11\\\\\\/23\\\\\\/10928838_667071930070683_585670321427441324_o.jpg\\\\\\\"]}\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"2\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_updates`
--

CREATE TABLE `holp3_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Dumping data for table `holp3_updates`
--

INSERT INTO `holp3_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 2, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.4.1', '', 'https://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(2, 2, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(3, 2, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.7.3.1', '', 'https://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(4, 2, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(5, 2, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.8.0.1', '', 'https://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(6, 2, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(7, 2, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(8, 2, 0, 'Georgian', '', 'pkg_ka-GE', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/ka-GE_details.xml', '', ''),
(9, 2, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.6.3.2', '', 'https://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(10, 2, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(11, 2, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'https://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(12, 2, 0, 'Bengali', '', 'pkg_bn-BD', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/bn-BD_details.xml', '', ''),
(13, 2, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(14, 2, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(15, 2, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(16, 2, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(17, 2, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(18, 2, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.7.3.3', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(19, 2, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.8.0.1', '', 'https://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(20, 2, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(21, 2, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(22, 2, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.8.2.2', '', 'https://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(23, 2, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(24, 2, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(25, 2, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(26, 2, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(27, 2, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.8.1.1', '', 'https://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(28, 2, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.7.3.1', '', 'https://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(29, 2, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(30, 2, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.7.4.1', '', 'https://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(31, 2, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'https://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(32, 2, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(33, 2, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.8.2.2', '', 'https://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(34, 2, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(35, 2, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(36, 2, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(37, 2, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.8.1.1', '', 'https://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(38, 2, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(39, 2, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(40, 2, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(41, 2, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(42, 2, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(43, 2, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.7.1.1', '', 'https://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(44, 2, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(45, 2, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(46, 2, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/eu-ES_details.xml', '', ''),
(47, 2, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.2', '', 'https://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(48, 2, 0, 'German DE', '', 'pkg_de-DE', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(49, 2, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(50, 2, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(51, 2, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(52, 2, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(53, 2, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(54, 2, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(55, 2, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(56, 2, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.8.1.1', '', 'https://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(57, 2, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(58, 2, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(59, 2, 0, 'English CA', '', 'pkg_en-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(60, 2, 0, 'French CA', '', 'pkg_fr-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(61, 2, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(62, 2, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(63, 2, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.2', '', 'https://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(64, 2, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.5.0.2', '', 'https://update.joomla.org/language/details3/tk-TM_details.xml', '', ''),
(65, 2, 0, 'Irish', '', 'pkg_ga-IE', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/ga-IE_details.xml', '', ''),
(66, 2, 0, 'Dzongkha', '', 'pkg_dz-BT', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/dz-BT_details.xml', '', ''),
(67, 2, 0, 'Slovenian', '', 'pkg_sl-SI', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/sl-SI_details.xml', '', ''),
(68, 2, 0, 'Spanish CO', '', 'pkg_es-CO', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/es-CO_details.xml', '', ''),
(69, 2, 0, 'German CH', '', 'pkg_de-CH', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/de-CH_details.xml', '', ''),
(70, 2, 0, 'German AT', '', 'pkg_de-AT', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/de-AT_details.xml', '', ''),
(71, 2, 0, 'German LI', '', 'pkg_de-LI', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/de-LI_details.xml', '', ''),
(72, 2, 0, 'German LU', '', 'pkg_de-LU', 'package', '', 0, '3.8.2.1', '', 'https://update.joomla.org/language/details3/de-LU_details.xml', '', ''),
(73, 2, 0, 'English NZ', '', 'pkg_en-NZ', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-NZ_details.xml', '', ''),
(74, 5, 0, 'Hydrogen', 'Hydrogen Template', 'g5_hydrogen', 'template', '', 0, '5.4.19', '', 'http://updates.gantry.org/5.0/joomla/tpl_g5_hydrogen.xml', '', ''),
(75, 5, 0, 'Helium', 'Helium Template', 'g5_helium', 'template', '', 0, '5.4.19', '', 'http://updates.gantry.org/5.0/joomla/tpl_g5_helium.xml', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_update_sites`
--

CREATE TABLE `holp3_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Dumping data for table `holp3_update_sites`
--

INSERT INTO `holp3_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1511408685, ''),
(2, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 1511347030, ''),
(3, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1511347037, ''),
(4, 'Gantry 5', 'extension', 'http://updates.gantry.org/5.0/joomla/pkg_gantry5.xml', 1, 1511347038, ''),
(5, 'Gantry 5', 'collection', 'http://updates.gantry.org/5.0/joomla/list.xml', 1, 1511347039, ''),
(6, 'Helix3 - Ajax', 'extension', 'http://www.joomshaper.com/updates/plg-ajax-helix3.xml', 1, 0, ''),
(7, 'System - Helix3 Framework', 'extension', 'http://www.joomshaper.com/updates/plg-system-helix3.xml', 1, 0, ''),
(8, 'Slideshow CK Update', 'extension', 'http://update.joomlack.fr/mod_slideshowck_update.xml', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_update_sites_extensions`
--

CREATE TABLE `holp3_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Dumping data for table `holp3_update_sites_extensions`
--

INSERT INTO `holp3_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 802),
(2, 10002),
(3, 28),
(4, 10010),
(5, 10010),
(6, 10013),
(7, 10014),
(8, 10016);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_usergroups`
--

CREATE TABLE `holp3_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_usergroups`
--

INSERT INTO `holp3_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `holp3_users`
--

CREATE TABLE `holp3_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_users`
--

INSERT INTO `holp3_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(108, 'Super User', 'admin', 'laotrips@hotmail.com', '$2y$10$A/ydrcq1n74p4kC/yxRXCurmHZi4uEtTt5jbcIIazVT.jN1NPHb6G', 0, 1, '2017-11-22 08:14:53', '2017-11-23 06:21:34', '0', '{\"admin_style\":\"\",\"admin_language\":\"th-TH\",\"language\":\"th-TH\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_user_keys`
--

CREATE TABLE `holp3_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_user_notes`
--

CREATE TABLE `holp3_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holp3_user_profiles`
--

CREATE TABLE `holp3_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `holp3_user_usergroup_map`
--

CREATE TABLE `holp3_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_user_usergroup_map`
--

INSERT INTO `holp3_user_usergroup_map` (`user_id`, `group_id`) VALUES
(108, 8);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_utf8_conversion`
--

CREATE TABLE `holp3_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_utf8_conversion`
--

INSERT INTO `holp3_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `holp3_viewlevels`
--

CREATE TABLE `holp3_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holp3_viewlevels`
--

INSERT INTO `holp3_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `holp3_assets`
--
ALTER TABLE `holp3_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indexes for table `holp3_associations`
--
ALTER TABLE `holp3_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Indexes for table `holp3_banners`
--
ALTER TABLE `holp3_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_banner_clients`
--
ALTER TABLE `holp3_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indexes for table `holp3_banner_tracks`
--
ALTER TABLE `holp3_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indexes for table `holp3_categories`
--
ALTER TABLE `holp3_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_cis_categories`
--
ALTER TABLE `holp3_cis_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holp3_cis_images`
--
ALTER TABLE `holp3_cis_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_slider` (`id_slider`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `ordering` (`ordering`);

--
-- Indexes for table `holp3_cis_sliders`
--
ALTER TABLE `holp3_cis_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `holp3_cis_templates`
--
ALTER TABLE `holp3_cis_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holp3_contact_details`
--
ALTER TABLE `holp3_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `holp3_content`
--
ALTER TABLE `holp3_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`),
  ADD KEY `idx_alias` (`alias`(191));

--
-- Indexes for table `holp3_contentitem_tag_map`
--
ALTER TABLE `holp3_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Indexes for table `holp3_content_frontpage`
--
ALTER TABLE `holp3_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `holp3_content_rating`
--
ALTER TABLE `holp3_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `holp3_content_types`
--
ALTER TABLE `holp3_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Indexes for table `holp3_extensions`
--
ALTER TABLE `holp3_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indexes for table `holp3_fields`
--
ALTER TABLE `holp3_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_fields_categories`
--
ALTER TABLE `holp3_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Indexes for table `holp3_fields_groups`
--
ALTER TABLE `holp3_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_fields_values`
--
ALTER TABLE `holp3_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Indexes for table `holp3_finder_filters`
--
ALTER TABLE `holp3_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `holp3_finder_links`
--
ALTER TABLE `holp3_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indexes for table `holp3_finder_links_terms0`
--
ALTER TABLE `holp3_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms1`
--
ALTER TABLE `holp3_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms2`
--
ALTER TABLE `holp3_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms3`
--
ALTER TABLE `holp3_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms4`
--
ALTER TABLE `holp3_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms5`
--
ALTER TABLE `holp3_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms6`
--
ALTER TABLE `holp3_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms7`
--
ALTER TABLE `holp3_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms8`
--
ALTER TABLE `holp3_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_terms9`
--
ALTER TABLE `holp3_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termsa`
--
ALTER TABLE `holp3_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termsb`
--
ALTER TABLE `holp3_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termsc`
--
ALTER TABLE `holp3_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termsd`
--
ALTER TABLE `holp3_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termse`
--
ALTER TABLE `holp3_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_links_termsf`
--
ALTER TABLE `holp3_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `holp3_finder_taxonomy`
--
ALTER TABLE `holp3_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indexes for table `holp3_finder_taxonomy_map`
--
ALTER TABLE `holp3_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `holp3_finder_terms`
--
ALTER TABLE `holp3_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indexes for table `holp3_finder_terms_common`
--
ALTER TABLE `holp3_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Indexes for table `holp3_finder_tokens`
--
ALTER TABLE `holp3_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Indexes for table `holp3_finder_tokens_aggregate`
--
ALTER TABLE `holp3_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Indexes for table `holp3_finder_types`
--
ALTER TABLE `holp3_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `holp3_languages`
--
ALTER TABLE `holp3_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `holp3_menu`
--
ALTER TABLE `holp3_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_menu_types`
--
ALTER TABLE `holp3_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indexes for table `holp3_messages`
--
ALTER TABLE `holp3_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indexes for table `holp3_messages_cfg`
--
ALTER TABLE `holp3_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indexes for table `holp3_modules`
--
ALTER TABLE `holp3_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_modules_menu`
--
ALTER TABLE `holp3_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indexes for table `holp3_newsfeeds`
--
ALTER TABLE `holp3_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `holp3_overrider`
--
ALTER TABLE `holp3_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holp3_postinstall_messages`
--
ALTER TABLE `holp3_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Indexes for table `holp3_redirect_links`
--
ALTER TABLE `holp3_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indexes for table `holp3_schemas`
--
ALTER TABLE `holp3_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indexes for table `holp3_session`
--
ALTER TABLE `holp3_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Indexes for table `holp3_tags`
--
ALTER TABLE `holp3_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `holp3_template_styles`
--
ALTER TABLE `holp3_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_home` (`home`);

--
-- Indexes for table `holp3_ucm_base`
--
ALTER TABLE `holp3_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Indexes for table `holp3_ucm_content`
--
ALTER TABLE `holp3_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Indexes for table `holp3_ucm_history`
--
ALTER TABLE `holp3_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Indexes for table `holp3_updates`
--
ALTER TABLE `holp3_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `holp3_update_sites`
--
ALTER TABLE `holp3_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indexes for table `holp3_update_sites_extensions`
--
ALTER TABLE `holp3_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indexes for table `holp3_usergroups`
--
ALTER TABLE `holp3_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indexes for table `holp3_users`
--
ALTER TABLE `holp3_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `holp3_user_keys`
--
ALTER TABLE `holp3_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD UNIQUE KEY `series_2` (`series`),
  ADD UNIQUE KEY `series_3` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `holp3_user_notes`
--
ALTER TABLE `holp3_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Indexes for table `holp3_user_profiles`
--
ALTER TABLE `holp3_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indexes for table `holp3_user_usergroup_map`
--
ALTER TABLE `holp3_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `holp3_viewlevels`
--
ALTER TABLE `holp3_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `holp3_assets`
--
ALTER TABLE `holp3_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `holp3_banners`
--
ALTER TABLE `holp3_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_banner_clients`
--
ALTER TABLE `holp3_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_categories`
--
ALTER TABLE `holp3_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `holp3_cis_categories`
--
ALTER TABLE `holp3_cis_categories`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_cis_images`
--
ALTER TABLE `holp3_cis_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `holp3_cis_sliders`
--
ALTER TABLE `holp3_cis_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_cis_templates`
--
ALTER TABLE `holp3_cis_templates`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_contact_details`
--
ALTER TABLE `holp3_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_content`
--
ALTER TABLE `holp3_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `holp3_content_types`
--
ALTER TABLE `holp3_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `holp3_extensions`
--
ALTER TABLE `holp3_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10020;

--
-- AUTO_INCREMENT for table `holp3_fields`
--
ALTER TABLE `holp3_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_fields_groups`
--
ALTER TABLE `holp3_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_finder_filters`
--
ALTER TABLE `holp3_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_finder_links`
--
ALTER TABLE `holp3_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_finder_taxonomy`
--
ALTER TABLE `holp3_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_finder_terms`
--
ALTER TABLE `holp3_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_finder_types`
--
ALTER TABLE `holp3_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_languages`
--
ALTER TABLE `holp3_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `holp3_menu`
--
ALTER TABLE `holp3_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `holp3_menu_types`
--
ALTER TABLE `holp3_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_messages`
--
ALTER TABLE `holp3_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_modules`
--
ALTER TABLE `holp3_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `holp3_newsfeeds`
--
ALTER TABLE `holp3_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_overrider`
--
ALTER TABLE `holp3_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT for table `holp3_postinstall_messages`
--
ALTER TABLE `holp3_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `holp3_redirect_links`
--
ALTER TABLE `holp3_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_tags`
--
ALTER TABLE `holp3_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holp3_template_styles`
--
ALTER TABLE `holp3_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `holp3_ucm_content`
--
ALTER TABLE `holp3_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_ucm_history`
--
ALTER TABLE `holp3_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `holp3_updates`
--
ALTER TABLE `holp3_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `holp3_update_sites`
--
ALTER TABLE `holp3_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `holp3_usergroups`
--
ALTER TABLE `holp3_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `holp3_users`
--
ALTER TABLE `holp3_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `holp3_user_keys`
--
ALTER TABLE `holp3_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_user_notes`
--
ALTER TABLE `holp3_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holp3_viewlevels`
--
ALTER TABLE `holp3_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
